package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;








import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_CREATE_SPE_WEBSITE.
 */
public class CM_ADMIN_CREATE_WEBSITE extends ActionEngine implements Page {
		
		/** The add_new_web. */
		By add_new_web = By.partialLinkText("Add a new website"); 
		
		/** The type. */
		By itype =By.xpath("//select[@id='WebsitePlate_WebsiteTypeDropDownList']");
		
		/** The name. */
		By name =By.xpath("//input[@id='WebsitePlate_NameTextBox']");
		
		/** The description. */
		By description =By.xpath("//input[@id='WebsitePlate_DescriptionTextBox']");
		
		/** The skin. */
		By skin =By.xpath(".//*[@id='WebsitePlate_SkinIdDropDownList']");
		
		/** The save_button. */
		By save_button = By.xpath("//a[@id='SaveButton']");
		
		/** The website_name. */
		By website_name	= By.xpath("//td[contains(.,'Website (test123, Acme New 2)')]");
		
		/** The Giving Exp. */
		By giving_exp = By.xpath(".//*[@id='WebsitePlate_PledgeExperienceDropDownList']");
		
		By Filter_Locator = By.cssSelector("[id*='FilterTextBox']");
		
		By Webname =By.xpath(".//*[@id='WebsiteGrid']/tbody/tr[2]/td[1]");
		
		By websites_expand = By.xpath("//a[contains(.,'Websites')]");
		
		String strname="";
				
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin create spe website.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_CREATE_WEBSITE(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
			try {
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }*/
				JSClick(add_new_web, "Add New Web");
				Thread.sleep(5000);
				String stype = (String)params.get("type");
				if((isElementVisible(itype, "type value")))
				{
				selectBySendkeys(itype, (String)params.get("type"), "type value");
				}
				//Thread.sleep(1000);
				//tab(itype, "tab out for dropdown");
				Thread.sleep(10000);
				strname = strwithtimestamp((String)params.get("name"));
				type(name, strname, "name value");
				Thread.sleep(2000);
				type(description, (String)params.get("description"), "description value");
				Thread.sleep(2000);
				if (stype.equals("Dynamic Pledge Experience")){
					selectBySendkeys(giving_exp, (String)params.get("giving_exp"), "Giving Experience");
					Thread.sleep(5000);
				}
				
				Thread.sleep(5000);			
				String strskin = (String)params.get("skin");
				if((isVisibleOnly(skin, "Skin dropdown"))) 
				{
				selectBySendkeys(skin,strskin,"Default Skin");
				//selectByIndex(skin, 1, "skin value");
				Thread.sleep(6000);
				tab(skin, "tab out for dropdown");
				Thread.sleep(5000);
				}
				Thread.sleep(5000);
				JSClick(save_button, "Click on save button");
				Thread.sleep(7000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(5000);
				click(websites_expand, "Websites");	
				Thread.sleep(6000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(6000);
				By grid = By.xpath(".//*[@id='WebsiteGrid']/tbody/tr[2]/td[1]");
				Thread.sleep(6000);
				FilterCheck(Filter_Locator,grid,strname);
				Thread.sleep(6000);
				boolean validationPoint = isElementPresent(grid, "website list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Verify Website list is present", "website list is present",this.Driver);
				}else{
					this.reporter.failureReport("Verify Website list is present", "website list is present", this.Driver);
				}
				Thread.sleep(5000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}	
		
		@Override
		public void move() {
			try {
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		
		}
	}