package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;














import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;


/**
 * The Class CM_CAMPAIGN_CONFIGURE_DONOR_SETTING.
 */
public class CM_CAMPAIGN_CONFIGURE_DONOR_SETTING extends ActionEngine implements Page {
		
		/** The donorGroupIcon. */
		By donorGroupIcon	= By.cssSelector("[id*='ViewSettingsButton']");
		
		/** The campaignSettingsTitle. */
		By campaignSettingsTitle = By.xpath(".//*[@id='Campaign_DonorPlate_CampaignSettingsPlaceHolder']/span[1]");
		
		/** The gridViewSettingsButton. */
		By gridViewSettingsButton	= By.cssSelector("#Campaign_DonorPlate_DonorGroupEditButton");
		
		/** The donorGroupEditButton. */
		By donorGroupEditButton	= By.cssSelector("#Campaign_DonorPlate_DonorGroupEditButton");
		
		/** The groupSelect. */
		//By groupSelect	= By.cssSelector("#Campaign_DonorPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList_ItemsSelect>option:nth-of-type(1)");
		By groupSelect = By.xpath(".//*[@id='Campaign_DonorPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList_ItemsSelect']/option[1]");
		
		/** The groupSelectButton. */
		By groupSelectButton	= By.cssSelector("#Campaign_DonorPlate_DonorGroupSelectionList_SelectButton");
		
		/** The userName. */
		By userName	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorSettings_UserNameTextBox");
		
		/** The password. */
		By password	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorSettings_PasswordTextBox");
		
		/** The confirmPasswordTextBox. */
		By confirmPasswordTextBox	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorSettings_ConfirmPasswordTextBox");
		
		/** The saveButton. */
		By saveButton	= By.cssSelector("#SaveButton");
		
		/** The filter. */
		By filter	= By.xpath("//input[@id='Campaign_DonorGrid_FilterTextBox']");
		
		/** The filter_Identifier. */
		By filter_Identifier	= By.xpath("//*[@id='Campaign_DonorGrid']/tbody/tr[2]/td[2]");
		
		/** The donor_expand. */
		By donor_expand = By.xpath("//a[contains(.,'Donors')]");
		By viewCompanyDonorSettings = By.id("Campaign_DonorPlate_Company_DonorEditHyperlink");
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CM_CAMPAIGN_CONFIGURE_DONOR_SETTING.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_CONFIGURE_DONOR_SETTING(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
					
				Thread.sleep(3000);
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */   
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
				click(donor_expand, "Expanding Donor Node under Campaigns");
				Thread.sleep(2000);
				
				} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {	
			
			try {
				Thread.sleep(4000);
				click(donorGroupIcon, "Settings icon displayed in grid");
				Thread.sleep(3000);
				verifyText(campaignSettingsTitle, (String)params.get("campaignSettingsTitle"), "Campaign Settings Page Title");
				click(donorGroupEditButton, "Pencile icon of Donor Group field");
				Thread.sleep(8000);
				clickOption(groupSelect, "Donor Group");
				Thread.sleep(3000);
				click(groupSelectButton, "Donor Group Select button");
				Thread.sleep(2000);
				type(userName, (String)params.get("userName"), "User Name");
				type(password, (String)params.get("password"), "Password");
				type(confirmPasswordTextBox, (String)params.get("confirmPasswordTextBox"), "Confirm Password");
				this.reporter.successwithscreenshot("User is able to enter password", "User is able to enter password",this.Driver);
				Thread.sleep(2000);
				click(saveButton, "Save / Update");
				Thread.sleep(3000);
				boolean validationPoint = isElementPresent(viewCompanyDonorSettings, "website list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Verify Donor setting should get saved", "Donor settings are saved.",this.Driver);
				}else{
					this.reporter.failureReport("Verify Donor setting should get saved", "Donor settings are not saved.", this.Driver);
				} 
			}catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}