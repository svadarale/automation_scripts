package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;




import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_CAMPAIGN_VERIFY_EMAIL.
 */
public class CM_CAMPAIGN_VERIFY_EMAIL extends ActionEngine implements Page {
		
		/** The email_expand. */
		By email_expand = By.xpath("//a[contains(.,'Email')]");
		
		/** The add_email_link. */
		By add_email_link	= By.xpath("//a[@id='CampaignPlate_EmailAddButton']");
		
		/** The delete_email_link. */
		By delete_email_link	= By.xpath("//a[@id='CampaignPlate_EmailSelectDeleteButton']");
		
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CAMPAIGN VERIFY EMAIL.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_VERIFY_EMAIL(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				Thread.sleep(2000);
				click(email_expand, "Expanding Email Node under Campaigns");
				Thread.sleep(3000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				isElementPresent(add_email_link, "Add a new email", true);
				isElementPresent(delete_email_link, "Delete selected email", true);
				boolean validationPoint = isElementPresent(delete_email_link, "Delete selected email link is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Is Delete selected email element present", "Delete selected email element is present",this.Driver);
				}else{
					this.reporter.failureReport("Is Delete selected email element present", "Delete selected email element is not present", this.Driver);
				}
				
				} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}