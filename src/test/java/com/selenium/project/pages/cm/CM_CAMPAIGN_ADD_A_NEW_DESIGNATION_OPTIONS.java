package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS  extends ActionEngine implements Page{
	
	/** Option set. */
	By Optionset = By.xpath("//a[contains(.,'Option Sets')]");
	
	/** Designation panel. */
	By Designation_panel = By.xpath(".//*[@id='OptionSetTabStrip_ctl10_ctl00']");
	
	/** Add a new designation option set. */
	By Add_a_new_designation_option_set = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetAddButton']");
	
	/** Designation Name . */
	By Designation_Name  = By.xpath(".//*[@id='OptionSetPlate_OptionSetNameTextBox']");

	/** Designation Description. */
	By Designation_Description = By.xpath(".//*[@id='OptionSetPlate_OptionSetDescriptionTextBox']");

	/** Designation Maximum Designation Count. */
	By Designation_Maximum_Designation_Count = By.xpath(".//*[@id='OptionSetPlate_MaximumDesignationCountTextBox']");

	/** Designation Maximum Designation Percent. */
	By Designation_Maximum_Designation_Percent = By.xpath(".//*[@id='OptionSetPlate_MaximumDesignationPercentTextBox']");

	/** Designation Option Amount. */
	By Designation_Option_Amount = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionAmountTypeDropDownList']");

	/** Designation Allow Negative Designation. */
	By Designation_Allow_Negative_Designation = By.xpath(".//*[@id='OptionSetPlate_AllowNegativeDesignationCheckBox']");

	/** Designation Allow Search IRS. */
	By Designation_Allow_Search_IRS = By.xpath(".//*[@id='OptionSetPlate_AllowSearchIRSCheckBox']");

	/** Designation Allow Write In Agency. */
	By Designation_Allow_Write_In_Agency = By.xpath(".//*[@id='OptionSetPlate_AllowWriteInAgencyCheckBox']");

	/** Designation Allow Matching Gift. */
	By Designation_Allow_Matching_Gift  = By.xpath(".//*[@id='OptionSetPlate_AllowMatchingGiftCheckBox']");

	/** Designation Allow Locate An Agency. */
	By Designation_Allow_Locate_An_Agency  = By.xpath(".//*[@id='OptionSetPlate_AllowLocateAnAgencyCheckBox']");
	
	/** Designation Allow Auto Allocation. */
	By Designation_Allow_Auto_Allocation  = By.xpath(".//*[@id='OptionSetPlate_AllowAutoAllocationCheckBox']");
	
	/** Designation Agency Search Mode. */
	By Designation_Agency_Search_Mode  = By.xpath(".//*[@id='OptionSetPlate_AgencySearchModeTypeDropDownList']");
	
	/** Designation Allow Special Instructions. */
	By Designation_Allow_Special_Instructions  = By.xpath(".//*[@id='OptionSetPlate_AllowSpecialInstructionsCheckBox']");
	
	/** Designation Allow Designation Recognition. */
	By Designation_Allow_Designation_Recognition = By.xpath(".//*[@id='OptionSetPlate_AllowDesignationRecognitionCheckBox']");
	
	/** Designation Display Donor Release. */
	By Designation_Display_Donor_Release  = By.xpath(".//*[@id='OptionSetPlate_AllowReleaseDonorDataCheckBox']");
	
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM CAMPAIGN ADD A NEW DESIGNATION OPTIONS.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS(WebDriver driver, CReporter reporter,Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		
		try {
			By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
			By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
			By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
			/** Clicking on Campaign Node  . */               
			Thread.sleep(5000);
			SwitchToFrame(By.name("tree"),"Tree Frame");
			Thread.sleep(5000);
			click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
			Thread.sleep(3000);
			click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
			Thread.sleep(3000);
			click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
			Thread.sleep(4000);
			click(Optionset, "Option set");
			Thread.sleep(5000);
			SwitchToFrame(By.name("content"), "option set Frame");
			} catch (Throwable e) 
		{
		
			e.printStackTrace();
		}

	}

	@Override
	public void move() {


		try {
			/** click the Designation panel  */
			click(Designation_panel, "Designation panel");
			click(Add_a_new_designation_option_set, "Add a new desigantion option set");
			SwitchToFrame(By.name("content"), "option set Frame");
			boolean DesignationName = isElementPresent(Designation_Name,"Designation Name", true);
			if(DesignationName){
				this.reporter.successwithscreenshot("User is able to verify add a new designation Name", "User is able to verify add a new designation Name",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new designation Name", "User is not able to verify add a new designation Name", this.Driver);
			}
			
			boolean DesignationDescription = isElementPresent(Designation_Description,"Designation Description", true);
			if(DesignationDescription){
				this.reporter.successwithscreenshot("User is able to verify add a new Designation Description", "User is able to verify add a new Designation Description",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new Designation Description", "User is not able to verify add a new Designation Description", this.Driver);
			}
			
			boolean DesignationMaximumDesignationCount = isElementPresent(Designation_Maximum_Designation_Count,"Designation Maximum Designation Count", true);
			if(DesignationMaximumDesignationCount){
				this.reporter.successwithscreenshot("User is able to verify add a new designation maximum designation count", "User is able to verify designation maximum designation count",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation maximum designation count", "User is not able to verify designation maximum designation count", this.Driver);
			}
			
			boolean DesignationOptionAmount = isElementPresent(Designation_Option_Amount,"Designation Option Amount", true);
			if(DesignationOptionAmount){
				this.reporter.successwithscreenshot("User is able to verify add a new desigantion option amount", "User is able to verify add a new desigantion option amount",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new desigantion option amount", "User is not able to verify add a new desigantion option amount", this.Driver);
			}
			
			boolean DesignationAllowNegativeDesignation = isElementPresent(Designation_Allow_Negative_Designation,"Designation Allow Negative Designation", true);
			if(DesignationAllowNegativeDesignation){
				this.reporter.successwithscreenshot("User is able to verify allow negative designation", "User is able to verify allow negative designation",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new desigantion option set", "User is not able to verify add a new desigantion option set", this.Driver);
			}
			
			boolean DesignationAllowSearchIRS = isElementPresent(Designation_Allow_Search_IRS,"Designation Allow Search IRS", true);
			if(DesignationAllowSearchIRS){
				this.reporter.successwithscreenshot("User is able to verify designation allow search IRS", "User is able to verify designation allow search IRS",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow search IRS", "User is not able to verify designation allow search IRS", this.Driver);
			}
			
			boolean DesignationAllowWriteInAgency = isElementPresent(Designation_Allow_Write_In_Agency,"Designation Allow Write In Agency", true);
			if(DesignationAllowWriteInAgency){
				this.reporter.successwithscreenshot("User is able to verify designation allow write in agency", "User is able to verify designation allow write in agency",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow write in agency", "User is not able to verify designation allow write in agency", this.Driver);
			}
			
			boolean DesignationAllowLocateAnAgency = isElementPresent(Designation_Allow_Locate_An_Agency,"Designation Allow Locate An Agency", true);
			if(DesignationAllowLocateAnAgency){
				this.reporter.successwithscreenshot("User is able to verify designation allow locate an agency", "User is able to verify designation allow locate an agency",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow locate an agency", "User is not able to verify designation allow locate an agency", this.Driver);
			}
			
			boolean DesignationAllowMatchingGift = isElementPresent(Designation_Allow_Matching_Gift,"Designation Allow Matching Gift", true);
			if(DesignationAllowMatchingGift){
				this.reporter.successwithscreenshot("User is able to verify designation allow matching gift", "User is able to verify designation allow matching gift",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow matching gift", "User is not able to verify designation allow matching gift", this.Driver);
			}
			
			boolean DesignationAllowAutoAllocation = isElementPresent(Designation_Allow_Auto_Allocation,"Designation Allow Auto Allocation", true);
			if(DesignationAllowAutoAllocation){
				this.reporter.successwithscreenshot("User is able to verify designation allow auto allocation", "User is able to verify designation allow auto allocation",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow auto allocation", "User is not able to verify designation allow auto allocation", this.Driver);
			}
			
			boolean DesignationAgencySearchMode = isElementPresent(Designation_Agency_Search_Mode,"Designation Agency Search Mode", true);
			if(DesignationAgencySearchMode){
				this.reporter.successwithscreenshot("User is able to verify designation agency search mode", "User is able to verify designation agency search mode",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation agency search mode", "User is not able to verify designation agency search mode", this.Driver);
			}
			
			boolean DesignationAllowSpecialInstructions = isElementPresent(Designation_Allow_Special_Instructions,"Designation Allow Special Instructions", true);
			if(DesignationAllowSpecialInstructions){
				this.reporter.successwithscreenshot("User is able to verify designation allow special instructions", "User is able to verify designation allow special instructions",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow special instructions", "User is not able to verify designation allow special instructions", this.Driver);
			}
			
			boolean DesignationAllowDesignationRecognition = isElementPresent(Designation_Allow_Designation_Recognition,"Designation Allow Designation Recognition", true);
			if(DesignationAllowDesignationRecognition){
				this.reporter.successwithscreenshot("User is able to verify designation allow designation recognition", "User is able to verify designation allow designation recognition",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation allow designation recognition", "User is not able to verify designation allow designation recognition", this.Driver);
			}
			
			boolean DesignationDisplayDonorRelease = isElementPresent(Designation_Display_Donor_Release,"Designation Display Donor Release", true);
			if(DesignationDisplayDonorRelease){
				this.reporter.successwithscreenshot("User is able to verify designation display donor release", "User is able to verify designation display donor release",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify designation display donor release", "User is not able to verify designation display donor release", this.Driver);
			}
			
			
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
	}
}
