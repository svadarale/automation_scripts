package com.selenium.project.pages.cm;


import java.awt.Robot;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;





import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;
import com.thoughtworks.selenium.webdriven.commands.KeyEvent;

/**
 * The Class CM_CAMPAIGN_VERIFY_IMPORT_PAGE.
 */
public class CM_CAMPAIGN_VERIFY_IMPORT_PAGE extends ActionEngine implements Page {
		
		/** The importing_expand. */
		By importing_expand = By.xpath("//a[contains(.,'Importing')]");
		
		/** The type_grid. */
		//By type_grid	= By.cssSelector("td.EnhancedDataGridHeader:nth-of-type(2)");
		By type_grid	= By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[1]/td[2]");
		
		/** The file_grid. */
		//By file_grid	= By.cssSelector("td.EnhancedDataGridHeader:nth-of-type(3)");
		By file_grid	= By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[1]/td[3]");
		
		/** The status_grid. */
		//By status_grid	= By.cssSelector("td.EnhancedDataGridHeader:nth-of-type(4)");
		By status_grid	= By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[1]/td[4]");
		
		/** The processData_grid. */
		//By processData_grid	= By.cssSelector("td.EnhancedDataGridHeader:nth-of-type(5)");
		By processData_grid	= By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[1]/td[5]");
		
		/** The action_grid. */
		//By action_grid	= By.cssSelector("td.EnhancedDataGridHeader:nth-of-type(6)");
		By action_grid	= By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[1]/td[6]");
		
		/** The addNewImportlink. */
		By addNewImportlink	= By.id("CampaignImportingPlate_AddImportTaskButton");
		
		/** The deleteSelectedImport. */
		By deleteSelectedImport	= By.cssSelector("#CampaignImportingPlate_SelectImportTaskDeleteButton");
		
		/** The refreshbutton. */
		By refreshbutton = By.cssSelector("#RefreshButton");
		
		/** The cancelReturnbutton. */
		By cancelReturnbutton = By.cssSelector("#CancelButton");
		
		/** The importType. */
		By importType = By.cssSelector("#CampaignImportingPlate_ImportClassDropDownList");
		
		/** The firstRowHeader. */
		By firstRowHeader = By.cssSelector("#CampaignImportingPlate_IsFirstRowHeaderCheckbox");
		
		/** The importFileBrowse. */
		By importFileBrowse = By.cssSelector("#CampaignImportingPlate_ImportFile");
		//By importFileBrowse = By.xpath("//td[text()='Import File']/following-sibling::td/input");
		//By importFileBrowse = By.id(".//*[@id='CampaignImportingPlate_ImportFile'][@name='CampaignImportingPlate$ImportFile']");
		
		/** The importSaveButton. */
		By importSaveButton = By.cssSelector("#SaveButton");
		
		/** The savedGridItem. */
		By savedGridItem = By.cssSelector(".EnhancedDataGridItem:nth-of-type(2)");
		
		/** The statusAssert. */
		By statusAssert = By.xpath(".//*[@id='CampaignImportingPlate_ImportTaskGrid']/tbody/tr[2]/td[4]");
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CAMPAIGN VERIFY IMPORTING PAGE
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_VERIFY_IMPORT_PAGE(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(10000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				click(importing_expand, "Expanding Importing Node under Campaigns");
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				isElementPresent(addNewImportlink, "Add a new Import", true);
				Thread.sleep(3000);
				isElementPresent(deleteSelectedImport, "Delete Selected Imports", true);
				Thread.sleep(3000);
				isElementPresent(type_grid, "Type in grid", true);
				Thread.sleep(3000);
				isElementPresent(file_grid, "File in grid", true);
				Thread.sleep(3000);
				isElementPresent(status_grid, "Status in grid", true);
				Thread.sleep(3000);
				isElementPresent(processData_grid, "Process Data in grid", true);
				Thread.sleep(3000);
				isElementPresent(action_grid, "Action in grid", true);
				Thread.sleep(2000);
				boolean validationPoint = isElementPresent(action_grid, "Action in grid", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("User is able to verify Action in grid", "User is able to verify Action in grid",this.Driver);
				}else{
					this.reporter.failureReport("User is not able to verify Action in grid", "User is not able to verify Action in grid", this.Driver);
				}	
				
				click(addNewImportlink, "Add a new Import");
				Thread.sleep(10000);
				selectBySendkeys(importType, (String)params.get("ImportType"), "Import Type");
				Thread.sleep(8000);
				if(!(firstRowHeader.findElement(Driver).isSelected())){
					click(firstRowHeader,"First Row Header");}
				Thread.sleep(6000);
				String filePath=System.getProperty("user.dir") + (String)params.get("Import_Upload.file");
				
				if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("chrome") || this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("firefox"))
	                  {
					Thread.sleep(2000);
					this.Driver.findElement(importFileBrowse).sendKeys(filePath);
	                }
				
				
				else if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("ie") || this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
	                {
					click(importFileBrowse, "importFileBrowse");
					Thread.sleep(4000);
					//Runtime.getRuntime().exec("C:/Users/IN02109/Desktop/fileupload.exe");
					type(importFileBrowse, filePath, "Browse the file");
					Thread.sleep(5000);
	                }
				/*
					tab(firstRowHeader, "firstRowHeader");
					mouseover(firstRowHeader, "firstRowHeader");
					new Robot().keyPress(java.awt.event.KeyEvent.VK_TAB);
					new Actions(this.Driver).moveToElement(this.Driver.findElement(importFileBrowse)).build().perform();
					//JSAcceptAlert();
	                click(importFileBrowse,"Browse");
	                browseAndUpload(filePath, importFileBrowse, "Import File");
	               	Runtime.getRuntime().exec("C:/Users/IN02109/Desktop/autoScript.exe");
	             	Runtime.getRuntime().exec("C:/Users/IN02109/Desktop/ashish/autoScript.exe");
	               }*/
				
	             else
	                {
			    Thread.sleep(4000);
	             browseAndUpload(filePath, importFileBrowse, "Import File");
	                }
				Thread.sleep(7000);
				click(importSaveButton, "Save / Update");
				Thread.sleep(5000);
				this.reporter.successwithscreenshot("User is able to verify status in grid", "User is able to verify status in grid",this.Driver);
				
				
				/*String string = getText(statusAssert, "Status");
				String[] parts = string.split("(");
				String part1 = parts[0]; 
				String part2 = parts[1];
				
			*/
				Thread.sleep(35000);
				click(refreshbutton, "Refresh");
				Thread.sleep(10000);
				click(refreshbutton, "Refresh");
				Thread.sleep(1000);
				this.reporter.successwithscreenshot("User is able to verify status in grid", "User is able to verify status in grid",this.Driver);
				
			//	verifyText(statusAssert, (String)params.get("status"), "Status");
				} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}