package com.selenium.project.pages.cm;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_ADD_CAMPAIGN  extends ActionEngine implements Page {
	
	/** The expand_camp. */
	By expand_camp = By.linkText("Campaigns");
	
	/** The camp_list. */
	By camp_list = By.xpath("//td[contains(.,'Campaign List')]");
	
	/** Add Campaign. */
	By AddCampaign  = By.xpath(".//*[@id='AddButton']");
	
	/** Organisation. */
	//By Organisation  = By.xpath(".//*[@id='CampaignPlate_OrganizationIdDropDownList']");
	By Organisation  = By.id("CampaignPlate_OrganizationIdDropDownList");
	
	/** Company. */
	By Company  = By.xpath(".//*[@id='CampaignPlate_CompanyIdDropDownList']");
	
	/** Campaign Name. */
	By CampaignName  = By.xpath(".//*[@id='CampaignPlate_NameTextBox']");
	
	/** Type. */
	By Type  = By.xpath(".//*[@id='CampaignPlate_CampaignTypeDropDownList']");
	
	/** Transaction Mode Type. */
	By TransactionModeType = By.xpath(".//*[@id='CampaignPlate_CampaignModeTypeDropDownList']");
	
	/** Campaign Number. */
	By CampaignNumber  = By.xpath(".//*[@id='CampaignPlate_NumberTextBox']");
	
	/** Campaign Code. */
	By CampaignCode  = By.xpath(".//*[@id='CampaignPlate_CodeTextBox']");
	
	/** Campaign Year. */
	By CampaignYear  = By.xpath(".//*[@id='CampaignPlate_CampaignYearDropDownList']");
	
	/** Country. */
	By Country  = By.xpath(".//*[@id='PlateHolder']/table[2]/tbody/tr[1]/td[2]");
	
	/** Address 1. */
	By Address1  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_Address1TextBox']");
	
	/** Address 2. */
	By Address2 = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_Address2TextBox']");
	
	/** City. */
	By City  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_CityTextBox']");
	
	/** Province/State. */
	By ProvinceState  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_StateDropDownList']");
	
	/** Postal Code. */
	By PostalCode  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_ZipCodeTextBox']");
	
	/** Postal Code1. */
	By PostalCode1  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_ZipCodeExtTextBox']");
	
	/** Phone 1. */
	By Phone1 = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_PhoneNumber1TextBox']");
	
	/** Phone 2. */
	By Phone2  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_PhoneNumber2TextBox']");
	
	/** Fax. */
	By Fax  = By.xpath(".//*[@id='CampaignPlate_AddressIdAddressTable_FaxNumberTextBox']");
	
	/** Save / Update. */
	By SaveUpdate = By.xpath(".//*[@id='SaveButton']");
	
	/** Cancel/Return. */
	By CancelReturn = By.xpath(".//*[@id='CancelButton']");
	
	/** Addcampaigntitle. */
	By Addcampaigntitle = By.xpath(".//*[@id='CampaignForm']/table/tbody/tr[1]/td[2]");
	
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a CM CAMPAIGN ADD CAMPAIGN.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
	
	public CM_CAMPAIGN_ADD_CAMPAIGN (WebDriver driver,CReporter reporter, Properties params)
	{
		setdriver(driver,reporter);
		this.params = params;
	}
	@Override
	public void fill() {
		try {
			SwitchToFrame(By.name("tree"),"Top NAV Frame");
			click(expand_camp, "Campaign");
			SwitchToFrame(By.name("content"),"Main Frame");
			click(AddCampaign,"Add Campaign");
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void move() {	
		try {
			/** verify the add campaign fields  */	
			isElementPresent(Organisation,"Organisation", true);
			isElementPresent(Company,"Comapny", true);
			isElementPresent(CampaignName,"Campaign Name",true);
			isElementPresent(Type,"Type",true);
			isElementPresent(TransactionModeType,"Transaction Mode Type",true);
			isElementPresent(CampaignNumber,"Campaign Number",true);
			isElementPresent(CampaignCode,"Campaign Code",true);
			isElementPresent(CampaignYear,"Campaign Year",true);
			isElementPresent(Country,"Country ",true);
			isElementPresent(Address1,"Address 1",true);
			isElementPresent(Address2,"Address 2",true);
			isElementPresent(City,"City",true);
			isElementPresent(ProvinceState,"Province State",true);
			isElementPresent(PostalCode,"Postal Code",true);
			isElementPresent(PostalCode1,"Postal Code",true);
			isElementPresent(Phone1,"Phone 1",true);
			isElementPresent(Phone2,"Phone 2",true);
			isElementPresent(Fax,"Fax",true);
			isElementPresent(SaveUpdate,"Save Update",true);
			isElementPresent(CancelReturn,"CancelButton",true);
			boolean validationPoint = isElementPresent(Addcampaigntitle, "Addcampaign title", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("User is able to verify Addcampaign title", "User is able to verify Addcampaign title",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify Addcampaign title", "User is not able to verify Addcampaign title", this.Driver);
			}			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}