package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;



import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_TRAFFIC_ICON.
 */
public class CM_ADMIN_VERIFY_TRAFFIC_ICON extends ActionEngine implements Page {
	
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe traffic icon.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_TRAFFIC_ICON(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
			
			try {				
			} catch (Throwable e) {	
				e.printStackTrace();
			}		
		}
		
		@Override
		public void move() {	
			try {
				Thread.sleep(1000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				/** The traffic_activate. */
				By traffic_activate = By.xpath("//td[text()='"+CM_ADMIN_CREATE_DONOR_GRPS.strname+"']//following::input[@alt='Activate Donor Group']");
				isElementVisible(traffic_activate, "Traffic Signal");
				waitForVisibilityOfElement(traffic_activate, "Traffic Signal");
				click(traffic_activate, "Clicking icon to Yes");
				Thread.sleep(3000);
				/** The traffic_yes. */
				By traffic_yes = By.xpath("//td[text()='"+CM_ADMIN_CREATE_DONOR_GRPS.strname+"']//following::td[contains(.,'Yes')]");		
				Thread.sleep(4000);
				boolean validationPoint = isElementPresent(traffic_yes, "Traffic icon value is yes", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Traffic icon value is yes", "Traffic icon value is yes",this.Driver);
				}else{
					this.reporter.failureReport("Traffic icon value is yes", "Traffic icon value is not yes", this.Driver);
				}
				/** The traffic_deactivate. */
				By traffic_deactivate = By.xpath("//td[text()='"+CM_ADMIN_CREATE_DONOR_GRPS.strname+"']//following::input[@alt='Deactivate Donor Group']");
				click(traffic_deactivate, "Click Icon to No");
				Thread.sleep(1000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}