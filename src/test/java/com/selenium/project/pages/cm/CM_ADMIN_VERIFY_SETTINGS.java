package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;






import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_SETTINGS.
 */
public class CM_ADMIN_VERIFY_SETTINGS extends ActionEngine implements Page {
			
		/** The campagins_expand. */
		By campagins_expand = By.xpath(".//*[@alt='Expand Campaigns']");
		
		/** The campagins_ a_expand. */
		By campagins_A_expand = By.xpath(".//*[@alt='Expand A']"); 
		
		/** The acme_expand. */
		By acme_expand = By.xpath(".//*[@alt='Expand Acme New 2 [AcmeNew2]']"); 
		
		/** The settings_expand. */
		By settings_expand = By.xpath("//a[contains(.,'Settings')]");
		
		/** The default_donorgrps. */
		By default_donorgrps	= By.xpath(".//*[@id='CampaignPlate_DonorGroupEditButton']");
		
		/** The donorgrps_option. */
		By donorgrps_option = By.xpath("//select[@id='CampaignPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList_ItemsSelect']");
		
		/** The select_button. */
		By select_button = By.xpath("//input[@id='CampaignPlate_DonorGroupSelectionList_SelectButton']");
		
		/** The conf_password. */
		By conf_password = By.xpath("//input[@id='CampaignPlate_ConfirmPasswordTextBox']");
		
		/** The save_btn. */
		By save_btn = By.xpath("//a[@id='SaveButton']");
		
		By defaultDonorGroup = By.xpath(".//td[contains(text(),'Default Donor Group')]//following-sibling::td");
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe settings.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_SETTINGS(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
					By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
					By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
					By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
					/** Clicking on Campaign Node  . */               
					Thread.sleep(5000);
					SwitchToFrame(By.name("tree"),"Tree Frame");
					Thread.sleep(5000);
					click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
					Thread.sleep(3000);
					click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
					Thread.sleep(3000);
					click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
					Thread.sleep(4000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {
				try {
				Thread.sleep(1000);
				click(settings_expand, "Clicking on Settings");
				Thread.sleep(1000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(1000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }*/
				click(default_donorgrps, "Default donor group icon");
				Thread.sleep(2000);
				selectBySendkeys(donorgrps_option, (String)params.get("option_value"), "option_value");
				Thread.sleep(2000);
				click(select_button, "Click select button");
				Thread.sleep(2000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToBottom();
			     }*/
				type(conf_password, (String)params.get("opps.password"), "Confirm Password");
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					js.executeScript("window.scrollBy(0,1000)", "");
			     }*/
				click(save_btn, "Click on save button");
				Thread.sleep(3000);
				String defaultDonorGroupvalue = getText(defaultDonorGroup, "website list is present");
				if(defaultDonorGroupvalue.contains((String)params.get("option_value"))){
					this.reporter.successwithscreenshot("Verify default donor group for the respective campaign", "Selected donor group should be saved as default donor group for the respective campaign",this.Driver);
				}else{
					this.reporter.failureReport("Verify default donor group for the respective campaign", "Selected donor group not saved as default donor group for the respective campaign", this.Driver);
			}
				} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
}
		
	