package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;



import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_DONOR_GROUP.
 */
public class CM_ADMIN_VERIFY_DESIGNATION_PANEL_IN_ORG extends ActionEngine implements Page {
			
		/** The organization_expand. */
		By organization_expand = By.xpath(".//*[@alt='Expand Organizations']");
		
		/** The organization_ a_expand. */
		By organization_A_expand = By.xpath(".//*[@alt='Expand A']"); 
		
		/** The arog_expand. */
		By aorg_expand = By.xpath(".//*[@alt='Expand Asorg2']"); 
		
		/** The desig_panels_expand. */
		By desig_panels_expand = By.xpath("//a[contains(.,'Designation Panels')]");
		
		/** The desig_panel_list. */
		//By desig_panel_list	= By.xpath("//td[contains(.,'Designation Panel List (Asorg2)')]");
		By desig_panel_list	= By.xpath("//td[contains(.,'Designation Panel List (Aloha United Way)')]");
		//Changed locator to execute in Stage Environment
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe donor group.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_DESIGNATION_PANEL_IN_ORG(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
					By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
					By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
					By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
					/** Clicking on Campaign Node  . */
					Thread.sleep(3000);
					SwitchToFrame(By.name("tree"),"Tree Frame");
					Thread.sleep(3000);
					click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
					Thread.sleep(3000);
					click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
					Thread.sleep(3000);
					click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
					Thread.sleep(3000);
			
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {
				try {
				Thread.sleep(2000);
				click(desig_panels_expand, "Expanding Designation Panels Node under Campaigns");
				Thread.sleep(5000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				boolean validationPoint = isElementPresent(desig_panel_list, "optionsets_list list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("optionsets_list element is present", "optionsets_list element is present",this.Driver);
				}else{
					this.reporter.failureReport("optionsets_list element is present", "optionsets_list element is not present", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}
