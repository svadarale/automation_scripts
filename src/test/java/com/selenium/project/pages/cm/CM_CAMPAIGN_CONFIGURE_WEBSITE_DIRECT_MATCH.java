package com.selenium.project.pages.cm;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;






import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH.
 */
public class CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH extends ActionEngine implements Page {
		
		/** The websites_expand. */
		By websites_expand = By.xpath("//a[contains(.,'Websites')]");
		
		/** The website_list. */
		By website_list	= By.xpath("//td[contains(.,'Website List')]");
		
		/** The add_new_website_link. */
		By add_new_website_link = By.cssSelector("#AddButton");
		
		/** The content_icon. */
		By content_icon = By.cssSelector(".EnhancedDataGridItem [alt='View / Edit Content']");
		
		/** The content_tab. */
		//By content_tab = By.xpath("//a[contains(.,'Content')]");
		//By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00");
		By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00_Image");
		
		/** The dropdownField. */
		By dropdownField = By.cssSelector("#WebsitePlate_ContentPlateDropDownList_ContentPlateDropDownList");
		
		/** The confirmationEmail. */
		By confirmationEmail = By.cssSelector("#WebsitePlate_ConfirmationEmailActive_CheckBox");
	
		/** The confirmation_fromName. */
		By confirmation_fromName = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_FromNameTextBox");
		
		/** The confirmation_fromAddress. */
		By confirmation_fromAddress = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_FromAddressTextBox");
		
		/** The confirmation_replyAddress. */
		By confirmation_replyAddress = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_ReplyAddressTextBox");
		
		/** The confirmation_format. */
		By confirmation_format = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_MailFormatTypeDropDownList");
		
		/** The confirmation_subject. */
		By confirmation_subject = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_SubjectTextBox");
		
		/** The confirmation_textBody. */
		By confirmation_textBody = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_BodyTextBox");
		
		/** The verificationEmail. */
		By verificationEmail = By.cssSelector("#WebsitePlate_VerificationEmailActive_CheckBox");
		
		/** The verification_fromName. */
		By verification_fromName = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_FromNameTextBox");
		
		/** The verification_fromAddress. */
		By verification_fromAddress = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_FromAddressTextBox");
		
		/** The verification_replyAddress. */
		By verification_replyAddress = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_ReplyAddressTextBox");
		
		/** The verification_subject. */
		By verification_subject = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_SubjectTextBox");
		
		/** The verification_format. */
		By verification_format = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_MailFormatTypeDropDownList");
		
		/** The verification_textBody. */
		By verification_textBody = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_BodyTextBox");

		/** The save_button. */
		By save_button = By.cssSelector("#SaveButton");
		
		/** The filter. */
		By filter = By.cssSelector("#WebsiteGrid_FilterTextBox");
		
		/** The filter_Identifier. */
		By filter_Identifier = By.xpath(".//*[@id='WebsiteGrid']/tbody/tr[2]/td[1]");
		

		

						
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new Configure Website Content for "Direct Match".
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
						try {
		By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
		By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
		By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
		/** Clicking on Campaign Node  . */               
		Thread.sleep(5000);
		SwitchToFrame(By.name("tree"),"Tree Frame");
		Thread.sleep(5000);
		click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
		Thread.sleep(3000);
		click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
		Thread.sleep(3000);
		click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
		Thread.sleep(4000);
		click(websites_expand, "websites");
		Thread.sleep(5000);
	
			} catch (Throwable e) {
				e.printStackTrace();
			}	
		}
		
		@Override
		public void move() {
			try {
				
				isElementPresent(website_list, "website list", true);
				Thread.sleep(2000);
				isElementPresent(add_new_website_link, "Add new website link", true);
				Thread.sleep(8000);
				click(content_icon, "edit icon");
				//Thread.sleep(60000);
				//waitForElementPresent(content_tab, "content tab", 250000);
				//JSClick(content_tab, "content tab");
				//click(content_tab, "content tab");
				Thread.sleep(8000);	
				selectBySendkeys(dropdownField, (String)params.get("directMatchDropdown"), "Website");
				Thread.sleep(2000);
				this.reporter.successwithscreenshot("Is direct Match Dropdown is selected", "direct Match Dropdown is selected",this.Driver);
				if(!(confirmationEmail.findElement(Driver).isSelected())){
				click(confirmationEmail,"Selecting Confirmation Email Active checkbox");}
				Thread.sleep(5000);
				type(confirmation_fromName, (String)params.get("confirmation_fromName"), "Confirmation Email From Name");
				type(confirmation_fromAddress, (String)params.get("confirmation_fromAddress"), "Confirmation Email  From Address");
				type(confirmation_replyAddress, (String)params.get("confirmation_replyAddress"), "Confirmation Email  Reply Address");
				type(confirmation_subject, (String)params.get("confirmation_subject"), "Subject");
				selectBySendkeys(confirmation_format, (String)params.get("confirmation_format"), "Format");
				Thread.sleep(5000);
				javascriptSendKeys((String)params.get("confirmation_textBody"), this.Driver.findElement(confirmation_textBody), "Confirmation TextBody");
				Thread.sleep(5000);
				if(!(verificationEmail.findElement(Driver).isSelected())){
				click(verificationEmail,"Selecting Verification Email Active checkbox");}
				Thread.sleep(9000);
				type(verification_fromName, (String)params.get("verification_fromName"), "Verification Email From Name");
				Thread.sleep(1000);
				type(verification_fromAddress, (String)params.get("verification_fromAddress"), "Verification Email  From Address");
				Thread.sleep(1000);
				type(verification_replyAddress, (String)params.get("verification_replyAddress"), "Verification Email  Reply Address");
				Thread.sleep(1000);
				type(verification_subject, (String)params.get("verification_subject"), "Subject");
				Thread.sleep(1000);
				selectBySendkeys(verification_format, (String)params.get("verification_format"), "Format");
				Thread.sleep(5000);
				javascriptSendKeys((String)params.get("verification_textBody"), this.Driver.findElement(verification_textBody), "Verification TextBody");
				Thread.sleep(4000);
				click(save_button, "Save / Update"); 
				Thread.sleep(2000);
				
				} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}