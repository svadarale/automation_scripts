package com.selenium.project.pages.cm;

import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

// TODO: Auto-generated Javadoc

/**
 * The Class CM__Add_Delete_Domain.
 */
public class CM__Add_Delete_Domain extends ActionEngine implements Page {

	/** The expand_ campaign. */
	By expand_Companies = By.xpath(".//*[@alt='Expand Companies']");

	/** The expand_ campaign. */
	By expand_Campaign = By.xpath(".//*[@alt='Expand Campaigns']");

	/** The lnk settings. */
	By lnkSettings = By.xpath("//a[text()='Settings']");

	/** The lnk donors. */
	By lnkDonors = By.xpath("//a[text()='Donors']");

	/** The lnk add new domain. */
	By lnkAddNewDomain = By.id("CompanyPlate_CompanyDomainAddButton");

	/** The lbl domain. */
	By lblDomain=By.xpath("//td[text()='Domain']");

	/** The txtbox domain name. */
	By txtboxDomainName = By.id("CompanyPlate_DomainTextBox");

	/** The btn save. */
	By btnSave=By.id("SaveButton");

	/** The btn impersonate. */
	By btnImpersonate=By.cssSelector("input[id$='ImpersonateButton']");

	/** The lnk dash board. */
	By lnkDashBoard =By.xpath("//a[text()='Dashboard']");

	/** The filter box. */
	By filterBox=By.cssSelector("input[id$='FilterTextBox']");

	/** The button send email. */
	By buttonSendEmail=By.cssSelector("a[id$='SendEmailButton']");

	/** The dropdown domain. */
	By dropdownDomain=By.cssSelector("select[id$='CompanyDomainDropDownList']");
	/** The button delete. */
	By buttonDelete=By.cssSelector("input[id$='DeleteButton']");

	/** The lbl no optionset element. */
	By lblNoDomainNameElement = By.xpath("//td[contains(text(),'none')]");

	/** The params. */
	Properties params = new Properties();

	/** The domain name. */
	public String domainName=null;
	
	/**
	 * Instantiates a new c m_ add_ delete_ batch.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM__Add_Delete_Domain(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#fill()
	 */
	@Override
	public void fill() {
		try {

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#move()
	 */
	@Override
	public void move() {

		try {

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}


	/**
	 * Navigate_ company.
	 */
	public void navigate_Company() {
		try {

			Thread.sleep(3000);
			/*if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}*/
			/** The expand_s. */
			By expand_S =By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CompanyInitial")+"']");

			/** The expand_sprint16_campaignclassic1. */
			By expand_Company = By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CampanyName")+"']");
			if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}
			waitForVisibilityOfElement(By.name("tree"),"Tree Frame");
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(expand_Companies,"expand_Companies");
			JSClick(expand_Companies, "expand_Companies");
			waitForVisibilityOfElement(expand_S,"Expand S icon");
			JSClick(expand_S, "Expand S icon");
			waitForVisibilityOfElement(expand_Company,"Expand Sprint16Company");
			JSClick(expand_Company, "Expand Sprint16Company");
			waitForVisibilityOfElement(lnkSettings," Settings");
			JSClick(lnkSettings, " Settings");

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Navigate_ campaign.
	 */
	public void navigate_Campaign() {
		try {

			/** The expand_s. */
			By expand_s =By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CompaignInitial")+"']");
			/** The expand_sprint16_campaignclassic1. */
			By expand_CampaignName = By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CampaignName")+"']");

			Thread.sleep(3000);
			if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}
			waitForVisibilityOfElement(By.name("tree"),"Tree Frame");
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(expand_Campaign,"Expand_Campaign");
			JSClick(expand_Campaign, "Expand_Campaign");
			waitForVisibilityOfElement(expand_s,"Expand S icon");
			JSClick(expand_s, "Expand S icon");
			waitForVisibilityOfElement(expand_CampaignName,"Expand Sprint18CampaignClassic1s");
			JSClick(expand_CampaignName, "Expand Sprint18CampaignClassic1");

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	/**
	 * Creates the domain.
	 *
	 * @return the string
	 */
	public String createDomain() {
		try {
			SwitchToFrame(By.name("content"), "content frame");
			waitForVisibilityOfElement(lnkAddNewDomain,"Add New Domain",120);
			if(isElementPresent(lnkAddNewDomain, "Add New Domain ", true))
				this.reporter.successwithscreenshot("Verify Company Settings Page", "User is able to navigate to Company Settings Page", this.Driver);
			else
				this.reporter.failureReport("Verify Company Settings Page", "Failed to navigate Company Settings Page ", this.Driver);
			JSClick(lnkAddNewDomain, "Add New Domain");
			waitForVisibilityOfElement(lblDomain,"Domain Name",120);
			if(isElementPresent(lblDomain, "Domain Name ", true))
				this.reporter.successwithscreenshot("Verify Domain Page", "User is able to navigate to Domain Page", this.Driver);
			else
				this.reporter.failureReport("Verify Domain Page", "Failed to navigate Domain Page ", this.Driver);
			domainName=this.params.getProperty("DomainName")+generateRandomString(3, Mode.NUMERIC)+".com";
			type(txtboxDomainName,domainName," Domain Name");
			JSClick(btnSave,"Save Button");
			By verify_DomainInGrid=By.xpath("//td[text()='"+domainName+"']");
			waitForVisibilityOfElement(verify_DomainInGrid," Domain Name In Grid",120);
			if(isElementPresent(verify_DomainInGrid, "Domain Name In Grid ", true))
				this.reporter.successwithscreenshot("Verify Domain Name In Grid", "Domain Name is created in Domain Grid", this.Driver);
			else
				this.reporter.failureReport("Verify Domain Name In Grid", "Failed to create Domain Name in Grid ", this.Driver);

		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return domainName;

	}

	/**
	 * Verify domain name in opcs.
	 *
	 * @param domainname the domainname
	 */
	public void verifyDomainNameInOPCS(String domainname) {
		try {
			waitForVisibilityOfElement(lnkDonors,"Donors");
			JSClick(lnkDonors, "Donors");
			SwitchToFrame(By.name("content"), "content frame");
			String donorName=this.params.getProperty("donorName");
			By donorNameInTable =By.xpath("//td[text()='"+donorName+"']");
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,donorName);
			ScrollToElement(btnImpersonate);
			click(btnImpersonate," Impersonate Button");
			windowHandles(this.Driver);
			waitForSeconds(10);
			waitForElementPresent(lnkDashBoard, "DashBoard", 120);
			JSClick(lnkDashBoard," DashBoard");
			waitForElementPresent(buttonSendEmail, "Send Email link", 120);
			ScrollToElementVisible(buttonSendEmail);
			JSClick(buttonSendEmail," Send Email");
			boolean flagDomainName=verifypartialValueInList(dropdownDomain,domainname,"Domain Name");
			if(flagDomainName)
				this.reporter.successwithscreenshot("Verify Domain Name in Grid", "Domain Name " +domainname+ " added in domain drop down successfully", this.Driver);
			else
				this.reporter.failureReport("Verify Domain Name in Grid", "Domain Name not created ", this.Driver);
			
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	/**
	 * Delete domain name.
	 *
	 * @param domainname the domainname
	 */
	public void deleteDomainName(String domainname) {
		try {
			SwitchToFrame(By.name("content"), "content frame");
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,domainname );
			waitForElementToBoClickable(buttonDelete, " Delete Domain Name", 120);
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(buttonDelete, " Delete Domain Name");
				Thread.sleep(3000);
				AlertAccept();
			}
			else{
				JSAcceptAlert();
				Thread.sleep(3000);
				JSClick(buttonDelete, " Delete Domain Name");
			}
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,domainname );
			waitForElementToBoClickable(lblNoDomainNameElement, "No Domain Name in Grid", 120);
			if(isElementPresent(lblNoDomainNameElement, "No Domain Name in Grid", true))
				this.reporter.successwithscreenshot(" Domain Name  Deletion", "Domain Name  deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Domain Name  Deletion", "Domain Name  not deleted ", this.Driver);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

}
