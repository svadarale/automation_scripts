package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;




import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_DONOR_GROUP.
 */
public class CM_ADMIN_VERIFY_DONOR_GROUP extends ActionEngine implements Page {
			
		/** The campagins_expand. */
		By campagins_expand = By.xpath(".//*[@alt='Expand Campaigns']");
		
		/** The campagins_ a_expand. */
		By campagins_A_expand = By.xpath(".//*[@alt='Expand A']"); 
		
		/** The acme_expand. */
		By acme_expand = By.xpath(".//*[@alt='Expand Acme New 2 [AcmeNew2]']"); 
		
		/** The donorgrps_expand. */
		By donorgrps_expand = By.xpath("//a[contains(.,'Donor Groups')]");
		
		/** The donorgrps_list. */
		By donorgrps_list	= By.xpath("//td[contains(.,'Donor Group List')]");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe donor group.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_DONOR_GROUP(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {
						
			try {
				Thread.sleep(2000);
				click(donorgrps_expand, "Expanding Websites Node under Campaigns");
				Thread.sleep(6000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }*/
				boolean validationPoint = isElementPresent(donorgrps_list, "website list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("donor group  list is present", "website list element is present",this.Driver);
				}else{
					this.reporter.failureReport("donor group  list is not present", "website list element is not present", this.Driver);
				}
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}