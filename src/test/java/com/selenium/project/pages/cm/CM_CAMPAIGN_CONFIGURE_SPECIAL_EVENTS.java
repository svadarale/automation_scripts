package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;
















import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_CAMPAIGN_CONFIGURE_SPECIAL_EVENTS.
 */
public class CM_CAMPAIGN_CONFIGURE_SPECIAL_EVENTS extends ActionEngine implements Page {
		
		/** The special_event_expand. */
		By special_event_expand = By.xpath("//a[contains(.,'Special Events')]");
		
		/** The special_events_title. */
		By special_events_title= By.id("SpecialEventTabStrip_ctl02_ctl00");
		
		/** The event_name_grid. */
		By event_name_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(text(),'Event Name')]");
		
		
		/** The description_grid. */
		By description_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(text(),'Description')]");
		
		/** The event_date_grid. */
		By event_date_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(text(),'Event Date')]");
		
		/** The beneficiary_grid. */
		By beneficiary_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(.,'Beneficiary')]");
		
		/** The total_dollars_raised_grid. */
		By total_dollars_raised_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(text(),'Total Dollars Raised')]");
		
		/** The event_cost_grid. */
		By event_cost_grid = By.xpath("//a[@class='EnhancedDataGridHeader'][contains(text(),'Event Cost')]");
		
		/** The action_grid. */
		By action_grid = By.xpath("//*[@class='EnhancedDataGridHeader'][contains(text(),'Action')]");
		
		/** The add_special_event_link. */
		By add_special_event_link	= By.cssSelector("#SpecialEventPlate_AddSpecialEventButton");
		
		/** The delete_special_event_link. */
		By delete_special_event_link = By.cssSelector("#SpecialEventPlate_DeleteSpecialEventButton");
		
		/** The eventName. */
		By eventName= By.cssSelector("#SpecialEventPlate_EventNameTextBox");
		
		/** The description. */
		By description= By.cssSelector("#SpecialEventPlate_DescriptionTextBox");
		
		/** The beneficiary. */
		By beneficiary= By.cssSelector("#SpecialEventPlate_BeneficiaryTextbox");
		
		/** The totalDollarsRaised. */
		By totalDollarsRaised= By.cssSelector("#SpecialEventPlate_TotalDollarsRaisedTextbox");
		
		/** The eventDate. */
		By eventDate= By.cssSelector("#SpecialEventPlate_EventDateTextbox");
		
		/** The eventCost. */
		By eventCost= By.cssSelector("#SpecialEventPlate_EventCostTextbox");
		
		/** The saveButton. */
		By saveButton= By.cssSelector("#SaveButton");
		
		/** The filter. */
		By filter= By.cssSelector("#SpecialEventPlate_SpecialEventGrid_FilterTextBox");
		
		
		/** The filter_column. */
		By filter_column = By.xpath("//*[@id='SpecialEventPlate_SpecialEventGrid']/tbody/tr[2]/td[2]");
		
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CM_CAMPAIGN_CONFIGURE_SPECIAL_EVENTS
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_CONFIGURE_SPECIAL_EVENTS(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(5000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(5000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(5000);
				click(special_event_expand, "Expanding Special Event Node under Campaigns");
				Thread.sleep(3000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(6000);
				isElementPresent(special_events_title, "Special Events", true);
				Thread.sleep(1000);
				isElementPresent(event_name_grid, "Event Name in grid", true);
				Thread.sleep(1000);
				isElementPresent(description_grid, "Description in grid", true);
				Thread.sleep(1000);
				isElementPresent(event_date_grid, "Event Date in grid", true);
				Thread.sleep(1000);
				isElementPresent(beneficiary_grid, "beneficiary in grid", true);
				Thread.sleep(1000);
				isElementPresent(total_dollars_raised_grid, "Total Dollars Raised in grid", true);
				Thread.sleep(1000);
				isElementPresent(event_cost_grid, "Event Cost in grid", true);
				Thread.sleep(1000);
				isElementPresent(action_grid, "Action in grid", true);
				Thread.sleep(1000);
				isElementPresent(add_special_event_link, "Add a new Special Event", true);
				Thread.sleep(1000);
				isElementPresent(delete_special_event_link, "Delete selected special events", true);
				Thread.sleep(1000);
				click(add_special_event_link, "Add a new Special Event");
				Thread.sleep(3000);
				String strEventName = (String) params.get("eventName");
				String strToPass = strwithtimestamp(strEventName);
			    Thread.sleep(5000);
			    type(eventName, strToPass, "Event Name");
			    type(description, (String)params.get("description"), "Description");
			    type(beneficiary, (String)params.get("beneficiary"), "Beneficiary");
			    type(totalDollarsRaised, (String)params.get("totalDollarsRaised"), "Total Dollars Raised");
			     type(eventDate, (String)params.get("eventDate"), "Event Date");
			    click(saveButton, "Save / Update");
			    Thread.sleep(5000);
			    FilterCheck(filter,filter_column,strToPass);
			    Thread.sleep(3000);
			    
				} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}