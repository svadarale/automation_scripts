package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;





import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_SAVE_REGISTRATION.
 */
public class CM_ADMIN_VERIFY_SAVE_REGISTRATION extends ActionEngine implements Page {

		/** The home_active_chk. */
		By home_active_chk = By.xpath("//*[@id='CampaignPlate_HomeAddressIsActiveCheckBox']");
		
		/** The work_active_chk. */
		By work_active_chk =  By.xpath("//*[@id='CampaignPlate_WorkAddressIsActiveCheckBox']"); 
		
		/** The home_contact_info_check. */
		By home_contact_info_check = By.xpath("//*[@id='CampaignPlate_HomeContactIsActiveCheckBox']");
		
		/** The work_contact_info_check. */
		By work_contact_info_check = By.xpath("//*[@id='CampaignPlate_WorkContactIsActiveCheckBox']");
		
		/** The label_txt. */
		By label_txt = By.xpath("//*[@id='CampaignPlate_CustomRegistrationField1LabelTextBox']");
		
		/** The default_value_txt. */
		By default_value_txt = By.xpath("//*[@id='CampaignPlate_CustomRegistrationField1DefaultValueTextBox']");
		
		/** The validation_opt. */
		By validation_opt = By.xpath("//*[@id='CampaignPlate_CustomRegistrationField1ValidatorTypeDropDownList']");
		
		/** The reg_active_chk. */
		By reg_active_chk = By.xpath("//*[@id='CampaignPlate_CustomRegistrationField1IsActiveCheckBox']");
		
		/** The employee_no_chk. */
		By employee_no_chk = By.xpath("//*[@id='CampaignPlate_ExtrinsicWorkFieldsIsActiveCheckBox']");
		
		/** The prefix_chk. */
		By prefix_chk = By.xpath("//*[@id='CampaignPlate_ExtrinsicNameFieldsIsActiveCheckBox']");
		
		/** The save_btn. */
		By save_btn = By.xpath("//*[@id='SaveButton']");

	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe save registration.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_SAVE_REGISTRATION(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				Thread.sleep(5000);
				SwitchToFrame(By.name("content"),"Main Content Frame");
				Thread.sleep(6000);
				WebDriverWait wait = new WebDriverWait(this.Driver, 300);
				wait.until(ExpectedConditions.presenceOfElementLocated(home_active_chk));
				if(!(home_active_chk.findElement(Driver).isSelected())){
					click(home_active_chk,"Activate Home Address Checkbox");}
				//click(home_active_chk, "Activete Home Address Checkbox");
				Thread.sleep(1000);
				if(!(work_active_chk.findElement(Driver).isSelected())){
					click(work_active_chk,"Activate Work Address Checkbox");}
				//click(work_active_chk, "Activete Work Address Checkbox");
				Thread.sleep(1000);
				if(!(home_contact_info_check.findElement(Driver).isSelected())){
					click(home_contact_info_check,"Activete Home Contact info Checkbox");}
				//click(home_contact_info_check, "Activete Home Contact info Checkbox");
				Thread.sleep(1000);
				if(!(work_contact_info_check.findElement(Driver).isSelected())){
					click(work_contact_info_check,"Activete work Contact info Checkbox");}
				//click(work_contact_info_check, "Activete work Contact info Checkbox");
				Thread.sleep(1000);
				type(label_txt, (String)params.get("label_txt"), "label_txt");
				Thread.sleep(1000);
				type(default_value_txt, (String)params.get("default_value_txt"), "default_value_txt");
				Thread.sleep(1000);
				selectBySendkeys(validation_opt, (String)params.get("validation_opt"), "validation_opt");
				Thread.sleep(1000);
				if(!(reg_active_chk.findElement(Driver).isSelected())){
					click(reg_active_chk,"Activete reg_active_chk Checkbox");}
				//click(reg_active_chk, "Activete reg_active_chk Checkbox");
				Thread.sleep(1000);
				if(!(employee_no_chk.findElement(Driver).isSelected())){
					click(employee_no_chk,"Activete employee_no_chk Checkbox");}
				//click(employee_no_chk, "Activete employee_no_chk Checkbox");
				Thread.sleep(5000);
				ScrollToBottom();
				if(!(prefix_chk.findElement(Driver).isSelected())){
					click(prefix_chk,"Activete prefix_chk Checkbox");}
			    //click(prefix_chk, "Activete prefix_chk Checkbox");
				Thread.sleep(1000);
			} catch (Throwable e) {
			e.printStackTrace();
			}
		}
		
		@Override
		public void move() {
				try {
				Thread.sleep(1000);
				WebDriverWait wait = new WebDriverWait(this.Driver, 300);
				wait.until(ExpectedConditions.presenceOfElementLocated(save_btn));
				click(save_btn, "Click on save_btn");
				Thread.sleep(1000);
				boolean validationPoint = isElementClickable(reg_active_chk, "website list is present");
				if(validationPoint){
					this.reporter.successwithscreenshot("Verify Home Address, Work Address, Home Contact Information and Work Contact Information fields should be active", "Donor registration fields are active",this.Driver);
				}else{
					this.reporter.failureReport("Verify Home Address, Work Address, Home Contact Information and Work Contact Information fields should be active", "Donor registration fields are not active", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}