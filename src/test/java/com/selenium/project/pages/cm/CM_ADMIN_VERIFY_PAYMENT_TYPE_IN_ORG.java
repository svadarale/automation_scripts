package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;





import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_DONOR_GROUP.
 */
public class CM_ADMIN_VERIFY_PAYMENT_TYPE_IN_ORG extends ActionEngine implements Page {
			
		/** The optionsets_expand. */
		By optionsets_expand = By.xpath(".//*[@alt='Expand Option Sets']");
		
		/** The donorgrps_expand. */
		By paytment_type = By.xpath("//a[contains(.,'Payment Type')]");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe donor group.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_PAYMENT_TYPE_IN_ORG(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		
		@Override
		public void fill() {
				try {
					By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
					By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
					By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
					/** Clicking on Organizations Node  . */               
					Thread.sleep(5000);
					SwitchToFrame(By.name("tree"),"Tree Frame");
					Thread.sleep(5000);
					click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under Organizations");
					Thread.sleep(5000);
					click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under Organizations");
					Thread.sleep(6000);
					click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under Organizations");
					Thread.sleep(6000);
			} catch (Throwable e) {
		      e.printStackTrace();
			}
		}

		@Override
		public void move() {				
			try {
				Thread.sleep(3000);
				click(optionsets_expand, "Expanding Websites Node under Organizations");
				Thread.sleep(3000);
				if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
				JavascriptExecutor js = (JavascriptExecutor) this.Driver;
			    js.executeScript("window.scrollBy(0,50)", "");
			     }
				click(paytment_type, "Expanding Websites Node under Organizations");
				Thread.sleep(3000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(3000);
				/** The optionsets_list. */
				By optionsets_list	= By.xpath("//td[contains(.,'Option Sets ("+(String)params.get("campaign.node3")+")')]");
				System.out.println(optionsets_list);
				if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }
				boolean validationPoint = isElementPresent(optionsets_list, "optionsets_list list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("optionsets_list element is present", "optionsets_list element is present",this.Driver);
				}else{
					this.reporter.failureReport("optionsets_list element is present", "optionsets_list element is not present", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}