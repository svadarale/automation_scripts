package com.selenium.project.pages.cm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_FILTER_DESIGNATIONOPTION  extends ActionEngine implements Page {
	
	/** Option set. */
	By Optionset = By.xpath("//a[contains(.,'Option Sets')]");
	
	/** Designation panel. */
	By Designation_panel = By.xpath(".//*[@id='OptionSetTabStrip_ctl10_ctl00']");
	
	/** Add a new designation option set. */
	By Add_a_new_designation_option_set = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetAddButton']");
	
	/** Designation Name . */
	By Designation_Name  = By.xpath(".//*[@id='OptionSetPlate_OptionSetNameTextBox']");
	
	/** Designation Agency Search Mode. */
	By Designation_Agency_Search_Mode  = By.xpath(".//*[@id='OptionSetPlate_AgencySearchModeTypeDropDownList']");
		
	/** Designation Save. */
	By Designation_Save  = By.xpath(".//*[@id='SaveButton']");
		
	/** Designation Filter. */
	By Designation_Filter  = By.cssSelector("[id*='FilterTextBox']");
	
	
	/** Designation Filter. */
	By Existing_Designation  = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetGrid']/tbody/tr[2]/td[1]");
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a  CM CAMPAIGN FILTER DESIGNATION OPTION.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_FILTER_DESIGNATIONOPTION(WebDriver driver, CReporter reporter,Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		
		try {
			By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
			By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
			By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
			/** Clicking on Campaign Node  . */               
			Thread.sleep(5000);
			SwitchToFrame(By.name("tree"),"Tree Frame");
			Thread.sleep(5000);
			click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
			Thread.sleep(3000);
			click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
			Thread.sleep(3000);
			click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
			Thread.sleep(4000);
			click(Optionset, "Option set");
			Thread.sleep(5000);
			SwitchToFrame(By.name("content"), "option set Frame");
			} catch (Throwable e) 
		{
		
			e.printStackTrace();
		}

	}

	@Override
	public void move() {
		try {
			/** click the Designation panel  */
			Thread.sleep(10000);
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(Designation_panel));
			click(Designation_panel, "Designation panel");
			Thread.sleep(3000);
			click(Add_a_new_designation_option_set, "Add a new desigantion option set");
			Thread.sleep(5000);
			String strdesignationname=strwithtimestamp((String) params.get("action.name"));
			type(Designation_Name,strdesignationname,"Designation Name");
			Thread.sleep(2000);
			selectByIndex(Designation_Agency_Search_Mode, 1, "agency search mode");
			Thread.sleep(2000);
			click(Designation_Save, "save");
			SwitchToFrame(By.name("content"), "Desigantion Frame");
			Thread.sleep(2000);
			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToBottom();
		     }*/
			type(Designation_Filter, strdesignationname, "Designation Name");
			tab(Designation_Filter, "presss the Enter key");
			Thread.sleep(5000);
			SwitchToFrame(By.name("content"), "Desigantion Frame");
			Thread.sleep(2000);
			String strexistingdesignation=getText(Existing_Designation, "");
			if (strdesignationname.contains(strexistingdesignation)) {
				boolean ExistingDesignation = isElementPresent(Existing_Designation,"Existing Designation", true);
				if(ExistingDesignation){
					this.reporter.successwithscreenshot("User is able to verify existing designation", "User is able to verify existing designation",this.Driver);
				}else{
					this.reporter.failureReport("User is not able to verify existing designation", "User is not able to verify existing designation", this.Driver);
				}
			}
			} catch (Throwable e) {
			
			e.printStackTrace();
		}
	}
}
