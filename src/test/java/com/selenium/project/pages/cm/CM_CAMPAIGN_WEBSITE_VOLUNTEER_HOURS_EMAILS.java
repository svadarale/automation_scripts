package com.selenium.project.pages.cm;

import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;















import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS.
 */
public class CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS extends ActionEngine implements Page {
		
		/** The websites_expand. */
		By websites_expand = By.xpath("//a[contains(.,'Websites')]");
		
		/** The website_list. */
		By website_list	= By.xpath("//td[contains(.,'Website List')]");
		
		/** The add_new_website_link. */
		By add_new_website_link = By.cssSelector("#AddButton");
		
		/** The content_icon. */
		By content_icon = By.cssSelector(".EnhancedDataGridItem [alt='View / Edit Content']");
		
		/** The content_tab. */
		//By content_tab = By.xpath("//a[contains(.,'Content')]");
		//By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00");
		By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00_Image");
		
		/** The dropdownField. */
		By dropdownField = By.cssSelector("#WebsitePlate_ContentPlateDropDownList_ContentPlateDropDownList");
		
		/** The confirmationEmail. */
		By confirmationEmail = By.cssSelector("#WebsitePlate_ConfirmationEmailActive_CheckBox");
	
		/** The confirmation_fromName. */
		By confirmation_fromName = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_FromNameTextBox");
		
		/** The confirmation_fromAddress. */
		By confirmation_fromAddress = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_FromAddressTextBox");
		
		/** The confirmation_replyAddress. */
		By confirmation_replyAddress = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_ReplyAddressTextBox");
		
		/** The confirmation_format. */
		By confirmation_format = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_MailFormatTypeDropDownList");
		
		/** The confirmation_subject. */
		By confirmation_subject = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_SubjectTextBox");
		
		/** The confirmation_textBody. */
		By confirmation_textBody = By.cssSelector("#WebsitePlate_ConfirmationEmailTemplate_EmailTable_BodyTextBox");
		
		/** The verificationEmail. */
		By verificationEmail = By.cssSelector("#WebsitePlate_VerificationEmailActive_CheckBox");
		
		/** The verification_fromName. */
		By verification_fromName = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_FromNameTextBox");
		
		/** The verification_fromAddress. */
		By verification_fromAddress = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_FromAddressTextBox");
		
		/** The verification_replyAddress. */
		By verification_replyAddress = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_ReplyAddressTextBox");
		
		/** The verification_subject. */
		By verification_subject = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_SubjectTextBox");
		
		/** The verification_format. */
		By verification_format = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_MailFormatTypeDropDownList");
		
		/** The verification_textBody. */
		By verification_textBody = By.cssSelector("#WebsitePlate_VerificationEmailTemplate_EmailTable_BodyTextBox");

		/** The save_button. */
		By save_button = By.cssSelector("#SaveButton");
		
	
		
						
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new Configure Website Content for volunteer_hour_email
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
						try {
		By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
		By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
		By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
		/** Clicking on Campaign Node  . */               
		Thread.sleep(5000);
		SwitchToFrame(By.name("tree"),"Tree Frame");
		Thread.sleep(5000);
		click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
		Thread.sleep(4000);
		click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
		Thread.sleep(4000);
		click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
		Thread.sleep(4000);
		click(websites_expand, "websites");
		Thread.sleep(6000);
	
			} catch (Throwable e) {
				e.printStackTrace();
			}	
		}
		@Override
		public void move() {
			try {
				
				//SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(2000);
				isElementPresent(website_list, "website list", true);
				Thread.sleep(4000);
				isElementPresent(add_new_website_link, "Add new website link", true);
				Thread.sleep(6000);
				click(content_icon, "edit icon");
				Thread.sleep(8000);
				//waitForElementPresent(content_tab, "content tab", 100000);
				//JSClick(content_tab, "content tab");
				//click(content_tab, "content tab");
				Thread.sleep(8000);	
				selectBySendkeys(dropdownField, (String)params.get("volunteerHourDropdown"), "");
				Thread.sleep(2000);
				this.reporter.successwithscreenshot("Is volunteer Hour Dropdown is selected", "volunteer Hour Dropdown is selected",this.Driver);
				if(!(confirmationEmail.findElement(Driver).isSelected())){
					click(confirmationEmail,"Selecting Confirmation Email Active checkbox");}
				Thread.sleep(5000);
				type(confirmation_fromName, (String)params.get("confirmation_fromName"), "Confirmation Email From Name");
				type(confirmation_fromAddress, (String)params.get("confirmation_fromAddress"), "Confirmation Email  From Address");
				type(confirmation_replyAddress, (String)params.get("confirmation_replyAddress"), "Confirmation Email  Reply Address");
				Thread.sleep(2000);
				type(confirmation_subject, (String)params.get("confirmation_subject"), "Subject");
				Thread.sleep(7000);
				selectBySendkeys(confirmation_format, (String)params.get("confirmation_format"), "Format");
				Thread.sleep(7000);
				selectBySendkeys(confirmation_format, (String)params.get("confirmation_format"), "Format");
				tab(confirmation_format, "Confirmation Format");
				Thread.sleep(9000);
				Robot robot = new Robot();
				robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_X);
			    robot.keyRelease(KeyEvent.VK_X);
			    robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_SPACE);
			    robot.keyRelease(KeyEvent.VK_SPACE);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_N);
			    robot.keyRelease(KeyEvent.VK_N);
			    robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_R);
			    robot.keyRelease(KeyEvent.VK_R);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_D);
			    robot.keyRelease(KeyEvent.VK_D);
			    Thread.sleep(3000);
			    //type(confirmation_textBody, (String)params.get("confirmation_textBody"), "Confirmation Email Text Body");Thread.sleep(2000);
			    if(!(verificationEmail.findElement(Driver).isSelected())){
					click(verificationEmail,"Selecting Verification Email Active checkbox");}
				Thread.sleep(5000);
				type(verification_fromName, (String)params.get("verification_fromName"), "Verification Email From Name");
				type(verification_fromAddress, (String)params.get("verification_fromAddress"), "Verification Email  From Address");
				type(verification_replyAddress, (String)params.get("verification_replyAddress"), "Verification Email  Reply Address");
				type(verification_subject, (String)params.get("verification_subject"), "Subject");
				Thread.sleep(4000);
				selectBySendkeys(verification_format, (String)params.get("verification_format"), "Format");
				Thread.sleep(8000);
				tab(verification_format, "Verification Format");
				Thread.sleep(2000);
				robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_X);
			    robot.keyRelease(KeyEvent.VK_X);
			    robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_SPACE);
			    robot.keyRelease(KeyEvent.VK_SPACE);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_N);
			    robot.keyRelease(KeyEvent.VK_N);
			    robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_R);
			    robot.keyRelease(KeyEvent.VK_R);
			    robot.keyPress(KeyEvent.VK_E);
			    robot.keyRelease(KeyEvent.VK_E);
			    robot.keyPress(KeyEvent.VK_D);
			    robot.keyRelease(KeyEvent.VK_D);
			    Thread.sleep(3000);
				//type(verification_textBody, (String)params.get("verification_textBody"), "Verification Email Text Body");Thread.sleep(2000);
				click(save_button, "Save / Update"); 
				Thread.sleep(4000);
				
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}