package com.selenium.project.pages.cm;

import java.util.Properties;

	import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;

import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

	import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

	/**
	 * The Class CM_ADMIN_LOGIN.
	 */
	public class CM_ADMIN_LOGIN extends ActionEngine implements Page {

		
		/** The User name. */
		By UserName  = By.xpath(".//*[@id='UserNameTextBox']");
		
		/** The Password. */
		By Password  = By.xpath(".//*[@id='PasswordTextBox']");
		
		/** The login button. */
		By loginButton = By.xpath(".//*[@id='LoginButton']");
		
		/** The login title. */
		By loginTitle = By.xpath(".//*[@href='http://support.united-e-way.org/']");
		
		/** The Organization. */
		By Organization = By.xpath(".//*[@id='NavigationTreeViewt1']");
		
		/** The Add new org. */
		By AddNewOrg =By.xpath(".//*[@id='AddButton']");
		
		
		
				
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin login.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_LOGIN(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
	
		@Override
		public void fill() {
			// TODO Auto-generated method stub
			try {
				
				/** Enter login details */
				waitForElementPresent(UserName, "UserName", 40);
				type(UserName, (String)params.get("opps.username"), "UserName");
				type(Password, (String)params.get("opps.password"), "Password");				
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}

	
		@Override
		public void move() {
			// TODO Auto-generated method stub
			
			try {
				
				JSClick(loginButton, "Login");
				Thread.sleep(10000);
				if(isVisibleOnly(loginButton, "Login button")) {
					waitForElementPresent(UserName, "UserName", 40);
					type(UserName, (String)params.get("opps.username"), "UserName");
					type(Password, (String)params.get("opps.password"), "Password");
					JSClick(loginButton, "Login");
				}
				/** verify title on next page */
				SwitchToFrame(By.name("topnav"),"Top NAV Frame");
				Thread.sleep(2000);
				WebDriverWait wait = new WebDriverWait(this.Driver, 300);
				wait.until(ExpectedConditions.presenceOfElementLocated(loginTitle));

				//this.Driver.switchTo().frame("topnav");	
				boolean validationPoint = isElementPresent(loginTitle, "QASeleniumAdmin", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("User is able to verify title", "User is able to verify title",this.Driver);
				}else{
					this.reporter.failureReport("User is not able to verify title", "User is not able to verify title", this.Driver);
				}				
				
			} catch (Throwable e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}
	
	


