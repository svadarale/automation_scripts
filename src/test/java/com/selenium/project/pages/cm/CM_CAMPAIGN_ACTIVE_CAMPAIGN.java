package com.selenium.project.pages.cm;


import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_ACTIVE_CAMPAIGN extends ActionEngine implements Page {
	public static String strcampaignnamecode;
	public static String strcampaigncode;
	public String strname ;
	/** The Campaigns. */
	By Campaigns = By.xpath(".//*[@id='NavigationTreeViewt3']");

	/** Add Campaign. */
	By AddCampaign = By.xpath(".//*[@id='AddButton']");

	/** Organisation. */
	By Organisation = By
			.xpath(".//*[@id='CampaignPlate_OrganizationIdDropDownList']");

	/** Comapny. */
	By Comapny = By.xpath(".//*[@id='CampaignPlate_CompanyIdDropDownList']");

	/** Campaign Name. */
	By CampaignName = By.xpath(".//*[@id='CampaignPlate_NameTextBox']");
	
	/** Type. */
	By Type = By.xpath(".//*[@id='CampaignPlate_CampaignTypeDropDownList']");

	/** Campaign Code. */
	By CampaignCode = By.xpath(".//*[@id='CampaignPlate_CodeTextBox']");

	/** Campaign Year. */
	By CampaignYear = By
			.xpath(".//*[@id='CampaignPlate_CampaignYearDropDownList']");

	/** Save / Update. */
	By SaveUpdate = By.xpath(".//*[@id='SaveButton']");
	/** The add_new_grp. */
	
	/** The add_new_grp. */
	By add_new_grp = By.xpath("//a[contains(.,'Add a new donor group')]");
	
	/** The name. */
	By Donor_name =By.xpath("//input[@id='DonorGroupPlate_NameTextBox']");
	
	/** The description. */
	By Donor_description =By.xpath("//input[@id='DonorGroupPlate_DescriptionTextBox']");
	
	/** The grp_name. */
	By grp_name	= By.xpath("//td[contains(.,'Donor Group')]");
	
	/** expand Campaigns. */
	By expand_Campaigns = By.xpath(".//*[@id='NavigationTreeViewn3']/img");

	/** expand Campaigns T. */
	By expand_CampaignsTe = By.xpath(".//*[@id='NavigationTreeViewn32']/img");

	/** Campaign nodes. */
	By Campaignnodes = By.xpath("//div[@id='NavigationTreeViewn32Nodes']");

	/** The name. */
	By name =By.xpath("//input[@id='WebsitePlate_NameTextBox']");
	
	/** The description. */
	By description =By.xpath("//input[@id='WebsitePlate_DescriptionTextBox']");
	
	/** The skin. */
	By skin =By.xpath(".//*[@id='WebsitePlate_SkinIdDropDownList']");
	
	/** Donor identifier. */
	By Donoridentifier =By.xpath(".//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_IdentifierTextBox']");
	
	/** The save_button. */
	By save_button = By.xpath("//a[@id='SaveButton']");

	/** Campaign list. */
	By Campaignlist = By.xpath("//a[contains(text,NavigationTreeViewt)]");

	/** The Donor_firstName. */
	By Donor_firstName = By.xpath(".//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_FirstNameTextBox']");
	/** The Donor_firstName. */
	By Donor_LastName = By.xpath(".//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_LastNameTextBox']");
	
	/** Activate button. */
	By Activatebutton = By.xpath(".//*[@id='CampaignGrid_ActivateButton_0']");
	
	/** Filter. */
	By Filter = By.xpath(".//a[contains(text(),'Filter')]/..//following-sibling::td/input");
	
	/** Settings. */
	By Settings = By.xpath("//a[contains(.,'Settings')]");
	
	/** DeafaultDonorgroup. */
	By DeafaultDonorgroup = By.xpath(".//*[@id='CampaignPlate_DonorGroupEditButton']");
	
	/** DeafaultDonorgroupFilter. */
	By DeafaultDonorgroupFilter = By.xpath(".//*[@id='CampaignPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList_FilterInput']");

	/** DeafaultDonorgroupSearch. */
	By DeafaultDonorgroupSearch = By.xpath(".//*[@id='CampaignPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList']/table/tbody/tr[3]/td[2]/img");
	
	/** DeafaultDonorgroupname. */
	By DeafaultDonorgroupname = By.xpath("//option[contains(.,'testdonor')]");
	
	/** DeafaultDonorgroupselect. */
	By DeafaultDonorgroupselect = By.xpath(".//*[@id='CampaignPlate_DonorGroupSelectionList_SelectButton']");
	
	/** campaignusername. */
	By campaignusername = By.xpath(".//*[@id='CampaignPlate_UsernameTextBox']");
	
	/** campaignPassword. */
	By campaignPassword = By.xpath(".//*[@id='CampaignPlate_PasswordTextBox']");
	
	/** The donorgrps_expand. */
	By donorgrps_expand = By.xpath("//a[contains(.,'Donor Groups')]"); 
	
	/** The name. */
	By donorname =By.xpath("//input[@id='DonorGroupPlate_NameTextBox']");
	
	/** The description. */
	By donordescription =By.xpath("//input[@id='DonorGroupPlate_DescriptionTextBox']");
	
	/** The grid. */
	By grid =By.xpath(".//*[@id='CampaignGrid']/tbody/tr[2]/td[1]");
	
	
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM_CAMPAIGN_ACTIVE_CAMPAIGN.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_ACTIVE_CAMPAIGN(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		try {
			Thread.sleep(2000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			click(Campaigns, "Campaign");
			SwitchToFrame(By.name("content"), "Main Frame");
			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToTop();
		     }*/
			click(AddCampaign, "Add Campaign");
			Thread.sleep(2000);
			selectBySendkeys(Organisation, "QASeleniumOrganization", "Organization DropDown");
			Thread.sleep(10000);
			tab(Organisation, "tab from dropdown");
			//Thread.sleep(10000);
			//tab(Organisation, "press tab");
			selectBySendkeys(Comapny, "QASeleniumCompany", "Comapny DropDown");
			Thread.sleep(6000);
			tab(Comapny, "tab from dropdown");
			Thread.sleep(3000);
			String strcampaignname = strwithtimestamp((String) params.get("action.campaignName"));
			type(CampaignName, strcampaignname, "Campaign Name");
			//selectByIndex(Type, 1, "TypeDropDown");
			Thread.sleep(3000);
			strcampaigncode = strwithtimestamp((String) params.get("action.campaigncode"));
			type(CampaignCode, strcampaigncode, "Campaign code");
			//Changed from 7 to 1 for stage environment execution
			Thread.sleep(3000);
			selectByIndex(CampaignYear, 1, "Campaign Year Type DropDown");
			Thread.sleep(3000);
			tab(Organisation, "press tab");
			Thread.sleep(3000);
			click(SaveUpdate, "Save / Update");
			Thread.sleep(3000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			Thread.sleep(3000);
			/** Expand the campaign tree */
			By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
			By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
			By click1_Expand =By.xpath("//[contains(.,'"+strcampaignname+"')]");
			By Node3_Expand =By.xpath(".//*[contains(@alt,'Expand "+strcampaignname+"')]");
			System.out.println(".//*[contains(@alt,'Expand "+strcampaignname+"')]");
			/** Clicking on Campaign Node  . */ 
			Thread.sleep(5000);
			SwitchToFrame(By.name("tree"),"Tree Frame");
			Thread.sleep(4000);
			click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
			Thread.sleep(3000);
			click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
			Thread.sleep(3000);
			if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
			Thread.sleep(3000);
			JavascriptExecutor js = (JavascriptExecutor) this.Driver;
			js.executeScript("window.scrollBy(0,-2000)", "");
			}
			click(Node3_Expand, "Expand "+strcampaignname+" Node under campaign");
			Thread.sleep(3000);
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(3000);
			By xpath = By.xpath("//input[@value='"+strcampaignname+"']");
			/** Verify the created campaign */
			boolean validationPoint = isElementPresent(xpath, "value is present", true);
			Thread.sleep(3000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			Thread.sleep(3000);
			System.out.println(validationPoint);
			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void move() {

		try {
			SwitchToFrame(By.name("tree"),"Tree Frame");
			Thread.sleep(3000);
			click(Settings, "settings");
			Thread.sleep(2000);
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(4000);
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(DeafaultDonorgroup));
			click(DeafaultDonorgroup, "Default donor group");
			Thread.sleep(2000);
			WebDriverWait wait2 = new WebDriverWait(this.Driver, 300);
			wait2.until(ExpectedConditions.presenceOfElementLocated(DeafaultDonorgroupname));
			click(DeafaultDonorgroupname, "select the donor group");
			Thread.sleep(2000);
			WebDriverWait wait1 = new WebDriverWait(this.Driver, 300);
			wait1.until(ExpectedConditions.presenceOfElementLocated(DeafaultDonorgroupselect));
			click(DeafaultDonorgroupselect, "select");
			Thread.sleep(2000);
			click(SaveUpdate, "save");
			Thread.sleep(2000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			Thread.sleep(3000);
			if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
			Thread.sleep(3000);
			JavascriptExecutor js = (JavascriptExecutor) this.Driver;
			js.executeScript("window.scrollBy(0,-2000)", "");
			}
			click(Campaigns, "Campaign");
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(4000);
			FilterCheck(Filter,grid,CM_CAMPAIGN_ACTIVE_CAMPAIGN.strcampaigncode);
			//Thread.sleep(10000);
			//commented below code and added Filtercheck method
			/*type(Filter,CM_CAMPAIGN_ACTIVE_CAMPAIGN.strcampaigncode, "enter campaign name");
			Thread.sleep(5000);
			tab(Filter, "press enter");
			Thread.sleep(2000);*/
			//this.reporter.successwithscreenshot("Activate button status before clicking", "Activate button status before clicking",this.Driver);
			Thread.sleep(5000);
			WebDriverWait wait3 = new WebDriverWait(this.Driver, 300);
			wait3.until(ExpectedConditions.presenceOfElementLocated(Activatebutton));
	        JSClick(Activatebutton, "Activate Button");
			Thread.sleep(5000);
			//this.reporter.successwithscreenshot("Activate button status after clicking", "Activate button status after clicking",this.Driver);
			boolean validationPoint = isElementPresent(Activatebutton, "Traffic light icon should change to Red", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("Verify Traffic light icon should change to Red", "Traffic light icon changed to Red",this.Driver);
			}else{
				this.reporter.failureReport("Verify Traffic light icon should change to Red", "Traffic light icon not changed to Red",this.Driver);
			}
			
			
			/*boolean validationPoint = isElementPresent(Activatebutton,
					"Activate button", false);
			if (validationPoint) {
				this.reporter.failureReport(
						"User is not able to verify Activatebutton",
						"User is not able to verify Activatebutton",
						this.Driver);	
			} else {
				this.reporter.successwithscreenshot(
						"User is able to verify Activatebutton",
						"User is able to verify Activatebutton",this.Driver);
			}*/
			} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}