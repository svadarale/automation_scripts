package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;









import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_CREATE_DONOR.
 */
public class CM_ADMIN_CREATE_DONOR extends ActionEngine implements Page {
		
		public static String str;

		/** The donor_expand. */
		By donor_expand = By.xpath("//a[contains(.,'Donors')]");
		
		/** The donor_list. */
		By donor_list	= By.xpath("//td[contains(.,'Donor List')]");
		
		/** The add_new_donor_link. */
		By add_new_donor_link	= By.xpath(".//*[@id='AddDonorButton']");
		
		/** The company_donor_profile. */
		By company_donor_profile	= By.xpath("//div[@id='Campaign_DonorPlate_CompanySettingsPanel']/span");
		
		/** The active. */
		By active	= By.cssSelector(".FormFieldData [id=Campaign_DonorPlate_CompanyDonorProfileSettings_DonorIsActiveCheckbox]");
	
	   /** The donor_mode_dropdown. */
		By donor_mode_dropdown = By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_DonorModeTypeDropDownList");
		
		/** The donor_identifier. */
		By donor_identifier = By.xpath("//input[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_IdentifierTextBox']");
		
         /** The name_prefix. */
	    By name_prefix	= By.xpath("//input[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_NamePrefixTextBox']");
		
		/** The first_name. */
		By first_name	= By.xpath("//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_FirstNameTextBox']");
		
		/** The last_name. */
		By last_name	= By.xpath("//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_LastNameTextBox']");
		
		/** The work_country. */
		By work_country	= By.xpath("//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_CountryDropDownList']");
		
		/** The work_address1. */
		By work_address1	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_Address1TextBox");
		
		/** The work_address2. */
		By work_address2	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_Address2TextBox");
		
		/** The work_city. */
		By work_city	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_CityTextBox");
		
		/** The work_province_state. */
		By work_province_state	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_ProvinceTextBox");
		
		/** The work_zip_postal_code. */
		By work_zip_postal_code	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_ZipCodeTextBox");
		
		/** The work_phone_number_1. */
		By work_phone_number_1	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_WorkAddressEditControl_PhoneNumber1TextBox");
		
		/** The work_email_address. */
		By work_email_address	= By.xpath("//*[@id='Campaign_DonorPlate_CompanyDonorProfileSettings_WorkEmailAddressTextBox']");
		
		/** The default_address. */
		By default_address	= By.cssSelector("#Campaign_DonorPlate_CompanyDonorProfileSettings_AddressPreferenceDropDownList");
		
		/** The save_button. */
		By save_button	= By.xpath("//a[@id='SaveButton']");
		
		/** The active_col. */
		By active_col	= By.xpath("//a[contains(.,'Active')]");
		
		/** The Filter. */
		By Filter_Locator = By.cssSelector("[id*='FilterTextBox']");
		By grid = By.xpath(".//*[@id='Campaign_DonorGrid']/tbody/tr[2]/td[2]");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CM_ADMIN_CREATE_DONOR.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
	
		public CM_ADMIN_CREATE_DONOR(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(4000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(4000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
			        } catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				Thread.sleep(2000);
				click(donor_expand, "Expanding Websites Node under Campaigns");
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);	
				click(add_new_donor_link, "Add new button");
				Thread.sleep(5000);
				if(!(active.findElement(Driver).isSelected())){
				click(active,"Selecting Active checkbox");}
				Thread.sleep(2000);
			    selectBySendkeys(donor_mode_dropdown, (String)params.get("donormode.dropdown"), "Donor Mode Type");
				Thread.sleep(3000);
				String idrname = (String) params.get("donor.identifier");
			    str = strwithtimestamp(idrname);
			    Thread.sleep(5000);
			    type(donor_identifier, str, "donor_identifier");
			    Thread.sleep(2000);
				type(name_prefix, (String)params.get("name.prefix"), "Name Prefix");
				Thread.sleep(1000);
				type(first_name, (String)params.get("first.name"), "First Name");
				Thread.sleep(2000);
				type(last_name, (String)params.get("last.name"), "Last Name");
				Thread.sleep(1000);
				selectBySendkeys(work_country, (String)params.get("work.country"), "Work Country");
				Thread.sleep(7000);
				type(work_address1, (String)params.get("work.address1"), "Work Address 1");
				Thread.sleep(2000);
				type(work_address2, (String)params.get("work.address2"), "Work Address 2");
				Thread.sleep(2000);
				type(work_city, (String)params.get("work.city"), "Work City");
				Thread.sleep(2000);
				type(work_province_state, (String)params.get("work.province.state"), "Work Province / State");
				Thread.sleep(1000);
				type(work_zip_postal_code, (String)params.get("work.zip.postal.code"), "Work Zip / Postal Code");
				type(work_phone_number_1, (String)params.get("work.phone.number.1"), "Work Phone Number 1");
				type(work_email_address, (String)params.get("work.email.address"), "Work E-mail Address");
				Thread.sleep(5000);
				selectBySendkeys(default_address, (String)params.get("default.address"), "Default Address");
				Thread.sleep(7000);
				click(save_button, "Save / Update"); 
				Thread.sleep(5000);
				
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(5000);
				click(donor_expand, "Expanding Websites Node under Campaigns");
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				 FilterCheck(Filter_Locator,grid,str);
				} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}