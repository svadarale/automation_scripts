package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;







import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_CREATE_SPE_DONOR_GRPS.
 */
public class CM_ADMIN_CREATE_DONOR_GRPS extends ActionEngine implements Page {
		
		public static String strname;
		/** The add_new_grp. */
		By add_new_grp = By.xpath("//a[contains(.,'Add a new donor group')]"); 
		
		/** The name. */
		By name =By.xpath("//input[@id='DonorGroupPlate_NameTextBox']");
		
		/** The description. */
		By description =By.xpath("//input[@id='DonorGroupPlate_DescriptionTextBox']");
		
		/** The save_button. */
		By save_button = By.xpath("//a[@id='SaveButton']");
		
		/** The grp_name. */
		By grp_name	= By.xpath("//td[contains(.,'Donor Group')]");
		
		/** The Filter. */
		By Filter_Locator = By.cssSelector("[id*='FilterTextBox']");
		
		/** The donorgrps_expand. */
		By donorgrps_expand = By.xpath("//a[contains(.,'Donor Groups')]");
		
		/** The params. */
		Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin create spe donor grps.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_CREATE_DONOR_GRPS(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
	
		@Override
		public void fill() {
			
			try {
				/** Create New Donor Group. */
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }*/
				click(add_new_grp, "Campaigns expand");
				Thread.sleep(4000);
				strname = strwithtimestamp((String)params.get("name"));
				type(name, strname, "name value");
				Thread.sleep(2000);
				type(description, (String)params.get("description"), "description value");
				Thread.sleep(2000);
				click(save_button, "Click on save button");
				Thread.sleep(2000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(5000);
				click(donorgrps_expand, "donorgrps_expand");
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				By grid = By.xpath(".//*[@id='DonorGroupGrid']/tbody/tr[2]/td[1]");
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToBottom();
			     }*/
                FilterCheck(Filter_Locator,grid,strname);
                Thread.sleep(2000);
			}   catch (Throwable e) {
				e.printStackTrace();
			}
		}
		
	
		@Override
		public void move() {
			try {
			}  catch (Throwable e) {
				e.printStackTrace();
			}
		}

	}
	
	


