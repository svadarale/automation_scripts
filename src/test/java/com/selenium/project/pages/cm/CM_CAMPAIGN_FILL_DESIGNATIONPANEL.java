package com.selenium.project.pages.cm;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_FILL_DESIGNATIONPANEL extends ActionEngine implements Page{

	/**  Organizations. */
	By Expand_Organizations = By.xpath(".//*[@id='NavigationTreeViewn1']/img");
	
	/**  Quotation mark. */
	By Expand_Quotationmark = By.xpath(".//*[@id='NavigationTreeViewn5']/img");
	
	/** A united way. */
	By Expand_Aunitedway = By.xpath(".//*[@id='NavigationTreeViewn31']/img");
	
	/** Designation panel. */
	By Designations_panel = By.xpath(".//*[@id='NavigationTreeViewt41']");

	/** Designation panel. */
	By ADD_Designations_panel = By.xpath(".//*[@id='AddButton']");
	
	/** Designation panel. */
	By Designations_panel_Type = By.xpath(".//*[@id='DesignationPanelPlate_DesignationPanelTypeDropDownList']");
	
	/** Designation panel. */
	By Designations_panel_Name = By.xpath(".//*[@id='DesignationPanelPlate_DesignationPanelNameTextBox']");
	
	/** Designation panel. */
	By Designations_panel_save = By.xpath(".//*[@id='SaveButton']");
	
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM CAMPAIGN ADD A NEW DESIGNATION OPTIONS.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_FILL_DESIGNATIONPANEL(WebDriver driver, CReporter reporter,Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		
		try {

			
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			click(Expand_Organizations, "Expand Organizations");
			/** Expand the Quotation mark.*/
			click(Expand_Quotationmark, "Expand Quotationmark");
			/** Expand the A United Way.*/
			click(Expand_Aunitedway, "Expand A united way");
			/** click the Designation panel  */
			click(Designations_panel, "Designation panel");
			
			} catch (Throwable e) 
		{
		
			e.printStackTrace();
		}

	}

	@Override
	public void move() {


		try {
			
			SwitchToFrame(By.name("content"), "Designation set Frame");
			click(ADD_Designations_panel, "Add Designation panel");
			selectByIndex(Designations_panel_Type, 0, "Introductory");
			type(Designations_panel_Name, (String) params.get("action.Name"), "Name");
			click(Designations_panel_save, "save button");
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			/** click the Designation panel  */
			click(Designations_panel, "Designation panel");
			click(ADD_Designations_panel, "Add Designation panel");
			selectByIndex(Designations_panel_Type, 1, "Primary");
			type(Designations_panel_Name, (String) params.get("action.PrimaryName"), "Primary Name");
			click(Designations_panel_save, "save button");
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			/** click the Designation panel  */
			click(Designations_panel, "Designation panel");
			click(ADD_Designations_panel, "Add Designation panel");
			selectByIndex(Designations_panel_Type, 2, "Primary");
			type(Designations_panel_Name, (String) params.get("action.Locateanagency"), "Locateanagency Name");
			click(Designations_panel_save, "save button");
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
						
		} catch (Throwable e) {
			
			e.printStackTrace();
		}
	}
}
