package com.selenium.project.pages.cm;


import java.awt.Robot;
import java.util.Properties;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.AWTException;


//import util.WebUtils;























import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;
import com.google.inject.Key;


/**
 * The Class CM_CAMPAIGN_CREATE_EMAIL.
 */
public class CM_CAMPAIGN_CREATE_EMAIL extends ActionEngine implements Page {
		
		/** The email_expand. */
		By email_expand = By.xpath("//a[contains(.,'Email')]");
		
		/** The add_email_link. */
		By add_email_link	= By.xpath("//a[@id='CampaignPlate_EmailAddButton']");
		
		/** The description. */
		By description	= By.xpath("//input[@id='CampaignPlate_EmailPanel_DescriptionTextBox']");
		
		/** The from_address. */
		By from_address	= By.xpath("//input[@id='CampaignPlate_EmailPanel_EmailTable_FromAddressTextBox']");
		
		/** The subject. */
		By subject	= By.xpath("//input[@id='CampaignPlate_EmailPanel_EmailTable_SubjectTextBox']");
		
		/** The format_dropdown. */
		By format_dropdown	= By.xpath("//select[@id='CampaignPlate_EmailPanel_EmailTable_MailFormatTypeDropDownList']");
		
		/** The text_body. */
		By text_body = By.xpath("//textarea[@id='CampaignPlate_EmailPanel_EmailTable_BodyTextBox']");
		
		/** The save_button. */
		By save_button	= By.xpath("//a[@id='SaveButton']");
		
		/** The filter. */
		By filter = By.xpath("//input[@id='CampaignPlate_EmailGrid_FilterTextBox']");
		
		/** The description_grid. */
		By filter_column = By.xpath("//table[@id='CampaignPlate_EmailGrid']/tbody/tr[2]/td[2]");
		
		/** The description_grid. */
		By description_grid	= By.xpath("//a[@class='EnhancedDataGridHeader'][contains(.,'Description')]");
		
		/** The subject_grid. */
		By subject_grid	= By.xpath("//td[@class='EnhancedDataGridHeader'][contains(.,'Subject')]");
		
		/** The send_date_grid. */
		By send_date_grid	= By.xpath("//td[@class='EnhancedDataGridHeader'][contains(.,'Send Date')]");
		
		/** The status_grid. */
		By status_grid	= By.xpath("//td[@class='EnhancedDataGridHeader'][contains(.,'Status')]");
		
		/** The process_date_grid. */
		By process_date_grid	= By.xpath("//td[@class='EnhancedDataGridHeader'][contains(.,'Process Date')]");
		
		/** The active_grid. */
		By active_grid	= By.xpath("//td[@class='EnhancedDataGridHeader'][contains(.,'Action')]");
		
		/** The html_body. */
		By html_body = By.xpath(".//*[@id='CampaignPlate_EmailPanel_EmailTable_BodyTextBox']");
	
		/** The no_html_body. */
		By no_html_body	= By.cssSelector("#CampaignPlate_EmailPanel_EmailTable_AlternateBodyTextBox");
		
		/** The saveImage. */
		By saveImage = By.cssSelector("#SaveButton_Image");
		
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CAMPAIGN EMAIL CREATION
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_CREATE_EMAIL(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				Thread.sleep(2000);
				click(email_expand, "Expanding Email Node under Campaigns");
				Thread.sleep(2000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(2000);
				//Creation of text format email
				click(add_email_link, "Add a new email");
				Thread.sleep(4000);
				String strdescription = (String) params.get("descriptiont");
				String strToPass1 = strwithtimestamp(strdescription);
			    Thread.sleep(5000);
			    type(description, strToPass1, "Description");
			    Thread.sleep(1000);
			    type(from_address, (String)params.get("from.addresst"), "From Address");
			    type(subject, (String)params.get("subjectt"), "Subject");
			    selectBySendkeys(format_dropdown, (String)params.get("formatt"), "Format");
			    Thread.sleep(2000);
			    javascriptSendKeys((String)params.get("text.bodyt"), this.Driver.findElement(text_body), "Text Body");
			    click(save_button, "Save / Update link");
			    Thread.sleep(5000);	
				FilterCheck(filter,filter_column,strToPass1);
				Thread.sleep(6000);	
				isElementPresent(description_grid, "Description", true);
				Thread.sleep(1000);
				isElementPresent(subject_grid, "Subject", true);
				Thread.sleep(1000);
				isElementPresent(send_date_grid, "Send date", true);
				Thread.sleep(1000);
				isElementPresent(status_grid, "Status", true);
				Thread.sleep(1000);
				isElementPresent(process_date_grid, "Process Date", true);
				Thread.sleep(1000);
				isElementPresent(active_grid, "Action", true);
				Thread.sleep(5000);
				
				//Creation of HTML format email
				click(add_email_link, "Add a new email");
				Thread.sleep(5000);
				String strdescription2 = (String) params.get("descriptionh");
				String strToPass2 = strwithtimestamp(strdescription2);
			    Thread.sleep(5000);
			    type(description, strToPass2, "Description");
			    Thread.sleep(1000);
			    type(from_address, (String)params.get("from.addressh"), "From Address");
			    type(subject, (String)params.get("subjecth"), "Subject");
			    selectBySendkeys(format_dropdown, (String)params.get("formath"), "Format");
			    Thread.sleep(9000);
			    tab(format_dropdown, "Format dropdown");
			    Thread.sleep(10000);
			    Actions action=new Actions(this.Driver);
			    if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			       action.sendKeys("bsgsjgdfsdgfgifagiasdhkhad").build().perform();
			       Thread.sleep(5000);
			     }
			    else{
			    Robot robot = new Robot();
			    robot.keyPress(KeyEvent.VK_H);
			    robot.keyRelease(KeyEvent.VK_H);
			    robot.keyPress(KeyEvent.VK_T);
			    robot.keyRelease(KeyEvent.VK_T);
			    robot.keyPress(KeyEvent.VK_M);
			    robot.keyRelease(KeyEvent.VK_M);
			    robot.keyPress(KeyEvent.VK_L);
			    robot.keyRelease(KeyEvent.VK_L);
			    robot.keyPress(KeyEvent.VK_B);
			    robot.keyRelease(KeyEvent.VK_B);
			    robot.keyPress(KeyEvent.VK_O);
			    robot.keyRelease(KeyEvent.VK_O);
			    robot.keyPress(KeyEvent.VK_D);	
			    robot.keyPress(KeyEvent.VK_Y);
			    robot.keyRelease(KeyEvent.VK_Y);
			    Thread.sleep(5000);
			    }
			    javascriptSendKeys((String)params.get("nohtml.body"), this.Driver.findElement(no_html_body), "no Html Body");
			   // action.moveToElement(this.Driver.findElement(no_html_body)).sendKeys("joiwehwghduwgdwfgswswq32").build().perform();
			   // action.release().build().perform();
			    Thread.sleep(5000);
			    tab(no_html_body, "no_html_body");
			    tab(saveImage, "no_html_body");
			    click(save_button, "Save / Update link");
			    if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			    {
			    	 javascriptSendKeys((String)params.get("nohtml.body"), this.Driver.findElement(no_html_body), "no Html Body");
					   // action.moveToElement(this.Driver.findElement(no_html_body)).sendKeys("joiwehwghduwgdwfgswswq32").build().perform();
					   // action.release().build().perform();
					    Thread.sleep(5000);
					    tab(no_html_body, "no_html_body");
					    tab(saveImage, "no_html_body");
					    click(save_button, "Save / Update link");
			    }
			    Thread.sleep(5000);
			    this.Driver.findElement(filter).clear();
			    Thread.sleep(5000);
				FilterCheck(filter,filter_column,strToPass2);
				Thread.sleep(6000);	
				isElementPresent(description_grid, "Description", true);
				Thread.sleep(1000);
				isElementPresent(subject_grid, "Subject", true);
				Thread.sleep(1000);
				isElementPresent(send_date_grid, "Send date", true);
				Thread.sleep(1000);
				isElementPresent(status_grid, "Status", true);
				Thread.sleep(1000);
				isElementPresent(process_date_grid, "Process Date", true);
				Thread.sleep(1000);
				isElementPresent(active_grid, "Action", true);
				Thread.sleep(5000);
			  
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}