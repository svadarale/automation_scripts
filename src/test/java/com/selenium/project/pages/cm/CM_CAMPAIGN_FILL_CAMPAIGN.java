package com.selenium.project.pages.cm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_FILL_CAMPAIGN extends ActionEngine implements Page{
	
	public static String strcampaignname;
	/** The expand_camp. */
	By expand_camp = By.xpath(".//*[@alt='Expand Campaigns']");
	
	/** The expand_camp. */
	By campaigns = By.linkText("Campaigns");
	
	/** The camp_list. */
	By camp_list = By.xpath("//td[contains(.,'Campaign List')]");
	
	/** Add Campaign. */
	By AddCampaign  = By.xpath(".//*[@id='AddButton']");
	
	/** Organisation. */
	By Organisation  = By.xpath(".//*[@id='CampaignPlate_OrganizationIdDropDownList']");
	
	/** Company. */
	By Company = By.xpath(".//*[@id='CampaignPlate_CompanyIdDropDownList']");
	//By Company  = By.id("CampaignPlate_CompanyIdDropDownList");
	
	/** Campaign Name. */
	By CampaignName  = By.xpath(".//*[@id='CampaignPlate_NameTextBox']");
	
	/** Type. */
	By Type  = By.xpath(".//*[@id='CampaignPlate_CampaignTypeDropDownList']");
	
	/** Campaign Code. */
	By CampaignCode  = By.xpath(".//*[@id='CampaignPlate_CodeTextBox']");
	
	/** Campaign Year. */
	By CampaignYear  = By.xpath(".//*[@id='CampaignPlate_CampaignYearDropDownList']");

	/** Save / Update. */
	By SaveUpdate = By.xpath(".//*[@id='SaveButton']");

	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a CM Filled Campaign .
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */

	public CM_CAMPAIGN_FILL_CAMPAIGN (WebDriver driver,CReporter reporter, Properties params)
	{
		setdriver(driver,reporter);
		this.params = params;
	}
	
	@Override
	public void fill() {
		try {
			SwitchToFrame(By.name("tree"),"Top NAV Frame");
			click(campaigns, "Campaign");
			SwitchToFrame(By.name("content"),"Main Frame");
			Thread.sleep(3000);
			isElementVisible(camp_list, "Campaign list is present");
			isElementPresent(camp_list, "Campaign list is present", true);
			click(AddCampaign,"Add Campaign");
			selectBySendkeys(Organisation, "Daiva's United Way", "Organization DropDown");
			tab(Organisation, "tab from dropdown");
			selectBySendkeys(Company, "Acme Manufacturing", "Company DropDown");
			tab(Company, "tab in dropdown");
			strcampaignname =strwithtimestamp((String) params.get("action.campaignName"));
			type(CampaignName, strcampaignname, "Campaign Name");
			String strcampaigncode = strwithtimestamp((String) params.get("action.campaignCode"));
			type(CampaignCode, strcampaigncode, "Campaign code");
			selectBySendkeys(CampaignYear, "2014", "CampaignYear Type DropDown");
			tab(CampaignYear, "tab in dropdown");
			Thread.sleep(3000);
			/** Save the add campaign fields */			
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void move() {
		try {
			click(SaveUpdate, "Save / Update");
			Thread.sleep(10000);
			SwitchToFrame(By.name("tree"),"Top NAV Frame");
			Thread.sleep(5000);
			click(expand_camp, "Campaign");
			Thread.sleep(10000);
			By Node2_Expand =By.xpath(".//*[contains(@alt,'Expand T')]");
			By Node3_Expand =By.xpath(".//*[contains(@alt,'Expand "+strcampaignname+"')]");
			isElementPresent(Node2_Expand, "T node is present", true);
			/** Clicking on Campaign Node  . */               
			Thread.sleep(5000);
			click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
			Thread.sleep(10000);			
			//click(Node3_Expand, "Expanding "+strcampaignname+" Node under T");
			//Thread.sleep(5000);
			ScrollToElementVisible(Node3_Expand);
			Thread.sleep(10000);	
			boolean validationPoint = isElementPresent(Node3_Expand, "website list is present", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("Verify create campaign", "Newly created campaign present in campaign list",this.Driver);
			}else{
				this.reporter.failureReport("Verify create campaign", "Create campaign failed", this.Driver);
			}
			Thread.sleep(5000);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}