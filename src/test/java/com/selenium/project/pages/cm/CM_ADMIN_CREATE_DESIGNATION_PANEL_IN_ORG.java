package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;







import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_CREATE_SPE_WEBSITE.
 */
public class CM_ADMIN_CREATE_DESIGNATION_PANEL_IN_ORG extends ActionEngine implements Page {
		
		/** The add_new_designation_panel. */
		By add_new_designation_panel = By.xpath("//a[contains(.,'Add a new Designation Panel')]"); 
		
		/** The type_option. */
		By type_option =By.xpath("//select[@id='DesignationPanelPlate_DesignationPanelTypeDropDownList']");
		
		/** The name. */
		By name =By.xpath("//input[@id='DesignationPanelPlate_DesignationPanelNameTextBox']");
		
		/** The save_button. */
		By save_button = By.xpath("//a[@id='SaveButton']");
		
		/** The desingation_panel_list. */
		By desingation_panel_list = By.xpath("//td[contains(.,'Designation Panel')]");

				
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin create spe website.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_CREATE_DESIGNATION_PANEL_IN_ORG(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		
		@Override
		public void fill() {
				try {
				Thread.sleep(3000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				click(add_new_designation_panel, "add_new_payment_type_set");
				Thread.sleep(4000);
				selectBySendkeys(type_option, (String)params.get("type_option"), "type_option");
				Thread.sleep(3000);
				String strname = strwithtimestamp((String)params.get("name"));
				type(name, strname, "name value");
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}	
		
		@Override
		public void move() {
				try {
				Thread.sleep(2000);
				click(save_button, "save_button");
				Thread.sleep(2000);
				boolean validationPoint = isElementPresent(desingation_panel_list, "desingation_panel_list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("desingation_panel_list element is present", "desingation_panel_list element is present",this.Driver);
				}else{
					this.reporter.failureReport("desingation_panel_list element is present", "desingation_panel_list element is not present", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}