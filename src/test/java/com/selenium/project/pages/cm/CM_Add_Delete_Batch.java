package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;
import com.thoughtworks.selenium.webdriven.commands.IsEditable;

// TODO: Auto-generated Javadoc
/**
 * The Class CM_Add_Delete_Batch.
 */
public class CM_Add_Delete_Batch extends ActionEngine implements Page {



	/** The lnk batches. */
	By lnkBatches = By.xpath("//a[text()='Batches']");

	/** The lnk add new batch. */
	By lnkAddNewBatch = By.xpath("//a[@id='AddButton']");

	/** The lbl use temaplate. */
	By lblUseTemaplate = By.xpath("//td[text()='Use Template']");


	/** The dropdown use tempalte. */
	By dropdownUseTempalte = By.xpath("//select[@id='BatchPlate_BatchTemplateDropDownList']");

	/** The txtbox batch code. */
	By txtboxBatchCode = By.id("BatchPlate_CodeTextBox");

	/** The txtbox label. */
	By txtboxLabel = By.id("BatchPlate_LabelTextBox");

	/** The txtbox description. */
	By txtboxDescription = By.id("BatchPlate_DescriptionTextBox");

	/** The btn save. */
	By btnSave = By.id("SaveButton");

	/** The in work filter box. */
	By inWorkFilterBox=By.xpath("//table[@id='BatchGridInWork']//tr[@class='EnhancedDataGridPager']//input[contains(@id,'FilterTextBox')]");

	/** The lnk delete selected batches. */
	By lnkDeleteSelectedBatches=By.id("DeleteSelectedButton");

	/** The lbl no element. */
	By lblNoBatchElement = By.xpath("//table[@id='BatchGridInWork']//td[text()='none']");

	By lblNoBatchDonationElement = By.xpath("//table[@id='BatchPlate_BatchDonationGrid']//td[text()='none']");

	By lnkAddNewBatchDonation = By.id("BatchPlate_AddButton");

	By lblDonorLookup = By.xpath("//td[text()='Donor Lookup']");

	By lblDonorsFound = By.xpath("//td[text()='123 donors found']");

	By btnLookup = By.id("BatchPlate_DonorLookupButton");

	By btnDonorSelect = By.id("BatchPlate_DonorSelectButton");

	By dropdownDonor = By.id("BatchPlate_DonorSelectionDropDownList");

	By dropdownPaymentType = By.id("BatchPlate_DonorSelectionDropDownList");

	By txtboxDonationAmt = By.id("BatchPlate_DonationPlate_DonationSubPlate_AmountTextBox");

	By lnkDeleteBatchDonation=By.xpath("//table[@id='BatchPlate_BatchDonationGrid']//following-sibling::td/input[@alt='Delete Batch Donation']");


	public static By verify_Batch=null;


	public static By iconDonation=null;


	/** The batchcode. */
	public  String batchcode=null;



	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new c m_ add_ delete_ batch.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM_Add_Delete_Batch(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#fill()
	 */
	@Override
	public void fill() {
		try {

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#move()
	 */
	@Override
	public void move() {

		try {

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	/**
	 * Create_ batch.
	 *
	 * @return the string
	 */
	public String create_Batch(String templateName) {
		try {
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(lnkBatches,"Batches");
			JSClick(lnkBatches, " Batches");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			waitForVisibilityOfElement(lnkAddNewBatch," Add New Batch");
			JSClick(lnkAddNewBatch, " Add New Batch");
			waitForSeconds(5);
			waitForVisibilityOfElement(lblUseTemaplate," Use Template drop down",60);
			if(isEdge())
				selectBySendkeys(dropdownUseTempalte, templateName, " Use Template drop down");
			else
				selectByVisibleText(dropdownUseTempalte, templateName, " Use Template drop down");
			Thread.sleep(5000);
			batchcode=getUniqueID();
			type(txtboxBatchCode,batchcode,"Batch Code");
			waitForSeconds(2);
			type(txtboxLabel,generateRandomString(5, Mode.ALPHA),"Label");
			waitForSeconds(2);
			//type(txtboxDescription,this.params.getProperty("Description"),"Group Description");
			waitForSeconds(2);
			waitForElementToBoClickable(btnSave, " Save Button", 60);
			JSClick(btnSave, " Save Button");
			waitForSeconds(4);
			
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return batchcode;

	}

	/**
	 * Verify_ batch.
	 *
	 * @param batchCode the batch code
	 */
	public void verify_Batch(String batchCode) {
		try {
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(lnkBatches,"Batches");
			JSClick(lnkBatches, " Batches");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			waitForSeconds(5);
			verify_Batch=By.xpath("//table[@id='BatchGridInWork']//td[text()='"+batchCode+"']");
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			if(isElementPresent(verify_Batch, "Batch ", true))
				this.reporter.successwithscreenshot("Verify Batch in Grid", "Batch " +batchCode+ " created successfully", this.Driver);
			else
				this.reporter.failureReport("Verify Batch  in Grid", "Batch  not created ", this.Driver);

		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	/**
	 * Create_ batch donation.
	 */
	public void create_BatchDonation(String batchCode) {
		try {
			iconDonation=By.xpath("//table[@id='BatchGridInWork']//td[text()='"+batchCode+"']/following-sibling::td/input[contains(@alt,'Edit Donations')]");
			waitForElementToBoClickable(iconDonation, " Edit Donations", 60);
			waitForSeconds(2);
			JSClick(iconDonation, " Edit Donations");
			waitForSeconds(2);
			waitForElementToBoClickable(lnkAddNewBatchDonation, " Add New Batch Donation", 60);
			JSClick(lnkAddNewBatchDonation, " Add New Batch Donation");
			waitForSeconds(2);
			waitForVisibilityOfElement(lblDonorLookup,"Donor Lookup",40);
			JSClick(btnLookup, " Donor Lookup");
			waitForSeconds(5);
			selectByVisibleText(dropdownDonor, this.params.getProperty("Donor"), " Donor drop down");
			waitForSeconds(5);
			JSClick(btnDonorSelect, " Select");
			waitForSeconds(4);
			waitForVisibilityOfElement(txtboxDonationAmt," Donation Amount",120);
			type(txtboxDonationAmt,this.params.getProperty("DonationAmt"),"Donation Amount");
			ScrollToElementVisible(btnSave);
			waitForSeconds(2);
			JSClick(btnSave, " Save Button");
			waitForSeconds(2);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Verify batch donation.
	 */
	public void verifyBatchDonation(String batchCode) {
		try {
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForSeconds(2);
			waitForVisibilityOfElement(lnkBatches,"Batches");
			JSClick(lnkBatches, " Batches");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			waitForElementToBoClickable(iconDonation, " Edit Donations", 60);
			JSClick(iconDonation, " Edit Donations");
			waitForSeconds(5);
			waitForElementToBoClickable(lnkAddNewBatchDonation, " Add New Batch Donation", 60);
			By verifyBatchDonation=By.xpath("//table[@id='BatchPlate_BatchDonationGrid']//td[contains(text(),'"+this.params.getProperty("PaymentType")+"')]");

			if(isElementPresent(verifyBatchDonation, "Batch Donation", true))
				this.reporter.successwithscreenshot("Verify Batch  Donation", "Batch Donation created successfully", this.Driver);
			else
				this.reporter.failureReport("Verify Batch  Donation", "Batch  not created ", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Delete batch donation.
	 */
	public void deleteBatchDonation() {
		try {
			waitForElementToBoClickable(lnkDeleteBatchDonation, " Delete Batch Donation", 120);
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(lnkDeleteBatchDonation, " Delete Batch Donations");
				waitForSeconds(7);
				AlertAccept();

			}
			else{
				JSAcceptAlert();
				Thread.sleep(3000);
				JSClick(lnkDeleteBatchDonation, " Delete Batch Donations");
			}
			waitForElementToBoClickable(lblNoBatchDonationElement, "No Batch Donation Element in Grid", 60);
			if(isElementPresent(lblNoBatchDonationElement, "No Batch Donation Element in Grid", true))
				this.reporter.successwithscreenshot(" Batch Donation  Deletion", "Batch Donation  deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Batch Donation  Deletion", "Batch Donation  not deleted ", this.Driver);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}


	/**
	 * Delete_ selected batch.
	 *
	 * @param batchCode the batch code
	 */
	public void delete_SelectedBatch(String batchCode) {
		try {
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			By chkboxBatchtoDelete=By.xpath("//table[@id='BatchGridInWork']//td[text()='"+batchCode+"']/preceding-sibling::td/input[contains(@id,'chkRowSelector')]");
			waitForElementToBoClickable(chkboxBatchtoDelete, " Batch Checkbox", 60);
			JSClick(chkboxBatchtoDelete, " Batch Checkbox");
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(lnkDeleteSelectedBatches, " Delete Selected Batches");
				Thread.sleep(3000);
				AlertAccept();

			}
			else{
				JSAcceptAlert();
				Thread.sleep(3000);
				JSClick(lnkDeleteSelectedBatches, " Delete Selected Batches");
			}
			waitForSeconds(5);
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			waitForElementToBoClickable(lblNoBatchElement, "No Element in Grid", 60);
			if(isElementPresent(lblNoBatchElement, "No Element in Grid", true))
				this.reporter.successwithscreenshot("Selected Batch  Deletion", "Batch   "+batchCode+" deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Selected Batch  Deletion", "Batch  not deleted ", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Delete_ batch.
	 */
	public void delete_Batch(String batchCode) {
		try {
			waitForSeconds(5);
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			By iconDelete=By.xpath("//table[@id='BatchGridInWork']//td[text()='"+batchCode+"']/following-sibling::td/input[@alt='Delete Batch']");
			waitForElementToBoClickable(iconDelete, " Delete Icon", 120);
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(iconDelete, " Delete Batch icon");
				Thread.sleep(6000);
				AlertAccept();

			}
			else{
				JSAcceptAlert();
				Thread.sleep(6000);
				JSClick(iconDelete, " Delete Batch icon");
			}
			waitForSeconds(5);
			waitForElementToBoClickable(inWorkFilterBox, " Filter box", 60);
			enterFilterbox(inWorkFilterBox,batchCode);
			waitForSeconds(5);
			waitForElementToBoClickable(lblNoBatchElement, "No Element in Grid", 60);
			if(isElementPresent(lblNoBatchElement, "No Element in Grid", true))
				this.reporter.successwithscreenshot("Batch  Deletion", "Batch   "+batchCode+" deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Batch  Deletion", "Batch  not deleted ", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
}
