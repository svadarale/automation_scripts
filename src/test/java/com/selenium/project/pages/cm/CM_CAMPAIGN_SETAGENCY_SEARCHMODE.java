package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_SETAGENCY_SEARCHMODE extends ActionEngine implements Page {
	
	/** Option set. */
	By Optionset = By.xpath("//a[contains(.,'Option Sets')]");
	
	/** Designation panel. */
	By Designation_panel = By.xpath(".//*[@id='OptionSetTabStrip_ctl10_ctl00']");
	
	/** Designation Name. */
	By Designation_Name = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetGrid']/tbody/tr[1]/td[1]/a");
	
	/** Designation Description. */
	By Designation_Description = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetGrid']/tbody/tr[1]/td[2]");
	
	/** Designation ViewEdit button. */
	By Designation_ViewEdit_button = By.cssSelector("[id*='ViewEditButton']");
	
	/** Designation Copy button. */
	By Designation_Copy_button =  By.cssSelector("[id*='QuickCopyButton']");
	
	/** Designation Delete button. */
	By Designation_Delete_button = By.cssSelector("[id*='DeleteButton']");
	
	/** Add a new designation option set. */
	By Add_a_new_designation_option_set = By.xpath(".//*[@id='OptionSetPlate_DesignationOptionSetAddButton']");
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM CAMPAIGN SETAGENCY SEARCH MODE.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_SETAGENCY_SEARCHMODE(WebDriver driver, CReporter reporter,Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		
		try {
			By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
			By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
			By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
			/** Clicking on Campaign Node  . */               
			SwitchToFrame(By.name("tree"),"Tree Frame");
			click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
			click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
			click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
			JavascriptExecutor js = (JavascriptExecutor) this.Driver;
			js.executeScript("window.scrollBy(0,10)", "");
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(Optionset));
			/** Click  the Optionset.*/
			JSClick(Optionset, "Option set");
			SwitchToFrame(By.name("content"), "option set Frame");
			} catch (Throwable e) 
		{
		
			e.printStackTrace();
		}

	}

	@Override
	public void move() {


		try {
			/** click the Designation panel  */
			Thread.sleep(5000);
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(Designation_panel));
			click(Designation_panel, "Designation panel");
			SwitchToFrame(By.name("content"), "option set Header Frame");
			Thread.sleep(2000);
			/** Verify the Designation panel Header name fields  */
			verifyText(Designation_Name,"Name","Designation Name");
			verifyText(Designation_Description,"Description","Desiganation Description");
			Thread.sleep(2000);		
			boolean ViewEdit_button = isElementPresent(Designation_ViewEdit_button,"Designation View Edit button", true);
			if(ViewEdit_button){
				this.reporter.successwithscreenshot("User is able to verify add a new desigantion View Edit button", "User is able to verify add a new desigantion View Edit button",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new desigantion View Edit button", "User is not able to verify add a new desigantion View Edit button", this.Driver);
			}
			boolean Copy_button = isElementPresent(Designation_Copy_button,"Designation Copy button", true);
			if(Copy_button){
				this.reporter.successwithscreenshot("User is able to verify add a new desigantion Copy button", "User is able to verify add a new desigantion Copy button",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new desigantion Copy button", "User is not able to verify add a new desigantion Copy button", this.Driver);
			}
			boolean designation_option_set = isElementPresent(Add_a_new_designation_option_set,"add a new desigantion option set", true);
			if(designation_option_set){
				this.reporter.successwithscreenshot("User is able to verify add a new desigantion option set", "User is able to verify add a new desigantion option set",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify add a new desigantion option set", "User is not able to verify add a new desigantion option set", this.Driver);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}

