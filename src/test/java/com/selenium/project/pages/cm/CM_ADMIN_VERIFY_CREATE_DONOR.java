package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;








import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;


/**
 * The Class CM_ADMIN_VERIFY_CREATE_DONOR.
 */
public class CM_ADMIN_VERIFY_CREATE_DONOR extends ActionEngine implements Page {
		
		/** The donor_expand. */
		By donor_expand = By.xpath("//a[contains(.,'Donors')]");
		
		/** The filter. */
		By filter	= By.cssSelector("[id*='FilterTextBox']");
		
		/** The filter_Identifier. */
		By filter_Identifier	= By.xpath("//*[@id='Campaign_DonorGrid']/tbody/tr[2]/td[2]");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CM_ADMIN_VERIFY_CREATE_DONOR.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_CREATE_DONOR(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				Thread.sleep(4000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				Thread.sleep(3000);
				click(donor_expand, "Expanding Donor Node under Campaigns");
				Thread.sleep(9000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);	
				FilterCheck(filter,filter_Identifier,CM_ADMIN_CREATE_DONOR.str);
				
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}