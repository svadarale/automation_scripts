package com.selenium.project.pages.cm;


import java.awt.Robot;
import java.util.Properties;
import java.awt.event.KeyEvent;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;

import java.awt.AWTException;


//import util.WebUtils;
















import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;


/**
 * The Class CM_CAMPAIGN_ADD_IMPORT.
 */
public class CM_CAMPAIGN_ADD_IMPORT extends ActionEngine implements Page {
		
		/** The specialEvents_Expand. */
		By specialEvents_Expand= By.xpath("//a[contains(.,'Special Events')]");
		
		/** The filter. */
		By filter = By.cssSelector("#SpecialEventPlate_SpecialEventGrid_FilterTextBox");
	
		/** The filter_Identifier. */
		By filter_Identifier = By.xpath(".//*[@id='SpecialEventPlate_SpecialEventGrid']/tbody/tr[2]/td[2]");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new CAMPAIGN ADD NEW IMPORT
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_ADD_IMPORT(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
			try {
				Thread.sleep(4000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(6000);
				click(specialEvents_Expand, "Expanding Special Events Node under Campaigns");
				Thread.sleep(5000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				FilterCheck(filter,filter_Identifier,(String)params.get("eventName"));
				Thread.sleep(5000);	
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}