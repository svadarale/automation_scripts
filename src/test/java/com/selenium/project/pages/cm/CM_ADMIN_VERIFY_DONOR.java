package com.selenium.project.pages.cm;


import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;





import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_DONOR_GROUP.
 */
public class CM_ADMIN_VERIFY_DONOR extends ActionEngine implements Page {
		
		/** The donor_expand. */
		By donor_expand = By.xpath("//a[contains(.,'Donors')]");
		
		/** The donor_list. */
		By donor_list	= By.xpath("//td[contains(.,'Donor List')]");
		
		/** The identifier_col. */
		By identifier_col	= By.xpath("//a[contains(.,'Identifier')]");
		
		/** The name_col. */
		By name_col	= By.xpath("//a[contains(.,'Name')]");
		
		/** The company_level_col. */
		By company_level_col	= By.xpath("//a[contains(.,'Company Level')]");
		
		/** The company_pay_period_col. */
		By company_pay_period_col	= By.xpath("//a[contains(.,'Company Pay Period')]");
		
		/** The donor_group_col. */
		By donor_group_col	= By.xpath("//a[contains(.,'Donor Group')]");
		
		/** The donated_col. */
		By donated_col	= By.xpath("//a[contains(.,'Donated')]");
		
		/** The active_col. */
		By active_col	= By.xpath("//a[contains(.,'Active')]");
		
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe donor group.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_DONOR(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(3000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(3000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(3000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(3000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(3000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}	
		}

		@Override
		public void move() {			
			try {
				Thread.sleep(2000);
				click(donor_expand, "Expanding Websites Node under Campaigns");
				Thread.sleep(2000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(3000);
				boolean validationPoint = isElementPresent(donor_list, "donor list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("donor  list is present", "donor element is present",this.Driver);
				}else{
					this.reporter.failureReport("donor list is not present", "donor list element is not present", this.Driver);
				}
				isElementPresent(donor_list, "donor list is present", true);
				Thread.sleep(1000);
				isElementPresent(name_col, "name is present", true);
				Thread.sleep(1000);
				isElementPresent(company_level_col, "company level is present", true);
				Thread.sleep(1000);
				isElementPresent(company_pay_period_col, "company pay period is present", true);
				Thread.sleep(1000);
				isElementPresent(donor_group_col, "donor group is present", true);
				Thread.sleep(1000);
				isElementPresent(donated_col, "donated is present", true);
				Thread.sleep(1000);
				isElementPresent(active_col, "active is present", true);
				Thread.sleep(1000);
			} catch (Throwable e) {
				e.printStackTrace();
			}
		}
	}