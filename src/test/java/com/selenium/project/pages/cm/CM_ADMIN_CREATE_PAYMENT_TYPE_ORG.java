package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;






import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_CREATE_SPE_WEBSITE.
 */
public class CM_ADMIN_CREATE_PAYMENT_TYPE_ORG extends ActionEngine implements Page {
		
		/** The add_new_web. */
		By add_new_payment_type_set = By.xpath("//a[contains(.,'Add a new payment type set')]"); 
		
		/** The name. */
		By name =By.xpath("//input[@id='OptionSetPlate_OptionSetNameTextBox']");
		
		/** The description. */
		By description =By.xpath("//textarea[@id='OptionSetPlate_OptionSetDescriptionTextBox']");
		
		/** The add_new_payment_type. */
		By add_new_payment_type = By.xpath("//a[@id='OptionSetPlate_PaymentTypeOptionAddButton']");
		
		/** The Allow One Time Payment. */
		By allow_one_type_pyt =By.xpath("//input[@id='OptionSetPlate_PaymentTypeOptionSubPlate_AllowOneTimePaymentCheckBox']");
		
		/** allow_per_pay_prd. */
		By allow_per_pay_prd=By.xpath("//input[@id='OptionSetPlate_PaymentTypeOptionSubPlate_AllowPerPayPeriodPaymentCheckBox']");
		
		/** activate_icon. */
		By activate_icon = By.xpath("//td[contains(.,'Payroll Deduction')]/descendant::input[@alt='Activate Payment Type']");
		
		/** deactivate_icon. */
		By deactivate_icon = By.xpath("//td[contains(.,'Payroll Deduction')]/descendant::input[@alt='Deactivate Payment Type']");
		
		/** The sig_question. */
		By sig_question =By.xpath("//input[@id='OptionSetPlate_PaymentTypeOptionSubPlate_SignatureQuestionTextBox']");
		
		/** The save_button. */
		By save_button = By.xpath("//a[@id='SaveButton']");

				
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin create spe website.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_CREATE_PAYMENT_TYPE_ORG(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				Thread.sleep(4000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(4000);
				click(add_new_payment_type_set, "add_new_payment_type_set");
				String strname = strwithtimestamp((String)params.get("name"));
				Thread.sleep(3000);
				type(name, strname, "name value");
				Thread.sleep(3000);
				type(description, (String)params.get("description"), "description value");
				Thread.sleep(3000);
				click(save_button, "save_button");
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}	
		
		@Override
		public void move() {
				try {
				Thread.sleep(3000);
				click(add_new_payment_type, "add_new_payment_type");
				Thread.sleep(2000);
				type(sig_question, (String)params.get("sig_question"), "sig_question");
				Thread.sleep(2000);
				click(allow_one_type_pyt, "allow_one_type_pyt");
				Thread.sleep(2000);
				click(allow_per_pay_prd, "allow_per_pay_prd");
				Thread.sleep(2000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToBottom();
			     }*/
				click(save_button, "save_button");
				Thread.sleep(2000);
				click(activate_icon, "activate_icon");
				Thread.sleep(2000);
				boolean validationPoint = isElementPresent(deactivate_icon, "deactivate_icon is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("deactivate_icon element is present", "deactivate_icon element is present",this.Driver);
					Thread.sleep(2000);
					click(deactivate_icon, "activate_icon");
				}else{
					this.reporter.failureReport("deactivate_icon element is present", "deactivate_icon element is not present", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}