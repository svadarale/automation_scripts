package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE.
 */
public class CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE extends ActionEngine implements Page {
			
		/** The campagins_expand. */
		By campagins_expand = By.xpath(".//*[@alt='Expand Campaigns']");
		
		/** The websites_expand. */
		By websites_expand = By.xpath("//a[contains(.,'Websites')]");
		
		/** The website_list. */
		By website_list	= By.xpath("//td[contains(.,'Website List')]");
		
		/** The add_new_website_link. */
		By add_new_website_link	= By.xpath("//a[contains(.,'Add a new website')]");
		
		/** The content_icon. */
		//By edit_icon = By.cssSelector("#WebsiteGrid_0a9727a3f-9bf5-44da-943d-f912d3fb237d_0[alt='View / Edit Website']");
	//	By edit_icon = By.xpath("html/body/form/table/tbody/tr[3]/td[2]/table[2]/tbody/tr[2]/td[6]/input[1]");
		By content_icon = By.cssSelector(".EnhancedDataGridItem [alt='View / Edit Content']");
		
		/** The content_tab. */
		//By content_tab = By.xpath("//a[contains(.,'Content')]");
		//By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00");
		By content_tab = By.cssSelector("#WebsiteTabStrip_ctl04_ctl00_Image");
		
		
		/** The donation_dropdown. */
		By donation_dropdown= By.xpath("//*[@id='WebsitePlate_ContentPlateDropDownList_ContentPlateDropDownList']");
		
		/** The show_donation_history. */
		By show_donation_history = By.cssSelector("#WebsitePlate_ShowDonationHistory_CheckBox");
		
		/** The current_campaign_donations. */
		By current_campaign_donations = By.cssSelector("#WebsitePlate_ShowCurrentCampaignDonation_CheckBox");
		
		/** The cancel_all_designation_textbox. */
		By cancel_all_designation_textbox  = By.cssSelector("#WebsitePlate_DonationCancelAllDesignationsWarning_TextBox");

		/** The save_button. */
		By save_button = By.cssSelector("#SaveButton");
						
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new Configure Website Content for "Donation History Page.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
						try {
		By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
		By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
		By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
		/** Clicking on Campaign Node  . */               
		Thread.sleep(5000);
		SwitchToFrame(By.name("tree"),"Tree Frame");
		Thread.sleep(5000);
		click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
		Thread.sleep(3000);
		click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
		Thread.sleep(3000);
		click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
		Thread.sleep(4000);
	    click(websites_expand, "websites");
	
			} catch (Throwable e) {
				e.printStackTrace();
			}	
		}
		@Override
		public void move() {
			try {
				Thread.sleep(5000);
			    SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				isElementPresent(website_list, "website list", true);
				Thread.sleep(2000);
				isElementPresent(add_new_website_link, "Add new website link", true);
				Thread.sleep(6000);
				boolean validationPoint = isElementPresent(add_new_website_link, "Add new website link", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Is Add new website is present", "Add new website link is present",this.Driver);
				}else{
					this.reporter.failureReport("Add new website is not present", "Add new website link is not present", this.Driver);
				}
				Thread.sleep(6000);
				click(content_icon, "edit icon");
				//Thread.sleep(60000);
		     	//	waitForElementPresent(content_tab, "content tab", 250000);
				
				//JSClick(content_tab, "content tab");
				//click(content_tab, "content tab");
				Thread.sleep(8000);
				selectBySendkeys(donation_dropdown, (String)params.get("donation.dropdown"), "Website");
				Thread.sleep(2000);
				this.reporter.successwithscreenshot("Is Show Donation History checkbox is selected", "Show Donation History checkbox is selected",this.Driver);
				Thread.sleep(2000);
				if(!(show_donation_history.findElement(Driver).isSelected())){
					click(show_donation_history,"Selecting Show Donation History checkbox");}
				Thread.sleep(2000);
				if(!(current_campaign_donations.findElement(Driver).isSelected())){
					click(current_campaign_donations,"Selecting Show Current Campaign Donations checkbox");}
				Thread.sleep(2000);
				//type(cancel_all_designation_textbox, (String)params.get("cancel.all.designation"), "Donation Cancel All Designations Warning textbox");
				javascriptSendKeys((String)params.get("cancel.all.designation"), this.Driver.findElement(cancel_all_designation_textbox), "Donation Cancel All Designations Warning textbox");
				Thread.sleep(2000);
				click(save_button, "Save / Update"); 
				Thread.sleep(2000);
				/*boolean validationPoint2 = isChecked(show_donation_history, "Selecting Show Donation History checkbox");
				if(validationPoint2){
					this.reporter.successwithscreenshot("Is Show Donation History checkbox is selected", "Show Donation History checkbox is selected",this.Driver);
				}else{
					this.reporter.failureReport("Show Donation History checkbox is not selected", "Show Donation History checkbox is not selected", this.Driver);
				}
				Thread.sleep(2000);
				String s = getText(cancel_all_designation_textbox, "Donation Cancel All Designations Warning textbox");
				String s2 = (String)params.get("cancel.all.designation");
				if(s.contains(s2)){
					this.reporter.successwithscreenshot("Is text is present", " Text is present",this.Driver);
				}else{
					this.reporter.failureReport("Text is not present", "Text is not present", this.Driver);
				}*/
				
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}