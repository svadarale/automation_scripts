package com.selenium.project.pages.cm;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

// TODO: Auto-generated Javadoc

/**
 * The Class CM__Add_Delete_Domain.
 */
public class CM__SEND_EMAIL extends ActionEngine implements Page {

	/** The expand_ campaign. */
	By expand_Companies = By.xpath(".//*[@alt='Expand Companies']");

	/** The expand_ campaign. */
	By expand_Campaign = By.xpath(".//*[@alt='Expand Campaigns']");

	/** The lnk donors. */
	By lnkDonors = By.xpath("//a[text()='Donors']");

	/** The lnk email. */
	By lnkEmail = By.xpath("//a[text()='Email']");

	/** The btn impersonate. */
	By btnImpersonate=By.cssSelector("input[id$='ImpersonateButton']");

	/** The lnk dash board. */
	By lnkDashBoard =By.xpath("//a[contains(text(),'Dashboard')]");

	/** The filter box. */
	By filterBox=By.cssSelector("input[id$='FilterTextBox']");

	/** The button send email. */
	By buttonSendEmail=By.cssSelector("a[id$='SendEmailButton']");

	/** The dropdown domain. */
	By dropdownDomain=By.cssSelector("select[id$='CompanyDomainDropDownList']");

	/** The txtbox subject. */
	By txtboxSubject=By.id("ViewControl_ContentArea1_SubjectTextBox");


	/** The button continue. */
	By buttonContinue=By.cssSelector("a[id$='ContinueButton']");


	/** The link send test email. */
	By linkSendTestEmail=By.cssSelector("a[id$='TestEmailButton']");

	/** The lbl send email. */
	By lblSendEmail=By.xpath("//p[contains(text(),'Please review your e-mail settings/message below')]");

	/** The lbl emailsucc msg. */
	By lblEmailsuccMsg=By.xpath("//span[@id='ViewControl_ContentArea1']/p[contains(text(),'Your email has been successfully queued')]");

	/** The lbl test emailsucc msg. */
	By lblTestEmailsuccMsg=By.xpath("//div[contains(text(),'Your test email has been successfully queued')]");


	/** The lnk view email history. */
	By lnkViewEmailHistory=By.xpath("//a[text()='View Email History']");


	/** The lnk add new email. */
	By lnkAddNewEmail=By.id("CampaignPlate_EmailAddButton");

	/** The lbl description. */
	By lblDescription=By.xpath("//td[text()='Description']");

	/** The txtbox description. */
	By txtboxDescription=By.id("CampaignPlate_EmailPanel_DescriptionTextBox");

	/** The txtbox from name. */
	By txtboxFromName=By.id("CampaignPlate_EmailPanel_EmailTable_FromNameTextBox");

	/** The txtbox from address. */
	By txtboxFromAddress=By.id("CampaignPlate_EmailPanel_EmailTable_FromAddressTextBox");

	/** The txtbox new email subject. */
	By txtboxNewEmailSubject=By.id("CampaignPlate_EmailPanel_EmailTable_SubjectTextBox");

	/** The txtbox reply to address. */
	By txtboxReplyToAddress=By.id("CampaignPlate_EmailPanel_EmailTable_ReplyAddressTextBox");

	/** The txtbox text body. */
	By txtboxTextBody=By.id("CampaignPlate_EmailPanel_EmailTable_BodyTextBox");


	/** The button save queue. */
	By buttonSaveQueue=By.id("SaveQueueButton");

	/** The linkforgot password. */
	By linkforgotPassword=By.xpath("//a[contains(text(),'Forgot Password')]");

	/** The txtbox campaign code. */
	By txtboxCampaignCode=By.cssSelector("input[id$='CampaignCodeTextBox']");

	/** The txtbox campaign code. */
	By txtboxUsername=By.cssSelector("input[id$='UserNameTextBox']");

	/** The link continue. */
	By linkContinue=By.xpath("//a[contains(text(),'continue')]");

	/** The link email password. */
	By linkEmailPassword=By.xpath("//a[contains(text(),'Email Password')]");

	/** The success email message. */
	By successMsgNewPassword =By.xpath("//table[@class='FormPanel']//p[contains(text(),'new password')]");


	/** The mail subject. */
	public String mailSubject="Test_"+generateRandomString(4, Mode.ALPHA);
	/** The params. */
	Properties params = new Properties();

	/**
	 *  The domain name.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */

	/**
	 * Instantiates a new c m_ add_ delete_ batch.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM__SEND_EMAIL(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#fill()
	 */
	@Override
	public void fill() {
		try {

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#move()
	 */
	@Override
	public void move() {

		try {

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Navigate_ company.
	 */
	public void navigate_Company() {
		try {

			Thread.sleep(3000);
			/*if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}*/
			/** The expand_s. */
			By expand_S =By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CompanyInitial")+"']");

			/** The expand_sprint16_campaignclassic1. */
			By expand_Company = By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CampanyName")+"']");
			if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}
			waitForVisibilityOfElement(By.name("tree"),"Tree Frame");
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(expand_Companies,"expand_Companies");
			JSClick(expand_Companies, "expand_Companies");
			waitForVisibilityOfElement(expand_S,"Expand S icon");
			JSClick(expand_S, "Expand S icon");
			waitForVisibilityOfElement(expand_Company,"Expand Sprint16Company");
			JSClick(expand_Company, "Expand Sprint16Company");


		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Navigate_ campaign.
	 */
	public void navigate_Campaign() {
		try {

			/** The expand_s. */
			By expand_s =By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CompaignInitial")+"']");
			/** The expand_sprint16_campaignclassic1. */
			By expand_CampaignName = By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CampaignName")+"']");

			Thread.sleep(3000);
			if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}
			waitForVisibilityOfElement(By.name("tree"),"Tree Frame");
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForSeconds(3);
			waitForVisibilityOfElement(expand_Campaign,"Expand_Campaign");
			click(expand_Campaign, "Expand_Campaign");
			waitForSeconds(5);
			waitForVisibilityOfElement(expand_s,"Expand S icon");
			click(expand_s, "Expand S icon");
			waitForSeconds(5);
			waitForVisibilityOfElement(expand_CampaignName,"Expand Sprint21CampaignClassic1s");
			click(expand_CampaignName, "Expand Sprint21CampaignClassic1");
			waitForSeconds(5);

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	public void navigateToCampaign() {
		try {

			/** The expand_s. */
			By expand_s =By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CompaignInitial")+"']");
			/** The expand_sprint16_campaignclassic1. */
			By expand_CampaignName = By.xpath(".//*[@alt ='Expand "+this.params.getProperty("CampaignName")+"']");
			waitForSeconds(3);
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForSeconds(3);
			JSClick(expand_Campaign, "Expand_Campaign");
			waitForSeconds(5);
			JSClick(expand_s, "Expand S icon");
			waitForSeconds(5);
			JSClick(expand_CampaignName, "Expand Sprint21CampaignClassic1");
			waitForSeconds(5);

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Navigate to opcs.
	 */
	public void navigateToOPCS() {
		try{
			waitForVisibilityOfElement(lnkDonors,"Donors");
			click(lnkDonors, "Donors");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			waitForSeconds(2);
			String donorName=this.params.getProperty("donorName");
			By donorNameInTable =By.xpath("//td[text()='"+donorName+"']");
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,donorName);
			waitForSeconds(5);
			ScrollToElement(btnImpersonate);
			waitForSeconds(2);
			if(isIE())
				JSClick(btnImpersonate," Impersonate Button");
			else
				click(btnImpersonate," Impersonate Button");
			waitForSeconds(5);
			windowHandles(this.Driver);
			waitForSeconds(10);
			waitForElementPresent(lnkDashBoard, "DashBoard", 120);
			JSClick(lnkDashBoard," DashBoard");
			waitForSeconds(2);
			waitForElementPresent(buttonSendEmail, "Send Email link", 120);
			ScrollToElementVisible(buttonSendEmail);
			waitForSeconds(2);
			JSClick(buttonSendEmail," Send Email");
			waitForSeconds(2);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Send email.
	 *
	 * @return the string
	 */
	public String sendEmail() {
		try{

			selectByVisibleText(dropdownDomain,this.params.getProperty("DomainName"),"Domain name");
			type(txtboxSubject, mailSubject, "Subject Textbox");
			waitForSeconds(3);
			Robot robot = new Robot();
			if(isIE()||isEdge()){
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);	
			}
			else{
				By btnNumlist=By.xpath("//a[contains(@class,'numberedlist')]");
				JSClick(btnNumlist,"Html Body");
			}
			waitForSeconds(2);
			StringSelection ss = new StringSelection(mailSubject);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			waitForSeconds(2);
			JSClick(buttonContinue, "Continue");
			waitForSeconds(2);
			waitForVisibilityOfElement(lblSendEmail,"Please review your email");
			if(isElementPresent(lblSendEmail, "Please review your email", true))
				this.reporter.successwithscreenshot("Verfiy Send-Email Page", "User is able to navigated to Send Email Page successfully", this.Driver);
			else
				this.reporter.failureReport("Verfiy Send-Email Page", "Unable to navigate to Send Email Page ", this.Driver);
			JSClick(buttonContinue, "Continue");
			waitForSeconds(2);
			waitForVisibilityOfElement(lblEmailsuccMsg,"Email Success Message");
			if(isElementPresent(lblEmailsuccMsg, "Email Success Message", true))
				this.reporter.successwithscreenshot("Verfiy Send-Email to donors", "email has been successfully queued for delivery", this.Driver);
			else
				this.reporter.failureReport("Verfiy Send-Email to donors", "Unable to Send Email to Donors ", this.Driver);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return mailSubject;

	}

	/**
	 * Send test email.
	 *
	 * @return the string
	 */
	public String sendTestEmail() {
		try{

			selectByVisibleText(dropdownDomain,this.params.getProperty("DomainName"),"Domain name");
			mailSubject="Test_"+generateRandomString(4, Mode.ALPHA);
			type(txtboxSubject, mailSubject, "Subject Textbox");
			waitForSeconds(2);
			Robot robot = new Robot();
			if(isIE()||isEdge()){
				robot.keyPress(KeyEvent.VK_TAB);
				robot.keyRelease(KeyEvent.VK_TAB);	
			}
			else{
				By btnNumlist=By.xpath("//a[contains(@class,'numberedlist')]");
				JSClick(btnNumlist,"Html Body");
			}
			waitForSeconds(2);
			StringSelection ss = new StringSelection(mailSubject);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			waitForSeconds(2);
			JSClick(buttonContinue, "Continue");
			//tab(txtboxSubject,"Subject Textbox");
			/*SwitchToFrame(By.className("cke_wysiwyg_frame cke_reset"), " HTML body frame");
			By htmlBody=By.xpath("//html/body/p");
			By btnNumlist=By.xpath("//a[contains(@class,'numberedlist')]/span[1]");
			JSClick(btnNumlist,"Html Body");
			StringSelection ss = new StringSelection(mailSubject);
			Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);
			Robot robot = new Robot();
			robot.keyPress(KeyEvent.VK_CONTROL);
			robot.keyPress(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_V);
			robot.keyRelease(KeyEvent.VK_CONTROL);
			JSClick(buttonContinue, "Continue");*/
			waitForVisibilityOfElement(lblSendEmail,"Please review your email");
			if(isElementPresent(lblSendEmail, "Please review your email", true))
				this.reporter.successwithscreenshot("Verfiy Send-Email Page", "User is able to navigated to Send Email Page successfully", this.Driver);
			else
				this.reporter.failureReport("Verfiy Send-Email Page", "Unable to navigate to Send Email Page ", this.Driver);
			JSClick(linkSendTestEmail, "Send Test Email");
			waitForVisibilityOfElement(lblEmailsuccMsg,"Email Success Message");
			if(isElementPresent(lblTestEmailsuccMsg, "Test Email Success Message", true))
				this.reporter.successwithscreenshot("Verfiy Send-Test-Email to donors", "Test email has been successfully queued for delivery", this.Driver);
			else
				this.reporter.failureReport("Verfiy Send-Test-Email to donors", "Unable to Send Test Email ", this.Driver);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return mailSubject;

	}

	/**
	 * View email history.
	 *
	 * @param subject the subject
	 */
	public void viewEmailHistory(String subject) {
		try{
			JSClick(lnkViewEmailHistory, "View Email History");
			By queuedEmailInGrid=By.xpath("//table[contains(@id,'EmailGrid')]//tr//td[text()='"+subject+"']//following-sibling::td[contains(text(),'Queued')]");
			By sentEmailInGrid=By.xpath("//table[contains(@id,'EmailGrid')]//tr//td[text()='"+subject+"']//following-sibling::td[contains(text(),'Sent')]");
			boolean queuedEmailFlag=isElementPresent(queuedEmailInGrid, "Email Which is Queued", true);
			//boolean sentEmailFlag=isElementPresent(sentEmailInGrid, "Email Which is Sent", true);
			if(queuedEmailFlag)
				this.reporter.successwithscreenshot("Verfiy View Email History", "email has been verified in Email History List" , this.Driver);
			else
				this.reporter.failureReport("Verfiy View Email History", "Failed to verify email in  Email History List", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Creates the new email.
	 */
	public void createNewEmail() {
		try {
			waitForSeconds(2);
			waitForVisibilityOfElement(lnkEmail,"Email");
			JSClick(lnkEmail, "Email");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			if(isElementPresent(lnkAddNewEmail, "Add New Email Link", true))
				this.reporter.successwithscreenshot("Verfiy Email Page", "user is able to navigate to Email Page", this.Driver);
			else
				this.reporter.failureReport("Verfiy Email Page", "Failed to navigate Email Page ", this.Driver);
			JSClick(lnkAddNewEmail, "Add New Email");
			waitForVisibilityOfElement(lblDescription,"Description",120);
			if(isElementPresent(lblDescription, "Description", true))
				this.reporter.successwithscreenshot("Verfiy Add New Email Page", "user is able to navigate to Add New Email Page", this.Driver);
			else
				this.reporter.failureReport("Verfiy Add New Email Page", "Failed to navigate Add New Email Page ", this.Driver);
			//String description=this.params.getProperty("Description")+getUniqueID();
			String description="STD"+getUniqueID();
			type(txtboxDescription,description,"Description");
			type(txtboxFromName,generateRandomString(5, Mode.ALPHA),"From Name");
			type(txtboxFromAddress,generateRandomString(5, Mode.ALPHA)+"@mailinator.com","From Address");
			type(txtboxReplyToAddress,generateRandomString(5, Mode.ALPHA)+"@mailinator.com","Reply To Address");
			type(txtboxNewEmailSubject,"Test_" +generateRandomString(5, Mode.ALPHA),"Subject");
			type(txtboxTextBody,"Test_" +generateRandomString(5, Mode.ALPHA),"Test Body");
			ScrollToElementVisible(buttonSaveQueue);
			waitForSeconds(2);
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(buttonSaveQueue, "SaveQueue");
				Thread.sleep(3000);
				AlertAccept();
			}
			else{
				JSAcceptAlert();
				Thread.sleep(3000);
				JSClick(buttonSaveQueue, "SaveQueue");
			}
			waitForSeconds(5);
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,description);
			waitForSeconds(5);
			//By lblEmailDescriptionIngrid=By.xpath("//td[text()='"+description+"']");
			By lblEmailDescriptionIngrid=By.xpath(".//table[@id='CampaignPlate_EmailGrid']/tbody/tr[2]/td[contains(text(),'"+description+"')]");
			if(isElementPresent(lblEmailDescriptionIngrid, "New Email In Grid", true))
				this.reporter.successwithscreenshot("Verfiy Campaign Welcome Email", "New Email has been saved successfully In Email Grid ", this.Driver);
			else
				this.reporter.failureReport("Verfiy Campaign Welcome Email", "Failed to create New Campaign Email ", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

	/**
	 * Send forgot password email.
	 */
	public void sendForgotPasswordEmail() {
		try {

			waitForSeconds(15);
			waitForElementPresent(linkforgotPassword, "Forgot Password", 15);
			JSClick(linkforgotPassword, "Forgot Password");
			waitForSeconds(15);
			waitForElementPresent(txtboxCampaignCode, "Camaign Code", 30);
			if(isIE()||isEdge()){
				javascriptSendKeys(this.params.getProperty("opcs.camcode"), this.Driver.findElement(txtboxCampaignCode), "Camaign Code");
			}
			else{
				type(txtboxCampaignCode, this.params.getProperty("opcs.camcode"), "Camaign Code");
			}
			Thread.sleep(3000);
			waitForElementPresent(txtboxUsername, "User name", 30);
			if(isIE()||isEdge()){
				javascriptSendKeys(this.params.getProperty("opcs.username"), this.Driver.findElement(txtboxUsername), "User name");
			}
			else{
				type(txtboxUsername, this.params.getProperty("opcs.username"), "User name");
			}
			Thread.sleep(2000);
			JSClick(linkContinue, "Continue");
			waitForSeconds(10);
			waitForElementPresent(linkEmailPassword,"Email Password", 30);
			JSClick(linkEmailPassword, "Email Password");
			waitForSeconds(5);
			waitForElementPresent(successMsgNewPassword,"Your new password has been emailed to you.", 30);
			if(isElementPresent(successMsgNewPassword, "Your new password has been emailed to you.", true))
				this.reporter.successwithscreenshot("Verfiy Send Forgot Password Email", "New Password has been mailed successfully ", this.Driver);
			else
				this.reporter.failureReport("Verfiy Send Forgot Password Email", "Failed to mail  New Password ", this.Driver);


		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
	}

}
