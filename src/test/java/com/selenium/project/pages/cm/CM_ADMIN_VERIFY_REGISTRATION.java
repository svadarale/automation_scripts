package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;



import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;


/**
 * The Class CM_ADMIN_VERIFY_SPE_REGISTRATION.
 */
public class CM_ADMIN_VERIFY_REGISTRATION extends ActionEngine implements Page {
			
		/** The campagins_expand. */
		By campagins_expand = By.xpath("//*[@alt='Expand Campaigns']");
		
		/** The campagins_ a_expand. */
		By campagins_A_expand =  By.xpath("//*[@alt='Expand A']"); 
		
		/** The acme_expand. */
		By acme_expand = By.xpath("//*[@alt='Expand Acme New 2 [AcmeNew2]']");
		
		/** The registration_expand. */
		By registration_expand = By.xpath(".//*[text()='Registration']");
		
		/** The campaign_name. */
		By campaign_name	= By.xpath("//td[contains(.,'Campaign (Acme New 2)')]");
						
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe registration.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_REGISTRATION(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
				Thread.sleep(3000);
				By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
				By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
				By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
				/** Clicking on Campaign Node  . */               
				Thread.sleep(5000);
				SwitchToFrame(By.name("tree"),"Tree Frame");
				Thread.sleep(5000);
				click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
				Thread.sleep(5000);
				click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
				Thread.sleep(5000);
				click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
				Thread.sleep(4000);

			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
		
		
		@Override
		public void move() {
			
			try {
				Thread.sleep(1000);
				click(registration_expand, "Expanding Websites Node under Campaigns");
				Thread.sleep(5000);
				SwitchToFrame(By.name("content"),"Main Content Frame");
				Thread.sleep(9000);
				WebDriverWait wait = new WebDriverWait(this.Driver, 500);
				wait.until(ExpectedConditions.presenceOfElementLocated(campaign_name));
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
			    ScrollToTop();
			     }*/
				boolean validationPoint = isElementPresent(campaign_name, "Verify Campain name is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Verify Campain name is present", "Verify Campain name is present",this.Driver);
				}else{
					this.reporter.failureReport("Verify Campain name is present", "Verify Campain name is not present", this.Driver);
				}
				Thread.sleep(5000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}

	}