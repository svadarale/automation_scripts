package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;
import com.automation.utilities.Locator;
import com.thoughtworks.selenium.webdriven.commands.IsEditable;

// TODO: Auto-generated Javadoc
/**
 * The Class CM__ADD_Delete_Batch_Template.
 */
public class CM__ADD_Delete_Batch_Template extends ActionEngine implements Page {

	
	/** The expand_ batches. */
	By expand_Batches = By.xpath(".//*[@alt='Expand Batches']");
	
	
	/** The lnk templates. */
	By lnkTemplates = By.xpath("//a[text()='Templates']");


	/** The lnk add new batch template. */
	By lnkAddNewBatchTemplate = By.xpath("//a[@id='AddButton']");
		
	/** The lbl temaplate name. */
	By lblTemaplateName = By.xpath("//td[text()='Template Name']");
		
	
	/** The txtbox template name. */
	By txtboxTemplateName = By.id("BatchTemplatePlate_TemplateNameTextBox");
	
	
	/** The dropdown option package. */
	By dropdownOptionPackage = By.id("BatchTemplatePlate_OptionPackageIdDropDownList");
	
	/** The dropdown donor group. */
	By dropdownDonorGroup = By.id("BatchTemplatePlate_DonorGroupIdDropDownList");
	
	/** The dropdown batch group. */
	By dropdownBatchGroup = By.id("BatchTemplatePlate_BatchGroupIdDropDownList");
	
	/** The txtbox batch template code. */
	By txtboxBatchTemplateCode = By.id("BatchTemplatePlate_CodeTextBox");
	
	/** The chkbox acc num host identification. */
	By chkboxAccNumHostIdentification = By.xpath("//label[text()='By Identification']/preceding-sibling::input[starts-with(@id,'BatchTemplatePlate_HostCatalogOrganizationSelectControl')]");
	
	/** The chkbox def desig identification. */
	By chkboxDefDesigIdentification = By.xpath("//label[text()='By Identification']/preceding-sibling::input[starts-with(@id,'BatchTemplatePlate_DefaultDesignationSelectControl')]");
	
	
	/** The btn acc num host search. */
	By btnAccNumHostSearch = By.id("BatchTemplatePlate_HostCatalogOrganizationSelectControl_SearchControl_SearchButton");
	
	/** The btn def desig search. */
	By btnDefDesigSearch = By.id("BatchTemplatePlate_DefaultDesignationSelectControl_SearchControl_SearchButton");
	/** The params. */
	
	By lblAccNumHostName = By.xpath("//div[@id='BatchTemplatePlate_HostCatalogOrganizationSelectControl_SearchControl_FilterPanel_IdentificationPanel']//td[text()='Name']");
	
	
	/** The lbl def desig name. */
	By lblDefDesigName = By.xpath("//div[@id='BatchTemplatePlate_DefaultDesignationSelectControl_SearchControl_FilterPanel_IdentificationPanel']//td[text()='Name']");
	
	/** The list acc num host select. */
	By listAccNumHostSelect=By.xpath("//select[@id='BatchTemplatePlate_HostCatalogOrganizationSelectControl_DesignationItemPagedSelectionList_ItemsSelect']/option[1]");
	
	/** The list def desig select. */
	By listDefDesigSelect=By.xpath("//select[@id='BatchTemplatePlate_DefaultDesignationSelectControl_DesignationItemPagedSelectionList_ItemsSelect']/option[1]");
	
	/** The btn acc num host select. */
	By btnAccNumHostSelect = By.id("BatchTemplatePlate_HostCatalogOrganizationSelectControl_SelectButton");
	
	/** The btn def desig select. */
	By btnDefDesigSelect = By.id("BatchTemplatePlate_DefaultDesignationSelectControl_SelectButton");
	
	
	/** The list agency resolution select. */
	By listAgencyResolutionSelect=By.xpath("//select[@id='BatchTemplatePlate_SearchSourcePagedPickList_DomainItemsSelect']/option[1]");
	
	/** The imgadd item. */
	By imgaddItem=By.xpath("//img[@alt='add item']");
	
	/** The btn save. */
	By btnSave=By.id("SaveButton");
	
	/** The filter box. */
	By filterBox=By.cssSelector("input[id$='FilterTextBox']");
		
	By buttonDelete=By.cssSelector("input[id$='DeleteButton']");
	
	/** The lbl no element. */
	By lblNoTemplateElement = By.xpath("//td[contains(text(),'none')]");
	//Locator buttonSave=new Locator(By.id("SaveButton"), "Save Button");
	
	/** The params. */
	Properties params = new Properties();
    
	/** The batch template name. */
	public String batchTemplateName=null;
	/**
	 * Instantiates a new c m_ add_ delete_ batch.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM__ADD_Delete_Batch_Template(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#fill()
	 */
	@Override
	public void fill() {
		try {
					
		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#move()
	 */
	@Override
	public void move() {

		try {
			
		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	
	/**
	 * Creates the batch template.
	 *
	 * @param batchGroupName the batch group name
	 * @return the string
	 */
	public String createBatchTemplate() {
		try {
			waitForVisibilityOfElement(expand_Batches,"expand_Batches");
			JSClick(expand_Batches, " expand_Batches");
			waitForSeconds(5);
			waitForVisibilityOfElement(lnkTemplates,"Templates");
			JSClick(lnkTemplates, "Templates");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			waitForVisibilityOfElement(lnkAddNewBatchTemplate," Add New Batch Template");
			JSClick(lnkAddNewBatchTemplate, " Add New Batch Template");
			waitForSeconds(5);
			waitForVisibilityOfElement(lblTemaplateName," Template Name",60);
			if(isElementPresent(lblTemaplateName, "Template Name ", true))
				this.reporter.successwithscreenshot("Verify Batch Template Page", "User is able to navigate to Batch Template Page", this.Driver);
			else
				this.reporter.failureReport("Verify Batch Template Page", "Failed to navigate Batch Template Page ", this.Driver);
			waitForSeconds(5);
			batchTemplateName=strwithtimestamp(this.params.getProperty("TempalteName"));
			type(txtboxTemplateName,batchTemplateName,"Template Name");
			waitForSeconds(5);
			selectByVisibleText(dropdownOptionPackage, this.params.getProperty("OptionPackage"), "Option Package");
			waitForSeconds(5);
			selectByVisibleText(dropdownDonorGroup, this.params.getProperty("DonorGroup"), "Donor Group");
			waitForSeconds(5);
			//selectByIndex(dropdownDonorGroup, 1, "Donor Group");
			selectByVisibleText(dropdownBatchGroup, this.params.getProperty("batchGroup"), "Batch Group");
			waitForSeconds(5);
			type(txtboxBatchTemplateCode,getUniqueID(),"Batch Code");
			waitForSeconds(5);
			if(!this.Driver.findElement(chkboxAccNumHostIdentification).isSelected())
				click(chkboxAccNumHostIdentification,"Accunt Num Host Identification Checkbox");
			waitForVisibilityOfElement(lblAccNumHostName, "Acc Host Name", 120);
			ScrollToElementVisible(btnAccNumHostSearch);
			waitForSeconds(2);
			JSClick(btnAccNumHostSearch, "Search");
			waitForSeconds(5);
			click(listAccNumHostSelect,"Account Host Name");
			waitForSeconds(5);
			JSClick(btnAccNumHostSelect, "Selct Account Num Host");
			waitForSeconds(5);
			ScrollToElementVisible(chkboxDefDesigIdentification);
			waitForSeconds(5);
			if(!this.Driver.findElement(chkboxDefDesigIdentification).isSelected())
				click(chkboxDefDesigIdentification,"Default Designation Identification Checkbox");
			waitForSeconds(5);
			waitForVisibilityOfElement(lblDefDesigName, "Default Designation  Name", 120);
			ScrollToElementVisible(btnDefDesigSearch);
			waitForSeconds(5);
			JSClick(btnDefDesigSearch, "Search");
			waitForSeconds(2);
			click(listDefDesigSelect,"Default Designation");
			waitForSeconds(2);
			JSClick(btnDefDesigSelect, "Selct Default Designation");
			waitForSeconds(5);
			click(listAgencyResolutionSelect,"Select Agency Resolution");
			waitForSeconds(2);
			JSClick(imgaddItem," Add Agency Resolution");
			waitForSeconds(5);
			ScrollToElementVisible(btnSave);
			waitForSeconds(2);
			JSClick(btnSave,"Save Button");
			waitForSeconds(5);
			
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return batchTemplateName;

	}
	
	/**
	 * Verify batch template.
	 *
	 * @param templateName the template name
	 */
	public void verifyBatchTemplate(String templateName) {
		try {
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(lnkTemplates,"Templates");
			JSClick(lnkTemplates, " Templates");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			By verify_BatchTemplate=By.xpath("//td[text()='" +templateName+"']");
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,templateName);
			if(isElementPresent(verify_BatchTemplate, "Batch Template", true))
				this.reporter.successwithscreenshot("Verify Batch Template in Grid", "Batch Template " +templateName+ " created successfully", this.Driver);
			else
				this.reporter.failureReport("Verify Batch Template in Grid", "Batch Template not created ", this.Driver);
		}catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	/**
	 * Delete batch template.
	 *
	 * @param templateName the template name
	 */
	public void deleteBatchTemplate(String templateName) {
		try {
			waitForElementToBoClickable(buttonDelete, " Delete Batch Template", 120);
			if(!isSafari()&&!isEdge()&&!isChrome()){
				click(buttonDelete, " Delete Batch Template");
				Thread.sleep(3000);
				AlertAccept();

			}
			else{
				JSAcceptAlert();
				Thread.sleep(3000);
				JSClick(buttonDelete, " Delete Batch Template");
			}
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,templateName);
			waitForElementToBoClickable(lblNoTemplateElement, "No Batch Template Element in Grid", 120);
			if(isElementPresent(lblNoTemplateElement, "No Batch Template Element in Grid", true))
				this.reporter.successwithscreenshot(" Batch Template  Deletion", "Batch Template  deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Batch Template  Deletion", "Batch Template  not deleted ", this.Driver);
		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
	
}
