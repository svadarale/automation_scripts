package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

//import util.WebUtils;







import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

/**
 * The Class CM_ADMIN_VERIFY_SPE_ESTABLISH_SETTINGS.
 */
public class CM_ADMIN_VERIFY_ESTABLISH_SETTINGS extends ActionEngine implements Page {
			
		/** The campagins_expand. */
		By campagins_expand = By.xpath(".//*[@alt='Expand Campaigns']");
		
		/** The campagins_ a_expand. */
		By campagins_A_expand = By.xpath(".//*[@alt='Expand A']"); 
		
		/** The acme_expand. */
		By acme_expand = By.xpath(".//*[@alt='Expand Acme New 2 [AcmeNew2]']"); 
		
		/** The settings_expand. */
		By settings_expand = By.xpath("//a[contains(.,'Settings')]");
		
		/** The align_donor. */
		By align_donor = By.xpath("//select[@id='CampaignPlate_DonorAssociationTypeDropDownList']");
		
		/** The opt_pkg. */
		By opt_pkg = By.xpath("//select[@id='CampaignPlate_OptionPackageIdDropDownList']");
		
		/** The currency. */
		By currency = By.xpath("//select[@id='CampaignPlate_CurrencyDropDownList']");
		
		/** The default_donorgrps. */
		By default_donorgrps	= By.xpath(".//*[@id='CampaignPlate_DonorGroupEditButton' or alt='change']");
		
		/** The donorgrps_option. */
		By donorgrps_option = By.xpath("//select[@id='CampaignPlate_DonorGroupSelectionList_DonorGroupPagedSelectionList_ItemsSelect']");
		
		/** The select_button. */
		By select_button = By.xpath("//input[@id='CampaignPlate_DonorGroupSelectionList_SelectButton']");
		
		/** The pledge_status. */
		By pledge_status = By.xpath("//select[@id='CampaignPlate_CampaignPledgeStatusDropDownList']");
		
		/** The donor_ct_limit. */
		By donor_ct_limit = By.xpath("//input[@id='CampaignPlate_DonorDonationCountLimitTextBox']");
		
		/** The Donor_cancel. */
		By Donor_cancel =By.xpath(".//*[@id='CampaignPlate_DonorGroupClearButton']");
		
		/** The donor_pay_ct_limit. */
		By donor_pay_ct_limit = By.xpath("//input[@id='CampaignPlate_DonorPayrollDonationCountLimitTextBox']");
		
		/** The distribution_edt. */
		By distribution_edt = By.xpath(".//*[@id='CampaignPlate_DistributingOrganizationEditButton']");
		
		/** The distribution_option. */
		By distribution_option = By.xpath("//select[@id='CampaignPlate_DistributingOrganizationSelectionList_OrganizationPagedSelectionList_ItemsSelect']");
		
		/** The distribution_selt_btn. */
		By distribution_selt_btn = By.xpath("//input[@id='CampaignPlate_DistributingOrganizationSelectionList_SelectButton']");
		
		/** The conf_password. */
		By conf_password = By.xpath("//input[@id='CampaignPlate_ConfirmPasswordTextBox']");
		
		/** The pledge_url. */
		By pledge_url = By.xpath("//select[@id='CampaignPlate_PrimaryPledgeUrlKeyDropDownList']");
		
		/** The save_btn. */
		By save_btn = By.xpath("//a[@id='SaveButton']");
		By Merchant_lbl = By.xpath(".//span[text()='Merchant']");
		
	/** The params. */
	Properties params = new Properties();
		
		/**
		 * Instantiates a new cm admin verify spe establish settings.
		 *
		 * @param driver the driver
		 * @param reporter the reporter
		 * @param params the params
		 */
		public CM_ADMIN_VERIFY_ESTABLISH_SETTINGS(WebDriver driver,CReporter reporter, Properties params)
		{
			setdriver(driver,reporter);
			this.params = params;
		}
		
		@Override
		public void fill() {
				try {
					By Node1_Expand = By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node1")+"']");
					By Node2_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node2")+"']");
					By Node3_Expand =By.xpath(".//*[@alt='Expand "+(String)params.get("campaign.node3")+"']");
					/** Clicking on Campaign Node  . */               
					Thread.sleep(5000);
					SwitchToFrame(By.name("tree"),"Tree Frame");
					Thread.sleep(5000);
					click(Node1_Expand, "Expand "+(String)params.get("campaign.node1")+" Node under campaign");
					Thread.sleep(6000);
					click(Node2_Expand, "Expanding "+(String)params.get("campaign.node2")+" Node under campaign");
					Thread.sleep(6000);
					click(Node3_Expand, "Expanding "+(String)params.get("campaign.node3")+" Node under campaign");
					Thread.sleep(6000);
			} catch (Throwable e) {
			  e.printStackTrace();
			}		
		}
		
		@Override
		public void move() {
				try {
				Thread.sleep(5000);
				click(settings_expand, "Clicking on Settings");
				Thread.sleep(5000);
				SwitchToFrame(By.name("content"),"Main Frame");
				Thread.sleep(5000);
				selectByIndex(align_donor,0,"align_donor");
				tab(align_donor, "tab from drop down");
				Thread.sleep(5000);
				selectByIndex(opt_pkg,1,"opt_pkg");
				tab(opt_pkg, "tab from drop down");
				Thread.sleep(2000);
				selectBySendkeys(currency, (String)params.get("currency"), "currency");
				tab(currency, "tab from drop down");
				Thread.sleep(5000);
				click(default_donorgrps, "Default donor group icon");
				Thread.sleep(5000);
				selectBySendkeys(donorgrps_option,(String)params.get("dnr_grp_opt_val"), "donorgrps_option");
				tab(donorgrps_option, "tab from drop down");
				Thread.sleep(5000);
				click(select_button, "Click select button");
				Thread.sleep(5000);
				selectBySendkeys(pledge_status, (String)params.get("pledge_status"), "pledge_status");
				tab(pledge_status, "tab from drop down");
				Thread.sleep(5000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					js.executeScript("window.scrollBy(0,50)", "");
			     }*/

				type(donor_ct_limit, (String)params.get("dnrr_donat_cnt_lmt"), "donor_ct_limit");
				Thread.sleep(5000);
				type(donor_pay_ct_limit, (String)params.get("dnr_pay_donat_cnt_lmt"), "donor_pay_ct_limit");
				Thread.sleep(5000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					js.executeScript("window.scrollBy(0,100)", "");
			     }*/
				click(distribution_edt, "Click on distribution_edt button");
				Thread.sleep(5000);
				selectBySendkeys(distribution_option, (String)params.get("orgnizations"), "distribution_option");
				tab(distribution_option, "tab from drop down");
				Thread.sleep(5000);
				click(distribution_selt_btn, "Click distribution_selt_btn");
				Thread.sleep(5000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					js.executeScript("window.scrollBy(0,100)", "");
			     }*/

				type(conf_password, (String)params.get("opps.password"), "Confirm Password");
				Thread.sleep(5000);
				selectByIndex(pledge_url,1, "pledge_url");
				tab(pledge_url, "tab from drop down");
				//Thread.sleep(5000);
				/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
			     {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					js.executeScript("window.scrollBy(0,1000)", "");
			 }*/

				click(save_btn, "Click on save button");
				Thread.sleep(5000);
				boolean validationPoint = isElementPresent(Merchant_lbl, "website list is present", true);
				if(validationPoint){
					this.reporter.successwithscreenshot("Verify request details for the Merchant", "Merchant details are requested",this.Driver);
				}else{
					this.reporter.failureReport("Verify request details for the Merchant", "Merchant details are requested", this.Driver);
				}
			} catch (Throwable e) {
			  e.printStackTrace();
			}
		}
	}