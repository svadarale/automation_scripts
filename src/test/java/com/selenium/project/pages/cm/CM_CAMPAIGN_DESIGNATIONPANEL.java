package com.selenium.project.pages.cm;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;


public class CM_CAMPAIGN_DESIGNATIONPANEL extends ActionEngine implements Page {
	
	public static String strcampaigncode;
	
	/** The Campaigns. */
	By Campaigns = By.xpath(".//*[@id='NavigationTreeViewt3']");

	/** Add Campaign. */
	By AddCampaign = By.xpath(".//*[@id='AddButton']");
	
	/** Organisation. */
	By Organisation = By.cssSelector("#CampaignPlate_OrganizationIdDropDownList");
	
	
	/** Company. */
	By Company = By.xpath(".//*[@id='CampaignPlate_CompanyIdDropDownList']");
	
	/** Campaign Name. */
	By CampaignName = By.xpath(".//*[@id='CampaignPlate_NameTextBox']");
	
	/** Type. */
	By Type = By.xpath(".//*[@id='CampaignPlate_CampaignTypeDropDownList']");
		
	/** Campaign Code. */
	By CampaignCode = By.xpath(".//*[@id='CampaignPlate_CodeTextBox']");
	
	/** Campaign Year. */
	By CampaignYear = By.xpath(".//*[@id='CampaignPlate_CampaignYearDropDownList']");
	
	/** Save / Update. */
	By SaveUpdate = By.xpath(".//*[@id='SaveButton']");
	
	/** Designation panel. */
	By Designationpanel = By.xpath(".//*[@id='CampaignTabStrip_ctl08_ctl00']");
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM CAMPAIGN DESIGNATION PANEL.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_DESIGNATIONPANEL(WebDriver driver, CReporter reporter,Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		try {
			Thread.sleep(2000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			click(Campaigns, "Campaign");
			Thread.sleep(7000);
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(8000);
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(AddCampaign));

			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToTop();
		     }*/
			click(AddCampaign, "Add Campaign");
			Thread.sleep(5000);
			WebDriverWait wait1 = new WebDriverWait(this.Driver, 300);
			wait1.until(ExpectedConditions.presenceOfElementLocated(Organisation));
			selectBySendkeys(Organisation, (String) params.get("action.Organization"), "Organization DropDown");
			Thread.sleep(6000);
			tab(Organisation, "tab from dropdown");
			Thread.sleep(5000);
			selectByIndex(Company, 1, "selecting first value from drop down");
			Thread.sleep(6000);
			//selectBySendkeys(Company, (String) params.get("action.company"), "Comapny DropDown");
			tab(Company, "tab from dropdown");
			Thread.sleep(5000);
			String strcampaignname = strwithtimestamp((String) params.get("action.campaignname"));
			type(CampaignName, strcampaignname, "Campaign Name");
			Thread.sleep(3000);
			strcampaigncode = strwithtimestamp((String) params.get("action.campaigncode"));
			type(CampaignCode, strcampaigncode, "Campaign code");
			//Changed index from 7 to 1 for stage environment execution
			selectByIndex(CampaignYear, 1, "Campaign Year Type DropDown");
			tab(CampaignYear, "tab from dropdown");
			Thread.sleep(3000);
			click(SaveUpdate, "Save / Update");
			Thread.sleep(3000);
		} catch (Throwable e) {	
			e.printStackTrace();
		}
	}

	@Override
	public void move() {
		try {
			/** Verify the Designation panel  */
			SwitchToFrame(By.name("content"), "Main Frame");
			click(Designationpanel, "Designationpanel");
			boolean validationPoint = isElementPresent(Designationpanel, "Designation panel", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("User is able to verify Designation panel", "User is able to verify Designation panel",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify Designation panel", "User is not able to verify Designation panel", this.Driver);
			}
		} catch (Throwable e) {	
			e.printStackTrace();
		}
	}
}