package com.selenium.project.pages.cm;
import java.util.List;
import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_DESIGANTION_FILLED extends ActionEngine implements
		Page {

	public static String strcampaigncode;
	
	/** The Campaigns. */
	By Campaigns = By.xpath(".//*[@id='NavigationTreeViewt3']");

	/** Add Campaign. */
	By AddCampaign = By.xpath(".//*[@id='AddButton']");

	/** Organisation. */
	By Organisation = By
			.xpath(".//*[@id='CampaignPlate_OrganizationIdDropDownList']");

	/** Company. */
	By Company = By.xpath(".//*[@id='CampaignPlate_CompanyIdDropDownList']");

	/** Campaign Name. */
	By CampaignName = By.xpath(".//*[@id='CampaignPlate_NameTextBox']");

	/** Type. */
	By Type = By.xpath(".//*[@id='CampaignPlate_CampaignTypeDropDownList']");

	/** Campaign Code. */
	By CampaignCode = By.xpath(".//*[@id='CampaignPlate_CodeTextBox']");

	/** Campaign Year. */
	By CampaignYear = By
			.xpath(".//*[@id='CampaignPlate_CampaignYearDropDownList']");

	/** Save / Update. */
	By SaveUpdate = By.xpath(".//*[@id='SaveButton']");

	/** expand Campaigns. */
	By expand_Campaigns = By.xpath(".//*[@id='NavigationTreeViewn3']/img");

	/** expand Campaigns T. */
	By expand_CampaignsC = By.xpath(".//*[@id='NavigationTreeViewn15']/img");

	/** Campaign nodes. */
	By Campaignnodes = By.xpath(".//*[@id='NavigationTreeViewt36']");

	/** Designation panel. */
	By Designationpanel = By
			.xpath(".//*[@id='CampaignTabStrip_ctl08_ctl00']");

	/** Campaign list. */
	By Campaignlist = By.xpath("//table[@cellspacing='0']");

	/** Introductory panel. */
	By Introductory_panel = By
			.xpath(".//*[@id='CampaignPlate_IntroductoryDesignationPanelIdDropDownList']");

	/** Primary Panel. */
	By Primary_Panel = By
			.xpath(".//*[@id='CampaignPlate_PrimaryDesignationPanelIdDropDownList']");

	/** Locate An Agency Panel. */
	By Locate_An_Agency_Panel = By
			.xpath(".//*[@id='CampaignPlate_LocateAnAgencyDesignationPanelIdDropDownList']");

	/** Agency Inclusions. */
	By Agency_Inclusions = By
			.xpath(".//*[@id='CampaignPlate_DesignationItemIncludePagedPickList_DomainItemsSelect']/option[1]");

	/** Agency Exclusions. */
	By Agency_Exclusions = By
			.xpath(".//*[@id='CampaignPlate_DesignationItemExcludePagedPickList_DomainItemsSelect']/option[1]");

	/** Agency Right arrow. */
	//By Agency_RightarrowINC = By.xpath(".//*[@id='CampaignPlate_DesignationItemIncludePagedPickList']/table/tbody/tr[2]/td[3]/img[1]");
	By Agency_RightarrowINC = By.xpath(".//*[@id='CampaignPlate_DesignationItemIncludePagedPickList']/table/tbody/tr[2]/td[2]/img[1]");
	
	
	/** Agency Right arrow. */
	//By Agency_RightarrowEXC = By.xpath(".//*[@id='CampaignPlate_DesignationItemExcludePagedPickList']/table/tbody/tr[2]/td[3]/img[1]");
	By Agency_RightarrowEXC = By.xpath(".//*[@id='CampaignPlate_DesignationItemExcludePagedPickList']/table/tbody/tr[2]/td[2]/img[1]");
	
	
	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new CM CAMPAIGN DESIGNATION PANEL.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 * @param params
	 *            the params
	 */

	public CM_CAMPAIGN_DESIGANTION_FILLED(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	@Override
	public void fill() {
		
		try {
			Thread.sleep(6000);
			SwitchToFrame(By.name("tree"), "Top NAV Frame");
			WebDriverWait wait = new WebDriverWait(this.Driver, 300);
			wait.until(ExpectedConditions.presenceOfElementLocated(Campaigns));

			click(Campaigns, "Campaign");
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(6000);
			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToTop();
		     }*/
			WebDriverWait wait1 = new WebDriverWait(this.Driver, 300);
			wait1.until(ExpectedConditions.presenceOfElementLocated(AddCampaign));
			click(AddCampaign, "Add Campaign");
			Thread.sleep(2000);
			WebDriverWait wait2 = new WebDriverWait(this.Driver, 300);
			wait2.until(ExpectedConditions.presenceOfElementLocated(Organisation));
			selectBySendkeys(Organisation, (String) params.get("action.Organization"), "Organization DropDown");
			Thread.sleep(6000);
			tab(Organisation, "press tab");
			Thread.sleep(5000);
			selectByIndex(Company, 1, "selecting first value from drop down");
			Thread.sleep(6000);
			//selectBySendkeys(Company, (String) params.get("action.company"), "Comapny DropDown");
			tab(Company, "press tab");
			Thread.sleep(5000);
			String strcampaignname = strwithtimestamp((String) params.get("action.campaignname"));
			type(CampaignName, strcampaignname, "Campaign Name");
			Thread.sleep(3000);
			strcampaigncode = strwithtimestamp((String) params.get("action.campaigncode"));
			type(CampaignCode, strcampaigncode, "Campaign code");
			//Changed from 7 to 1 for stage environment execution
			selectByIndex(CampaignYear, 1, "Campaign Year Type DropDown");
			Thread.sleep(5000);
			tab(CampaignYear, "press tab");
			Thread.sleep(3000);
			WebDriverWait wait3 = new WebDriverWait(this.Driver, 300);
			wait3.until(ExpectedConditions.presenceOfElementLocated(SaveUpdate));
			click(SaveUpdate, "Save / Update");
			Thread.sleep(4000);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void move() {
		try {
			/** Verify the Designation panel */
			SwitchToFrame(By.name("content"), "Main Frame");
			Thread.sleep(4000);
			click(Designationpanel, "Designationpanel");
			Thread.sleep(4000);
			
			SwitchToFrame(By.name("content"), "Main Frame");

			//Changed to index for stage environment execution
			Thread.sleep(8000);
			selectByIndex(Introductory_panel, 1, "Introductory panel");
			Thread.sleep(7000);
			//String str_Introductorypanel =(String) params.get("action.IntroductoryPanel");
			//selectBySendkeys(Introductory_panel, str_Introductorypanel, "Introductory panel");
			tab(Introductory_panel, "press tab");
			Thread.sleep(5000);
			//String str_Primarypanel =(String) params.get("action.PrimaryName");
			//Thread.sleep(5000);
			selectByIndex(Primary_Panel, 1, "Selecting first value in drop down");
			Thread.sleep(5000);
			//selectBySendkeys(Primary_Panel, str_Primarypanel, "Primary Panel");
			tab(Primary_Panel, "press tab");
			Thread.sleep(5000);
			//String str_LocateAgencypanel =(String) params.get("action.Locateanagency");
			//selectBySendkeys(Locate_An_Agency_Panel, str_LocateAgencypanel,"Locate An Agency Panel");
			
			//Changed to index for stage environment execution
			
			selectByIndex(Locate_An_Agency_Panel, 1, "Locate An Agency Panel");
			Thread.sleep(5000);
			
			tab(Locate_An_Agency_Panel, "press tab");
			Thread.sleep(3000);
			click(Agency_Inclusions, "A Pest");
			Thread.sleep(2000);
			click(Agency_RightarrowINC, "Right Arrow");
			Thread.sleep(2000);
			click(Agency_Exclusions, "A Pest");
			Thread.sleep(2000);
			click(Agency_RightarrowEXC, "Right Arrow");
			Thread.sleep(2000);
			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToBottom();
		    Thread.sleep(1000);
		     }*/
			click(SaveUpdate, "Save");
			Thread.sleep(2000);
			click(Designationpanel, "Designation panel");
			Thread.sleep(2000);
			boolean validationPoint = isElementPresent(SaveUpdate, "website list is present", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("Verify configuration saved for the Designation Panel", "Configuration saved for the Designation Panel",this.Driver);
			}else{
				this.reporter.failureReport("Verify configuration saved for the Designation Panel", "Configuration not saved for the Designation Panel", this.Driver);
				
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}