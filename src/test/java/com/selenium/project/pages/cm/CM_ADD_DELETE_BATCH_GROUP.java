package com.selenium.project.pages.cm;

import java.util.Properties;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

// TODO: Auto-generated Javadoc
/**
 * The Class CM_Add_NEW_Batch_Group.
 */
public class CM_ADD_DELETE_BATCH_GROUP extends ActionEngine implements Page {

	/** The expand_ campaign. */
	By expand_Campaign = By.xpath(".//*[@alt='Expand Campaigns']");

	/** The expand_s. */
	By expand_s =By.xpath(".//*[@alt ='Expand S']");

	/** The expand_sprint16_campaignclassic1. */
	By expand_sprint16_campaignclassic1 = By.xpath(".//*[@alt ='Expand Sprint16CampaignClassic1 [sp16camp1]']");

	/** The expand_ batches. */
	By expand_Batches = By.xpath(".//*[@alt ='Expand Batches']");

	/** The lnk batch groups. */
	By lnkBatchGroups = By.xpath("//a[text()='Batch Groups']");

	/** The lnk add new batch group. */
	By lnkAddNewBatchGroup = By.xpath("//a[@id='AddButton']");

	/** The lbl group name. */
	By lblGroupName = By.xpath("//td[text()='Group Name']");

	/** The txtbox group name. */
	By txtboxGroupName = By.id("BatchGroupPlate_GroupNameTextBox");

	/** The txtbox description. */
	By txtboxDescription = By.id("BatchGroupPlate_DescriptionTextBox");

	/** The btn save. */
	By btnSave = By.id("SaveButton");

	/** The filter box. */
	By filterBox=By.cssSelector("input[id$='FilterTextBox']");

	/** The btn delete. */
	By btnDelete=By.cssSelector("input[id$='DeleteButton']");

	/** The lbl no element. */
	By lblNoElement = By.xpath("//td[contains(text(),'none')]");


	/** The batch group name. */
	public String batchGroupName=null;

	/** The params. */
	Properties params = new Properties();

	/**
	 * Instantiates a new c m_ add_ ne w_ batch_ group.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM_ADD_DELETE_BATCH_GROUP(WebDriver driver, CReporter reporter,
			Properties params) {
		setdriver(driver, reporter);
		this.params = params;
	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#fill()
	 */
	@Override
	public void fill() {


	}

	/* (non-Javadoc)
	 * @see com.automation.page.Page#move()
	 */
	@Override
	public void move() {

	}

	/**
	 * Create_ batch group.
	 *
	 * @return the string
	 */
	public String create_BatchGroup()
	{
		try {
			waitForVisibilityOfElement(expand_Batches,"expand_Batches");
			JSClick(expand_Batches, " expand_Batches");
			waitForSeconds(5);
			waitForVisibilityOfElement(lnkBatchGroups,"Batch Groups");
			JSClick(lnkBatchGroups, " Batch Groups");
			waitForSeconds(2);
			SwitchToFrame(By.name("content"), "content frame");
			waitForSeconds(2);
			waitForVisibilityOfElement(lnkAddNewBatchGroup," Add New Batch Group");
			JSClick(lnkAddNewBatchGroup, " Add New Batch Group");
			waitForSeconds(5);
			waitForVisibilityOfElement(lblGroupName," Group Name");
			batchGroupName=strwithtimestamp(this.params.getProperty("BatchGroupname"));
			waitForElementPresent(txtboxGroupName, "Batch Group Name", 20);
			type(txtboxGroupName,batchGroupName,"Batch Group Name");
			waitForSeconds(2);
			type(txtboxDescription,this.params.getProperty("Description"),"Group Description");
			waitForSeconds(2);
			waitForElementToBoClickable(btnSave, " Save Button", 30);
			JSClick(btnSave, " Save Button");
			waitForSeconds(5);
			
		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}
		return batchGroupName;

	}
	
	/**
	 * Verify_ batch group.
	 *
	 * @param batchGroupName the batch group name
	 */
	public void verify_BatchGroup(String batchGroupName)
	{

		try {
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(lnkBatchGroups,"Batch Groups");
			JSClick(lnkBatchGroups, " Batch Groups");
			waitForSeconds(5);
			SwitchToFrame(By.name("content"), "content frame");
			By verify_BatchGroup=By.xpath("//td[text()='" +batchGroupName+"']");
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,batchGroupName);
			waitForSeconds(5);
			if(isElementPresent(verify_BatchGroup, "Batch Group", true))
				this.reporter.successwithscreenshot("Verify Batch Group in Grid", "Batch Group" +batchGroupName+ " created successfully", this.Driver);
			else
				this.reporter.failureReport("Verify Batch Group in Grid", "Batch Group not created ", this.Driver);

		} catch (Throwable e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}
	/**
	 * Navigate_ campaign.
	 */
	public void navigate_Campaign() {
		try {

			Thread.sleep(3000);
			/*if(!this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge")){
				this.Driver.navigate().refresh();
			}*/
			waitForVisibilityOfElement(By.name("tree"),"Tree Frame");
			SwitchToFrame(By.name("tree"), "Tree Frame");
			waitForVisibilityOfElement(expand_Campaign,"Expand_Campaign");
			JSClick(expand_Campaign, "Expand_Campaign");
			waitForVisibilityOfElement(expand_s,"Expand S icon");
			JSClick(expand_s, "Expand S icon");
			waitForVisibilityOfElement(expand_sprint16_campaignclassic1,"Expand Sprint16CampaignClassic1s");
			JSClick(expand_sprint16_campaignclassic1, "Expand Sprint16CampaignClassic1");

		} catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}

	/**
	 * Delete_ batch group.
	 */
	public void delete_BatchGroup(String batchGroupName) {
		try {
			waitForSeconds(2);
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,batchGroupName);
			waitForSeconds(5);
			JSClick(btnDelete, " Delete Button");
			waitForSeconds(5);
			waitForElementToBoClickable(filterBox, " Filter box", 60);
			enterFilterbox(filterBox,batchGroupName);
			waitForSeconds(5);
			waitForElementToBoClickable(lblNoElement, "No Element in Grid", 60);
			if(isElementPresent(lblNoElement, "No Element in Grid", true))
				this.reporter.successwithscreenshot("Batch Group Deletion", "Batch Group  "+batchGroupName+" deleted successfully", this.Driver);
			else
				this.reporter.failureReport("Batch Group Deletion", "Batch Group not deleted ", this.Driver);

		}catch (Throwable e) {

			e.printStackTrace();
			throw new RuntimeException(e);
		}

	}
}
