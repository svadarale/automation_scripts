package com.selenium.project.pages.cm;
import java.util.Properties;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import com.automation.accelerators.ActionEngine;
import com.automation.page.Page;
import com.automation.report.CReporter;

public class CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE extends ActionEngine implements Page {

	/** The expand_camp. */
	By expand_camp = By.linkText("Campaigns");
	
	/** The Campaigns Name Header. */
	By CampaignsNameHeader=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'Name')]");
	
	/** The Campaigns Number Header. */
	By CampaignsNumberHeader=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'Number')]");
	
	/** The Campaigns Code. */
	By CampaignsCode=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'Code')]");
	
	/** The Campaigns Start Date. */
	By CampaignsStartDate=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'Start Date')]");
	
	/** The Campaigns END Date. */
	By CampaignsENDDate=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'End Date')]");
	
	/** The Campaigns Status. */
	By CampaignsStatus=By.xpath(".//tr[@class='EnhancedDataGridHeader']//a[contains(text(),'Status')]");
			
	/** The params. */
	Properties params = new Properties();
	
	/**
	 * Instantiates a CM CAMPAIGN CREATE CAMPAIGN PROFILE.
	 *
	 * @param driver the driver
	 * @param reporter the reporter
	 * @param params the params
	 */
	public CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE (WebDriver driver,CReporter reporter, Properties params)
	{
		setdriver(driver,reporter);
		this.params = params;
	}
	
	@Override
	public void fill() {
		
		try {
			/** verify title on next page */
			SwitchToFrame(By.name("tree"),"Top NAV Frame");
			Thread.sleep(1000);
			click(expand_camp, "Campaign");
			SwitchToFrame(By.name("content"),"Main Frame");
			Thread.sleep(5000);
			Actions a = new Actions(Driver);
			a.moveToElement(Driver.findElement(CampaignsNameHeader)).build().perform();
			Thread.sleep(5000);
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}

	@Override
	public void move() {		
		try {
			/** verify campaign page header names */
			/*if(this.reporter.getBrowserContext().getBrowserName().equalsIgnoreCase("edge"))
		     {
		    ScrollToTop();
		     }*/
			verifyText(CampaignsNameHeader,"Name","Campaign Name Header");
			verifyText(CampaignsNumberHeader,"Number","CampaignsNumber Header ");
			verifyText(CampaignsCode,"Code","CampaignsCode Header");
			verifyText(CampaignsStartDate,"Start Date","CampaignsStartDate Header");
			verifyText(CampaignsENDDate,"END Date","CampaignsENDDate Header");
			verifyText(CampaignsStatus,"Status","CampaignsStatus Header");
			boolean validationPoint = isElementPresent(CampaignsStatus, "CampaignsStatus", true);
			if(validationPoint){
				this.reporter.successwithscreenshot("User is able to verify Campaigns Status", "User is able to verify Campaigns Status",this.Driver);
			}else{
				this.reporter.failureReport("User is not able to verify Campaigns Status", "User is not able to verify Campaigns Status", this.Driver);
			}
		} catch (Throwable e) {
			e.printStackTrace();
		}
	}
}