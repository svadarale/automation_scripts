package Frontstream.scripts;
import java.util.Properties;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_DESIGANTION_FILLED;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

public class CM_023_TC002_IPE_CAMPAIGN_Fill_DESIGNATIONPANEL extends ActionEngine{
	/** The params. */
	Properties params;
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.IPE_FILL_DESIGNATIONPANEL);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	/**
	 * FILL DESIGNATIONPANEL.
	 */
	@Test(groups={"regression"})
	public void CM_023_TC002_IPE_FILL_DESIGNATIONPANEL(){
		
		this.reporter.initTestCaseDescription("FILL DESIGNATIONPANEL");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_DESIGANTION_FILLED(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_DESIGANTION_FILLED(this.Driver,this.reporter,params).move();
		
		
	}

}
