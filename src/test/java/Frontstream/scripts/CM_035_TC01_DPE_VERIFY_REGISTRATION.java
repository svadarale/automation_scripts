package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_ESTABLISH_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_REGISTRATION;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_035_TC01_DPE_VERIFY_REGISTRATION.
 */
public class CM_035_TC01_DPE_VERIFY_REGISTRATION extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_REGISTRATION_035);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * Verify_DPE_Regisration.
	 */
	@Test(groups={"regression"})
	public void CM_035_TC01_Regisration(){
		
		this.reporter.initTestCaseDescription("Verify_DPE_Regisration");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_REGISTRATION(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_REGISTRATION(this.Driver,this.reporter,params).move();	
	}

}
