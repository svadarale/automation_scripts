package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_CREATE_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;
import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_DONOR_SETTING;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 *The Class CM_134_TC01_SPE_CAMPAIGN_CONFIGURE_DONOR_SETTING.
 */
public class CM_134_TC01_SPE_CAMPAIGN_CONFIGURE_DONOR_SETTING extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CAMPAIGN_DONOR_SETTING);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * SPE Verify Donor settings
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_134_TC01_SPE_CAMPAIGN_CONFIGURE_DONOR_SETTING(){
		this.reporter.initTestCaseDescription("SPE Verify Donor settings");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_DONOR(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_DONOR(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_CREATE_DONOR(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_CREATE_DONOR(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_CONFIGURE_DONOR_SETTING(this.Driver,this.reporter,params).move();
	}
}