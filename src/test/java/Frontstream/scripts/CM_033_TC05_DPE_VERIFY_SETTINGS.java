package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * 
 */
public class CM_033_TC05_DPE_VERIFY_SETTINGS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_SETTINGS_033);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * DPE_Verify_Settings.
	 */
	@Test(groups={"regression"})
	public void CM_033_TC05_Verify_Settings(){
		
		this.reporter.initTestCaseDescription("DPE_Verify_Settings");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_SETTINGS(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_SETTINGS(this.Driver,this.reporter,params).move();	
	}

}
