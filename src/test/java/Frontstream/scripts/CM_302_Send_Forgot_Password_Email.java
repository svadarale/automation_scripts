package Frontstream.scripts;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_302_SEND_FORGOT_PASSWORD_EMAIL.
 */
public class CM_302_Send_Forgot_Password_Email extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_302_Send_Forgot_Password_Email.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.SENDFORGOTPASSWORDEMAIL);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}

			
	
	/**
	 * C m_302_ send_ forgot_ password_ email.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_302_send_Forgot_Password_Email() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Send Forgot Password Email ");
			this.Driver.get("http://qa-opcs.unitedeway.org/Login.aspx");
			waitForSeconds(8);
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.sendForgotPasswordEmail();
						
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
