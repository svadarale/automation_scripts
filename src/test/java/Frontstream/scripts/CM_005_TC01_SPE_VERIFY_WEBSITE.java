package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_005_TC01_SPE_VERIFY_WEBSITE
 */
public class CM_005_TC01_SPE_VERIFY_WEBSITE extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_WEBSITE_005);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * SPE Verify Website.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_005_TC01_Verify_Website(){
		this.reporter.initTestCaseDescription("SPE Verify Website");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).move();
	}
}