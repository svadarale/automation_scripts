package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_FILTER_DESIGNATIONOPTION;

public class CM_029_TC003_DPE_CAMPAIGN_FILTER_DESIGNATION extends ActionEngine{
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.FILTER_CREATE_DESIGANTION);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * CM_029_TC003_CM CAMPAIGN FILTER DESIGNATION OPTION.
	 */
	@Test(groups={"regression"})
	public void CM_029_TC003_CM_CAMPAIGN_FILTER_DESIGNATIONOPTION (){
		
		this.reporter.initTestCaseDescription("CAMPAIGN FILTER DESIGNATION OPTION");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_FILTER_DESIGNATIONOPTION(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_FILTER_DESIGNATIONOPTION(this.Driver,this.reporter,params).move();
				
	}

}


