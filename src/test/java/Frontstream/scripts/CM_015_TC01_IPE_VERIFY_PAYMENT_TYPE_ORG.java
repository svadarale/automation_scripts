package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_PAYMENT_TYPE_IN_ORG;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;



import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_015_TC01_IPE_VERIFY_PAYMENT_TYPE_ORG.
 */
public class CM_015_TC01_IPE_VERIFY_PAYMENT_TYPE_ORG extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_PAYMENT_TYPE_ORG_015);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * IPE Verify Payments type org.
	 */
	@Test(groups={"regression"})
	public void CM_015_TC01_IPEPayments_type_org(){
		
		this.reporter.initTestCaseDescription("IPE Verify Payments type org");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_PAYMENT_TYPE_IN_ORG(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_PAYMENT_TYPE_IN_ORG(this.Driver,this.reporter,params).move();	
	}

}
