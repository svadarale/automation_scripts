package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_DESIGNATIONPANEL;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

public class CM_023_TC001_IPE_CAMPAIGN_CONFIGURE_DESIGNATIONPANEL extends ActionEngine{
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.IPE_CONFIGURE_DESIGNATIONPANEL);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * IPE CAMPAIGN DESIGNATIONPANEL.
	 */
	@Test(groups={"regression"})
	public void CM_023_TC001_CAMPAIGN_DESIGNATIONPANEL(){
		this.reporter.initTestCaseDescription("IPE CAMPAIGN DESIGNATIONPANEL");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_DESIGNATIONPANEL(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_DESIGNATIONPANEL(this.Driver,this.reporter,params).move();
		
		
	}
}
