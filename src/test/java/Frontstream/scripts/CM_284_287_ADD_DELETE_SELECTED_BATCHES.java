package Frontstream.scripts;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_Add_Delete_Batch;
import com.selenium.project.pages.cm.CM_ADD_DELETE_BATCH_GROUP;
import com.selenium.project.pages.cm.CM__ADD_Delete_Batch_Template;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_284_287_Add_Delete_Selected_Batches.
 */
public class CM_284_287_ADD_DELETE_SELECTED_BATCHES extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_284_287_ADD_DELETE_SELECTED_BATCHES.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ADD_DELETESELECTED_BATCH);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}


	/**
	 * C m_284_287_ add_ delete_ selected_ batch.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_284_287_Add_Delete_Selected_Batch() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Add and Delete Selected Batch");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			CM__ADD_Delete_Batch_Template batchTemplate=new CM__ADD_Delete_Batch_Template(this.Driver,this.reporter,params);
			String batchTempalteName=batchTemplate .createBatchTemplate();
			batchTemplate.verifyBatchTemplate(batchTempalteName);
			CM_Add_Delete_Batch batch=new CM_Add_Delete_Batch(this.Driver,this.reporter,params);
			String batchCode=batch.create_Batch(batchTempalteName);
			batch.verify_Batch(batchCode);
			batch.delete_SelectedBatch(batchCode);
			
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
