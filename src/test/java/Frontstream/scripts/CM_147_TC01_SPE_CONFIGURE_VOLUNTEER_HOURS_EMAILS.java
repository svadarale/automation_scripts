package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH;
import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE;
import com.selenium.project.pages.cm.CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_147_TC01_SPE_CONFIGURE_VOLUNTEER_HOURS_EMAILS.
 */
public class CM_147_TC01_SPE_CONFIGURE_VOLUNTEER_HOURS_EMAILS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * Verify Campaign Website configuration of Volunteer hour emails
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_147_TC01_SPE_CONFIGURE_VOLUNTEER_HOURS_EMAILS(){
		
		this.reporter.initTestCaseDescription("Verify Campaign Website Content for Volunteer hour emails");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS(this.Driver,this.reporter,params).move();
	}
}
