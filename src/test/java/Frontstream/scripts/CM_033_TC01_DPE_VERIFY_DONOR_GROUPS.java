package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_033_TC01_DPE_VERIFY_DONOR_GROUPS.
 */
public class CM_033_TC01_DPE_VERIFY_DONOR_GROUPS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_DONOR_GRPS_033);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	

	/**
	 * SP e_ verify_ website.
	 */
	@Test(groups={"regression"})
	public void CM_033_TC01_Verify_Donor_link(){
		
		this.reporter.initTestCaseDescription("Verify DPE Donor Group link");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).move();
	}

}
