package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm. CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

public class CM_004_TC001_SPE_CAMPAIGN_CREATE_A_CAMPAIGN_PROFILE extends ActionEngine{
	
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_NEW_CAMPAIGN);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * CREATE CAMPAIGN PROFILE.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_004_TC001_CREATE_A_CAMPAIGN_PROFILE(){
		this.reporter.initTestCaseDescription("Create a new Campaign");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE(this.Driver,this.reporter,params).move();	
	}
}
