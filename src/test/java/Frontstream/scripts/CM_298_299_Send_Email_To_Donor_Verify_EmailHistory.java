package Frontstream.scripts;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_298_299_SEND_EMAIL_TO_DONOR_VERFIY_EMAILHISTORY.
 */
public class CM_298_299_Send_Email_To_Donor_Verify_EmailHistory extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_298_299_Send_Email_To_Donor_Verify_EmailHistory.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.SENDEMAILVERIFYHISTORY);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}

		
	
	
	/**
	 * C m_298_298_ send email_ verify email_ history.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_298_298_SendEmail_VerifyEmail_History() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Send Email To Donors Verify Email History");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			sendEmail.navigateToOPCS();
			String mailSubject=sendEmail.sendEmail();
			sendEmail.viewEmailHistory(mailSubject);
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
