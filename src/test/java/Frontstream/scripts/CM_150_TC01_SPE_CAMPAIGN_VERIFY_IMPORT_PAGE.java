package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;
import com.selenium.project.pages.cm.CM_CAMPAIGN_ADD_IMPORT;
import com.selenium.project.pages.cm.CM_CAMPAIGN_VERIFY_EMAIL;
import com.selenium.project.pages.cm.CM_CAMPAIGN_VERIFY_IMPORT_PAGE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 *The Class CM_150_TC01_SPE_CAMPAIGN_VERIFY_IMPORT_PAGE.
 */
public class CM_150_TC01_SPE_CAMPAIGN_VERIFY_IMPORT_PAGE extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CAMPAIGN_VERIFY_IMPORT_PAGE);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * SPE Verify CAMPAIGN IMPORTING_PAGE
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_150_TC01_SPE_CAMPAIGN_VERIFY_IMPORT_PAGE(){
		this.reporter.initTestCaseDescription("Verify Campaign Importing page");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_VERIFY_IMPORT_PAGE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_VERIFY_IMPORT_PAGE(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_ADD_IMPORT(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_ADD_IMPORT(this.Driver,this.reporter,params).move();
	}
}