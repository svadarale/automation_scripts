package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE;

public class CM_030_TC001_DPE_CAMPAIGN_CREATECAMPAIGN extends ActionEngine{
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_DPE_CAMPAIGN);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * CM_030_TC001_DPE_CREATECAMPAIGN.
	 */
	@Test(groups={"regression"})
	public void CM_030_TC001_DPE_CREATECAMPAIGN(){
		
		this.reporter.initTestCaseDescription("DPE CREATE CAMPAIGN");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_CREATE_CAMPAIGN_PROFILE(this.Driver,this.reporter,params).move();
				
	}
}
