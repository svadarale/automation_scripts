package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_031_TC03_DPE_CREATE_WEBSITE_DISG_DRIVEN.
 */
public class CM_031_TC03_DPE_CREATE_WEBSITE_DISG_DRIVEN extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_WEBSITE_TC03_032);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * DPE Create Website.
	 */
	@Test(groups={"regression"})
	public void CM_031_TC03_Create_Website(){
		
		this.reporter.initTestCaseDescription("DPE Create Website");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).move();
	}

}
