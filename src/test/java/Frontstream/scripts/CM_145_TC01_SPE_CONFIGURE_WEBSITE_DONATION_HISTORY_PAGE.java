package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_018_TC01_IPE_VERIFY_WEBSITE.
 */
public class CM_145_TC01_SPE_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CAMPAIGN_CONFIGURE_WEBSITE);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * Verify Campaign Website Content for Donation History Page.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_145_TC01_SPE_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE(){
		
		this.reporter.initTestCaseDescription("Verify Campaign Website Content for Donation History Page");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE(this.Driver,this.reporter,params).move();
	}
}
