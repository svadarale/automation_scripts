package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 *The Class CM_131_TC02_SPE_CREATE_DONOR.
 */
public class CM_131_TC02_SPE_CREATE_DONOR extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_DONOR_131);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * SPE Create Donor
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_131_TC02_SPE_CREATE_DONOR(){
		this.reporter.initTestCaseDescription("SPE Create Donor");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_DONOR(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_DONOR(this.Driver,this.reporter,params).move();
		
	}
}