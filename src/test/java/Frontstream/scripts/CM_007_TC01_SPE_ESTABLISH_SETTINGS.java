package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_ESTABLISH_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_007_TC01_SPE_ESTABLISH_SETTINGS
 */
public class CM_007_TC01_SPE_ESTABLISH_SETTINGS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_ESTABLISH_SETTINGS_007);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * submit pledge cash.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_007_TC01_Verify_Establish_Settings(){
		this.reporter.initTestCaseDescription("Verify SPE Establish Settings");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_ESTABLISH_SETTINGS(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_ESTABLISH_SETTINGS(this.Driver,this.reporter,params).move();	
	}
}