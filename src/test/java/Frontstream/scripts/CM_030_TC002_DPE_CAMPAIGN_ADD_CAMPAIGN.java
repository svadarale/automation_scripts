package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_ADD_CAMPAIGN;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

public class CM_030_TC002_DPE_CAMPAIGN_ADD_CAMPAIGN extends ActionEngine {

	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ADD_CAMPAIGN);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 *CM_030_TC002_DPE_CAMPAIGN_ADD_CAMPAIGN.
	 */
	@Test(groups={"regression"})
	public void CM_030_TC002_DPE_ADD_CAMPAIGN(){
		this.reporter.initTestCaseDescription(" DPE Add Campaign");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_ADD_CAMPAIGN(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_ADD_CAMPAIGN(this.Driver,this.reporter,params).move();
			
	}
}
