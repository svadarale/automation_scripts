package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_SETAGENCY_SEARCHMODE;

public class CM_029_TC001_DPE_CAMPAIGN_SETAGENCY_SEARCHMODE extends ActionEngine {
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.SETAGENCY_SEARCHMODE);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * CM_029_TC001_CAMPAIGN SETAGENCY SEARCHMODE.
	 */
	@Test(groups={"regression"})
	public void CM_029_TC001_DPE_SETAGENCY_SEARCHMODE (){
		
		this.reporter.initTestCaseDescription("CAMPAIGN SETAGENCY SEARCHMODE");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_SETAGENCY_SEARCHMODE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_SETAGENCY_SEARCHMODE(this.Driver,this.reporter,params).move();
		
		
	}


}
