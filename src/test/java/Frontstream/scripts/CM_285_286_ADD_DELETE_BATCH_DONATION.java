package Frontstream.scripts;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_Add_Delete_Batch;
import com.selenium.project.pages.cm.CM_ADD_DELETE_BATCH_GROUP;
import com.selenium.project.pages.cm.CM__ADD_Delete_Batch_Template;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_285_286_Add_Delete_Batch_Donation.
 */
public class CM_285_286_ADD_DELETE_BATCH_DONATION extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_285_286_ADD_DELETE_BATCH_DONATION.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ADD_DELETE_BATCH_DONATION);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}


	/**
	 * C m_285_286_ add_ delete_ batch_ donation.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_285_286_Add_Delete_Batch_Donation() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Add  and Delete  Batch Donation");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			/*new CM_ADD_DELETE_BATCH_GROUP(this.Driver,this.reporter,params).navigate_Campaign();
			String batchGroupname=new CM_ADD_DELETE_BATCH_GROUP(this.Driver,this.reporter,params).create_BatchGroup();
			new CM_ADD_DELETE_BATCH_GROUP(this.Driver,this.reporter,params).verify_BatchGroup(batchGroupname);
			String batchTempalteName=new CM__ADD_Delete_Batch_Template(this.Driver,this.reporter,params).createBatchTemplate(batchGroupname);
			new CM__ADD_Delete_Batch_Template(this.Driver,this.reporter,params).verifyBatchTemplate(batchTempalteName);
			String batchCode=new CM_Add_Delete_Batch(this.Driver,this.reporter,params).create_Batch(batchTempalteName);
			new CM_Add_Delete_Batch(this.Driver,this.reporter,params).verify_Batch(batchCode);*/
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			CM__ADD_Delete_Batch_Template batchTemplate=new CM__ADD_Delete_Batch_Template(this.Driver,this.reporter,params);
			String batchTempalteName=batchTemplate .createBatchTemplate();
			batchTemplate.verifyBatchTemplate(batchTempalteName);
			CM_Add_Delete_Batch batch=new CM_Add_Delete_Batch(this.Driver,this.reporter,params);
			String batchCode=batch.create_Batch(batchTempalteName);
			batch.verify_Batch(batchCode);
			batch.create_BatchDonation(batchCode);
			batch.verifyBatchDonation(batchCode);
			batch.deleteBatchDonation();

		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
