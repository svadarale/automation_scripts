package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_DONOR_GRPS;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * .The Class CM_006_TC03_SPE_VERIFY_TRAFFIC_ICON
 */
public class CM_006_TC03_SPE_VERIFY_TRAFFIC_ICON extends ActionEngine{
	
	/** The params. */
	Properties params;

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_DONOR_GRPS_006);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * SPE Verify TRAFFIC ICON.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_006_TC03_Verify_TRAFFIC_ICON(){
		this.reporter.initTestCaseDescription("SPE Verify TRAFFIC ICON");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_DONOR_GRPS(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_TRAFFIC_ICON(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_TRAFFIC_ICON(this.Driver,this.reporter,params).move();	
	}
}