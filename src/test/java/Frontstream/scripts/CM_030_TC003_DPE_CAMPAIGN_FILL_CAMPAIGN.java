package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_FILL_CAMPAIGN;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

public class CM_030_TC003_DPE_CAMPAIGN_FILL_CAMPAIGN extends ActionEngine{
	/** The params. */
	Properties params;
	
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.FILL_CAMPAIGN);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 *DPE Fill Campaign.
	 */
	@Test(groups={"regression"})
	public void CM_030_TC003_DPE_Fill_Campaign(){
		this.reporter.initTestCaseDescription("DPE Fill Campaign");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_FILL_CAMPAIGN(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_FILL_CAMPAIGN(this.Driver,this.reporter,params).move();
		
		
	}
	
}
