package Frontstream.scripts;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_301_SEND_CAMPAIGN_WELCOME_EMAIL.
 */
public class CM_301_Send_Campaign_Welcome_Email extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_301_Send_Campaign_Welcome_Email.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.SENDCAMPAIGNWELCOMEEMAIL);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}

			
	/**
	 * C m_301_ send_ campaign_ welcome_ email.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_301_send_Campaign_Welcome_Email() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Send Campaign Welcome Email ");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			sendEmail.createNewEmail();
			
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
