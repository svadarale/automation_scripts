package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_DONOR_GRPS;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_CAMPAIGN_ACTIVE_CAMPAIGN;
public class CM_010_TC001_SPE_CAMPAIGN_ACTIVATE  extends ActionEngine{


	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ACTIVATE_CAMPAIGN);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * Login.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_010_TC001_CAMPAIGN_ACTIVATE(){
		this.reporter.initTestCaseDescription("CAMPAIGN_ACTIVATE");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_ACTIVE_CAMPAIGN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_DONOR_GRPS(this.Driver,this.reporter,params).fill();	
		new CM_CAMPAIGN_ACTIVE_CAMPAIGN(this.Driver,this.reporter,params).move();		
	}


}
