package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH;
import com.selenium.project.pages.cm.CM_CAMPAIGN_CONFIGURE_WEBSITE_DONATION_HISTORY_PAGE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_146_TC01_SPE_CONFIGURE_WEBSITE_DIRECT_MATCH.
 */
public class CM_146_TC01_SPE_CONFIGURE_WEBSITE_DIRECT_MATCH extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CAMPAIGN_WEBSITE_DIRECT_MATCH);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * @author madhuri katta
	 * Verify Campaign Website configuration of direct match
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_146_TC01_SPE_CONFIGURE_WEBSITE_DIRECT_MATCH(){
		
		this.reporter.initTestCaseDescription("Verify Campaign Website Content for direct match");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_CONFIGURE_WEBSITE_DIRECT_MATCH(this.Driver,this.reporter,params).move();
	}
}
