package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_018_TC02_IPE_CREATE_WEBSITE.
 */
public class CM_018_TC02_IPE_CREATE_WEBSITE extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_WEBSITE_018);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * Create IPE Website link.
	 */
	@Test(groups={"regression"})
	public void CM_018_TC02_Create_Website(){
		
		this.reporter.initTestCaseDescription("Create IPE Website link");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).move();
	}
}
