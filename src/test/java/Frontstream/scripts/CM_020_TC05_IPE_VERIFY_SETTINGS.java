package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_SETTINGS;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_TRAFFIC_ICON;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_020_TC05_IPE_VERIFY_SETTINGS.
 */
public class CM_020_TC05_IPE_VERIFY_SETTINGS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.VERIFY_SETTINGS_020);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 * IPE Verify Settings.
	 */
	@Test(groups={"regression"})
	public void CM_020_TC05_Verify_Settings(){
		
		this.reporter.initTestCaseDescription("IPE Verify Settings");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_SETTINGS(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_SETTINGS(this.Driver,this.reporter,params).move();	
	}

}
