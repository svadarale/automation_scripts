package Frontstream.scripts;
import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS;

public class CM_029_TC002_DPE_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS extends ActionEngine {
	/** The params. */
	Properties params;
	

	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ADD_A_NEW_DESIGNATION_OPTIONS);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 *  CAMPAIGN ADD A NEW DESIGNATION OPTIONS.
	 */
	@Test(groups={"regression"})
	public void CM_029_TC002_CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS (){
		
		this.reporter.initTestCaseDescription("ADD A NEW DESIGNATION OPTIONS");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS(this.Driver,this.reporter,params).fill();
		new CM_CAMPAIGN_ADD_A_NEW_DESIGNATION_OPTIONS(this.Driver,this.reporter,params).move();
				
	}

}
