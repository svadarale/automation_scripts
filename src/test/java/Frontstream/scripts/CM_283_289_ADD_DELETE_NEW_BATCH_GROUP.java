package Frontstream.scripts;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADD_DELETE_BATCH_GROUP;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_283_289_Add_Delete_New_Batch_Group.
 */
public class CM_283_289_ADD_DELETE_NEW_BATCH_GROUP extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_283_289_ADD_DELETE_NEW_BATCH_GROUP.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.ADD_DELETE_NEW_BATCH_GROUP);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}



	
	/**
	 * C m_283_ add_ delete_ new_ batch_ group.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_283_289_add_Delete_New_Batch_Group() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Add  and Delete New Batch Group");
			if(!isSafari()){
			this.Driver.get(this.urlOpps);
			}
			else{
				this.Driver.navigate().to(ReporterConstants.ADMINOPPS_QA_URL);
			}
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			CM_ADD_DELETE_BATCH_GROUP batchGroup=new CM_ADD_DELETE_BATCH_GROUP(this.Driver,this.reporter,params);
			String batchGroupname=batchGroup.create_BatchGroup();
			batchGroup.verify_BatchGroup(batchGroupname);
			batchGroup.delete_BatchGroup(batchGroupname);
			
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
