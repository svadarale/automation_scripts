package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.automation.report.ReporterConstants;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_DONOR_GRPS;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_DONOR_GROUP;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;


import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * The Class CM_006_TC02_SPE_CREATE_DONOR_GROUPS.
 */
public class CM_006_TC02_SPE_CREATE_DONOR_GROUPS extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_DONOR_GRPS_006);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	
	/**
	 * SPE Create Donor Group.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_006_TC02_Create_Donor_Group(){	
		this.reporter.initTestCaseDescription("SPE Create Donor Group");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_DONOR_GROUP(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_DONOR_GRPS(this.Driver,this.reporter,params).fill();
	}
}