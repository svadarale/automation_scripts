package Frontstream.scripts;

import java.util.Properties;
import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;
import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_300_SEND_TEST_EMAIL.
 */
public class CM_300_Send_Test_Email extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_300_Send_Test_Email.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.SENDTESTEMAIL);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}

		
	
	
	
	/**
	 * C m_300_ send_ test_ email.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_300_send_Test_Email() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Send Test Email ");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			sendEmail.navigateToOPCS();
			sendEmail.sendTestEmail();
			
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
