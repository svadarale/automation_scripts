package Frontstream.scripts;

import java.util.Properties;

import org.apache.log4j.Logger;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADD_DELETE_BATCH_GROUP;
import com.selenium.project.pages.cm.CM__ADD_Delete_Batch_Template;
import com.selenium.project.pages.cm.CM__SEND_EMAIL;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;


// TODO: Auto-generated Javadoc

/**
 * The Class CM_290_Delete_Batch_Template.
 */
public class CM_290_DELETE_BATCH_TEMPLATE extends ActionEngine{

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(CM_290_DELETE_BATCH_TEMPLATE.class);

	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.DELETE_BATCH_TEMPLATE);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}


	
	/**
	 * C m_290_ delete_ batch_ template.
	 *
	 * @throws Throwable the throwable
	 */
	@Test(groups={"regression"})
	public void CM_290_Delete_Batch_Template() throws Throwable {
		try
		{
			this.reporter.initTestCaseDescription("Add and Delete Batch Template");
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
			new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
			CM__SEND_EMAIL sendEmail=new CM__SEND_EMAIL(this.Driver,this.reporter,params);
			sendEmail.navigateToCampaign();
			CM__ADD_Delete_Batch_Template batchTemplate=new CM__ADD_Delete_Batch_Template(this.Driver,this.reporter,params);
			String batchTempalteName=batchTemplate .createBatchTemplate();
			batchTemplate.verifyBatchTemplate(batchTempalteName);
			batchTemplate.deleteBatchTemplate(batchTempalteName);
		}

		catch(Exception e){
			LOG.info(e.getMessage());

		}
	}

}
