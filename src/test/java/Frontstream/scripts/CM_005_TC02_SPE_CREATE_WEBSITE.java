package Frontstream.scripts;

import java.util.Properties;

import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.automation.accelerators.ActionEngine;
import com.selenium.project.pages.cm.CM_ADMIN_CREATE_WEBSITE;
import com.selenium.project.pages.cm.CM_ADMIN_LOGIN;
import com.selenium.project.pages.cm.CM_ADMIN_VERIFY_WEBSITE;

import Frontstream.enumeration.ActionType;
import Frontstream.framework.GlobalVariables;

/**
 * .The Class CM_005_TC02_SPE_CREATE_WEBSITE
 */
public class CM_005_TC02_SPE_CREATE_WEBSITE extends ActionEngine{
	
	/** The params. */
	Properties params;

	/**
	 * Inits the.
	 */
	@BeforeClass(groups={"initGroup"})
	public void init(){
		this.params = GlobalVariables.getGV().getEventPropMap().get(ActionType.CREATE_WEBSITE_005);
		GlobalVariables.getGV().setCurrentProp(this.params);
	}
	
	/**
	 *SPE Create Website.
	 */
	@Test(groups={"regression", "smoke"})
	public void CM_005_TC02__Create_Website(){	
		this.reporter.initTestCaseDescription("SPE Create Website");
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_LOGIN(this.Driver,this.reporter,params).move();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).fill();
		new CM_ADMIN_VERIFY_WEBSITE(this.Driver,this.reporter,params).move();
		new CM_ADMIN_CREATE_WEBSITE(this.Driver,this.reporter,params).fill();
	}
}