package Frontstream.framework;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Properties;

import Frontstream.enumeration.ActionType;

/**
 * framework global variable singleton class
 * @author Nirmala Gokidi, 2015
 *
 */
public class GlobalVariables {
	List<Object> list=new ArrayList<Object>();
	LinkedHashMap<ActionType, Properties> eventPropMap;
	Properties currentProp = new Properties();
	
	
	public static GlobalVariables globalVar;
	
	public static synchronized GlobalVariables getGV() {
		if (globalVar == null) {
			globalVar = new GlobalVariables();
		}
		return globalVar;

	}
	
	
	public List<Object> getList() {
		return list;
	}

	public void setList(List<Object> list) {
		this.list = list;
	}


	public LinkedHashMap<ActionType, Properties> getEventPropMap() {
		return eventPropMap;
	}


	public void setEventPropMap(LinkedHashMap<ActionType, Properties> eventPropMap) {
		this.eventPropMap = eventPropMap;
	}


	public Properties getCurrentProp() {
		return currentProp;
	}


	public void setCurrentProp(Properties currentProp) {
		this.currentProp = currentProp;
	}
	
	
}
