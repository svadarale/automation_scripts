package Frontstream.enumeration;

import java.util.Properties;

/**
 * Enum to handle Action Type
 *
 */
public enum ActionType {
	VERIFY_WEBSITE_005("Verify_website_005"),
	CREATE_WEBSITE_005("Create_Website_005"),
	VERIFY_WEBSITE_018("Verify_website_018"),
	CREATE_WEBSITE_018("Create_Website_018"),
	VERIFY_WEBSITE_032("Verify_website_032"),
	CREATE_WEBSITE_TC02_032111("Giving_Experience_TC02_03211"),
	CREATE_WEBSITE_TC03_032("Giving_Experience_TC03_032"),
	VERIFY_DONOR_GRPS_006("Verify_Donor_Grps_006"),
	VERIFY_DONOR_GRPS_020("Verify_Donor_Grps_020"),
	VERIFY_DONOR_GRPS_033("Verify_Donor_Grps_033"),
	CREATE_DONOR_GRPS_006("Create_Donor_Grps_006"),
	CREATE_DONOR_GRPS_020("Create_Donor_Grps_020"),
	CREATE_DONOR_GRPS_033("Create_Donor_Grps_033"),
	VERIFY_SETTINGS_006("Verify_Settings_006"),
	VERIFY_SETTINGS_020("Verify_Settings_020"),
	VERIFY_SETTINGS_033("Verify_Settings_033"),
	VERIFY_ESTABLISH_SETTINGS_007("Verify_Establish_Settings_007"),
	VERIFY_ESTABLISH_SETTINGS_021("Verify_Establish_Settings_021"),
	VERIFY_ESTABLISH_SETTINGS_034("Verify_Establish_Settings_034"),
	VERIFY_REGISTRATION_008("Verify_Registration_008"),
	VERIFY_REGISTRATION_022("Verify_Registration_022"),
	SAVE_REGISTRATION_008("Save_Registration_008"),
	SAVE_REGISTRATION_022("Save_Registration_022"),
	CREATE_NEW_CAMPAIGN("Campaign"),
	ADD_CAMPAIGN("Addcampaign"),
	FILL_CAMPAIGN("Fillcampaign"),
	ACTIVATE_CAMPAIGN("ActivateCampaign"),
	DPE_ACTIVATE_CAMPAIGN("DPE_ActivateCampaign"),
	CONFIGURE_DESIGNATIONPANEL("Designationpanel"),
	SETAGENCY_SEARCHMODE("SetAgencysearchmode"),
	ADD_A_NEW_DESIGNATION_OPTIONS("Adddesignnationoptions"),
	FILTER_CREATE_DESIGANTION("Filtercreatedesignation"),
	CREATE_DPE_CAMPAIGN("Dpecampaign"),
	DPE_CAMPAIN_VERIFY_WEBSITE("Dpe_Verify_website"),
	DPE_CAMPAIGN_WEBSUTE("Dpe_Create_Website"),
	IPE_CAMPAIGN_SEARCHAGENCY("IpeSetAgencysearchmode"),
	DPE_CAMPAIGN_DESIGNATIONPANEL("Dpe_designation_panel"),
	IPE_ADD_A_NEW_DESIGNATION_OPTIONS("Ipe_add_new_designation"),
	IPE_FILTER_CREATE_DESIGANTION("Ipe_filter_designation"),
	IPE_ADD_CAMPAIGN("Ipe_Addcampaign"),
	IPE_FILL_CAMPAIGN("Ipe_Fillcampaign"),
	IPE_CREATE_NEW_CAMPAIGN("Ipe_Campaign"),
	IPE_ACTIVATE_CAMPAIGN("Ipe_ActivateCampaign"),
	IPE_CONFIGURE_DESIGNATIONPANEL("IPE_Designationpanel"),
	SPE_FILL_DESIGNATIONPANEL("Spe_Fill_Designationpanel"),
	IPE_FILL_DESIGNATIONPANEL("Ipe_Fill_Designationpanel"),
	DPE_FILL_DESIGNATIONPANEL("Dpe_Fill_Designationpanel"),
	GIVING_EXPEREINCE_TC01_032("Giving_Experience_TC01_032"),
	GIVING_EXPEREINCE_TC02_032("Giving_Experience_TC02_032"),
	VERIFY_REGISTRATION_035("Verify_Registration_035"),
	SAVE_REGISTRATION_035("Save_Registration_035"),
	VERIFY_PAYMENT_TYPE_ORG_015("Verify_Payment_Type_org_015"),
	CREATE_PAYMENT_TYPE_ORG_015("Create_Payment_Type_org_015"),
	VERIFY_DESIGNATION_PANEL_024("Verify_designation_panel_024"),
	CREATE_DESIGNATION_PANEL_ORG_024("Create_designation_panel_org_024"),
	VERIFY_DONOR_131("Verify_Donor_131"),
	CREATE_DONOR_131("Create_Donor_131"),
	VERIFY_CREATED_DONOR_131("Verify_Created_Donor_131"),
	CAMPAIGN_CONFIGURE_WEBSITE("campaign_configure_website_for_Donation_History_Page"),
	CAMPAIGN_CONTENT_DIRECT_MATCH_EMAILS("CM_146_Campaign_Configure_Website_Content_for_Direct_Match_emails"),
	CAMPAIGN_CONFIGURE_GENERAL_DONOR_SETTINGS("Verify_campaign_configure_general_donor_settings"),
	CAMPAIGN_VERIFY_EMAIL("Verify_campaign_email_page"),
	CAMPAIGN_CREATE_EMAIL("Verify_campaign_email_creation"),
	CAMPAIGN_SPECIAL_EVENTS("Verify_campaign_configure_special_events"),
	CAMPAIGN_VERIFY_IMPORT_PAGE("Verify_campaign_importing_page"),
	CAMPAIGN_ADD_IMPORT("Verify_campaign_add_new_import"),
	CAMPAIGN_WEBSITE_DIRECT_MATCH("campaign_configure_website_for_direct_match"),
	CAMPAIGN_WEBSITE_VOLUNTEER_HOURS_EMAILS("campaign_configure_website_for_volunteer_hour_email"),
	CAMPAIGN_DONOR_SETTING("Verify_campaign_configure_donor_settings"),
	ADD_DELETE_NEW_BATCH_GROUP("AddAndDeleteNewBatchGroup"),
	ADD_DELETESELECTED_BATCH("AddDeleteSelectedBatch"),
	DELETE_BATCH("DeleteBatch"),
	DELETE_BATCH_TEMPLATE("DeleteBatchTemplate"),
	ADD_DELETE_BATCH_DONATION("AddDeleteBatchDonation"),
	SENDEMAILVERIFYHISTORY("SendEmailVerifyHistory"),
	SENDTESTEMAIL("SendTestEmail"),
	SENDCAMPAIGNWELCOMEEMAIL("SendCampaignWelcomeEmail"),
	SENDFORGOTPASSWORDEMAIL("SendForgotPasswordEmail");
	
	
	private final String text;
	
	ActionType(String text) {
		this.text = text;
	}
	
	public String toString() {
		return text;
	}
	
	public static ActionType fromString(Properties params, String key) {
		ActionType actionType = null;
		 String text = params.getProperty(key);
		if (text != null) {
	      for (ActionType a : ActionType.values()) {
	        if (text.equals(a.text)) {
	        	actionType = a;
	        }
	      }
	    }
		return actionType;
	}

}
