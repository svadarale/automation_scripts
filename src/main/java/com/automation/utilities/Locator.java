package com.automation.utilities;

import org.openqa.selenium.By;

public class Locator {
	By by;
	String locatorName;
	
	public Locator(By locator, String locatorName){
		this.by = locator;
		this.locatorName = locatorName;
	}
	
	public By getBy(){
		return by;
	}

	public String getLocatorName(){
		return locatorName;
	}

}
