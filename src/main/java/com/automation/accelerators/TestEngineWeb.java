package com.automation.accelerators;

import io.appium.java_client.AppiumDriver;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.concurrent.TimeUnit;
import org.apache.log4j.Logger;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.safari.SafariDriver;
import org.testng.SkipException;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Parameters;
import Frontstream.framework.GlobalVariables;
import com.automation.report.CReporter;
import com.automation.report.ReporterConstants;

/**
 * The Class TestEngineWeb.
 *
 * @author in01518
 */
public class TestEngineWeb {

	public boolean proceedExecution;
	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(TestEngineWeb.class);

	/** The appium driver. */
	protected AppiumDriver appiumDriver = null;

	/** The Driver. */
	protected WebDriver Driver = null;

	/** The reporter. */
	protected CReporter reporter = null;

	/** The browser. */
	/*cloud platform*/
	public String browser = null;

	/** The version. */
	public String version = null;

	/** The platform. */
	public String platform = null;

	/** The environment. */
	public String environment = null;

	/** The cloud base url. */
	public String cloudBaseUrl = null;

	/** The user name. */
	public String userName = null;

	/** The access key. */
	public String accessKey = null;

	/** The cloud implicit wait. */
	public String cloudImplicitWait = null;

	/** The cloud page load time out. */
	public String cloudPageLoadTimeOut = null;

	/** The update jira. */
	public String updateJira = null;

	/** The build number. */
	public String buildNumber = "";

	/** The job name. */
	public String jobName = "";

	/** The executed from. */
	public String executedFrom = null;       

	/** The url rem. */
	public String urlRem = null;

	/** The nodes selected. */
	public String nodes = null;
	/** The nodes selected. */
	public String nodeParam = null;

	/**Test Environment URL **/
    public String urlOpps = null;
    public String urlOpcs = null;
    
    /** 
	 * IMPLICIT_WAIT_TIME (implicit waiting time)
	 * The constant that defines implicit waiting time for the driver throughout its extent. 
	 * Meant to be set at once after instantiating the driver.
	 * Private to this class, which indicates that it shouldn't be altered outside.  
	 */
	private static final long IMPLICIT_WAIT_TIME = 10;
	/** 
	 * EXPLICIT_WAIT_TIME (default explicit waiting time)
	 * The constant that defines the default time while explicitly waiting for an element based on a condition. 
	 * Ideally this time should be more than implicit wait time, otherwise there is no meaning in setting explicit wait time (implicit time is considered). 
	 * Protected, as it should be available for ActionEngine
	 */
	protected static final long EXPLICIT_WAIT_TIME = IMPLICIT_WAIT_TIME + 20; // 30 seconds

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite() throws IOException{
		if(System.getProperty("os.name").toLowerCase().contains("windows")){
			String command="cmd /c start  "+" CALL "+"\""+System.getProperty("user.dir")+"\\Drivers\\MakeDisplayActive.bat"+"\"";
			String command1="cmd /c start  "+" CALL "+"\""+System.getProperty("user.dir")+"\\Drivers\\EnableDisplayContentIESetting.bat"+"\"";
			String command2="cmd /c start  "+" CALL "+"\""+System.getProperty("user.dir")+"\\Drivers\\ReturnSessionToConsole.bat"+"\"";
			Runtime runtime=Runtime.getRuntime();
			@SuppressWarnings("unused")
			Process pr=runtime.exec(command);
			@SuppressWarnings("unused")
			Process pr1=runtime.exec(command1);
			@SuppressWarnings("unused")
			Process pr2=runtime.exec(command2);
		}else{
			System.out.println("****** Batch file to make display active is not execeuted because this is not the windows platform. ******");
		}
	}

	/**
	 * Before test. - Create Driver & Reporter instance
	 *
	 * @param automationName the automation name
	 * @param browser the browser
	 * @param browserVersion the browser version
	 * @param environment the environment
	 * @param platformName the platform name
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	@BeforeClass(dependsOnGroups={"initGroup"} , alwaysRun = true)
	@Parameters({"automationName","browser","browserVersion","environment","platformName","nodeUrl"})
	public void beforeClass(String automationName, String browser, String browserVersion,String environment,String platformName,String nodeUrl) throws IOException, InterruptedException
	{
		/*get configuration */
		this.browser = browser;
		this.version = browserVersion;
		this.platform = platformName;
		this.environment = environment;
		this.userName = ReporterConstants.SAUCELAB_USERNAME;
		this.accessKey = ReporterConstants.SAUCELAB_ACCESSKEY;
		this.executedFrom = System.getenv("COMPUTERNAME");
		this.cloudImplicitWait = ReporterConstants.CLOUD_IMPLICIT_WAIT;
		this.cloudPageLoadTimeOut = ReporterConstants.CLOUD_PAGELOAD_TIMEOUT;        
		this.updateJira = "";

		System.out.println("::::::::urlRem = "+urlRem);

		/**/

		if(environment.equalsIgnoreCase("local"))
		{
			proceedExecution = true;
			this.setWebDriverForLocal(browser);
		}
		if(environment.equalsIgnoreCase("cloudSauceLabs"))
		{

			this.setRemoteWebDriverForCloudSauceLabs();	           
		}
		if(environment.equalsIgnoreCase("grid")){
			this.setWebdriverForGrid(browser,nodeUrl);
		}

		if(environment.equalsIgnoreCase("jenkins"))
		{
			proceedExecution = true;
			this.updateConfigurationFromJenkins();

			if(proceedExecution == false)
			{
				throw new SkipException("Skipped");
			}
		}

		if(environment.equalsIgnoreCase("cloudSauceLabsJenkins"))
		{
			this.updateConfigurationForCloudSauceLabsJenkins();
			/*set remoteWebDriver for cloudsaucelabs*/        

			this.setRemoteWebDriverForCloudSauceLabs();	           
		}

		if (environment.equalsIgnoreCase("cloudBrowserStackJenkins"))
		{
			/*TBD: Not Implemented For Running Using Jenkins*/
			this.updateConfigurationForCloudBrowserStackJenkins();
		}

		urlRem = (String)GlobalVariables.getGV().getCurrentProp().get("url");
		System.out.println(urlRem);

		reporter = CReporter.getCReporter(browser, browserVersion , environment, true);

		if(urlRem.equalsIgnoreCase("DoneropcsURL")){
        	Driver.get(this.urlOpcs);
        }
        
        else if(urlRem.equalsIgnoreCase("AdminoppsURL")){
        	Driver.get(this.urlOpps);
        }

		if(!this.browser.equalsIgnoreCase("safari")){
			Driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);
			Driver.manage().timeouts().implicitlyWait(20, TimeUnit.SECONDS);
		}
		if(!this.browser.equalsIgnoreCase("ie"))
			Driver.manage().deleteAllCookies();
		Driver.manage().window().maximize();
		if(this.reporter.getBrowserContext().getBrowserName().equals("ie")){
			if(this.Driver.getPageSource().trim().contains("There is a problem with this website")){
				this.Driver.navigate().to("javascript:document.getElementById('overridelink').click()");
				Thread.sleep(Long.parseLong("3000"));
			}
		}
		reporter.calculateSuiteStartTime();

	}

	/**
	 * After test. - Close Driver & Reporter instance
	 *
	 * @throws Exception the exception
	 */
	@AfterClass(alwaysRun = true)
	public void afterClass() throws Exception
	{

		if(proceedExecution == false)
		{
			System.out.println("Should Exit");
			throw new SkipException("Skipped");
		}
		if(!this.browser.equalsIgnoreCase("edge")){
			Driver.close();
		}
		Driver.quit();
		//reporter.calculateSuiteExecutionTime(); 
		reporter.calculateSuiteExecutionTime();
		LOG.info("******* Before Creating HTML Summary Report *******");
	    if(urlRem.equalsIgnoreCase("DoneropcsURL")){
        	reporter.createHtmlSummaryReport(this.urlOpcs,true);
        	}
	    
        else if(urlRem.equalsIgnoreCase("AdminoppsURL")){
        	reporter.createHtmlSummaryReport(this.urlOpps,true);
        	}
		//reporter.createHtmlSummaryReport(ReporterConstants.APP_BASE_URL,true);
		reporter.closeSummaryReport();
	}

	@AfterSuite(alwaysRun = true)
	public void afterSuite()
			throws Throwable {
		LOG.info("Before killing "+browser+" browser");
		Runtime.getRuntime().exec("taskkill /F /IM chromedriver_New.exe");
		Runtime.getRuntime().exec("taskkill /F /IM IEDriverServer.exe");
		Runtime.getRuntime().exec("taskkill /F /IM MicrosoftWebDriver.exe");
		LOG.info("After killing "+browser+" browser");
	}
	/**
	 * Before method.
	 *
	 * @param method the method
	 */
	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(Method method)
	{
		if(proceedExecution == false)
		{
			System.out.println("Should Exit");
			throw new SkipException("Skipped");
		}
		//get browser info		
		//reporter = CReporter.getCReporter(deviceName, platformName, platformVersion, true);	
		reporter.initTestCase(this.getClass().getName().substring(0,this.getClass().getName().lastIndexOf(".")), method.getName(), null, false);

	}

	/**
	 * After method.
	 *
	 * @throws IOException Signals that an I/O exception has occurred.
	 */
	@AfterMethod(alwaysRun = true)
	public void afterMethod() throws IOException
	{
		if(proceedExecution == false)
		{
			System.out.println("Should Exit");
			throw new SkipException("Skipped");
		}
		//get browser info				
		reporter.calculateTestCaseExecutionTime();		
		reporter.closeDetailedReport();		
		reporter.updateTestCaseStatus();
	}

	public void setWebdriverForGrid(String browser,String nodeUrl) throws MalformedURLException{
		DesiredCapabilities caps = new DesiredCapabilities();
		if(browser.equalsIgnoreCase("IE")){
			caps = DesiredCapabilities.internetExplorer();
			caps.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
		}
		else if(browser.equalsIgnoreCase("Firefox")){
			caps = DesiredCapabilities.firefox();
			//caps.setBrowserName("firefox");
			//caps.setVersion("33.0");
			caps.setPlatform(Platform.WINDOWS);
		}
		else if(browser.equalsIgnoreCase("chrome")){
			caps = DesiredCapabilities.chrome();
			//caps.setPlatform(Platform.ANY);
		}
		else {
			caps = DesiredCapabilities.safari();
			//caps.setPlatform(Platform.ANY);
		}
		//String Node = "http://172.16.14.172:5555/wd/hub";
		//URL commandExecutorUri = new URL("http://ondemand.saucelabs.com/wd/hub");
		this.Driver = new RemoteWebDriver(new URL(nodeUrl), caps);
	}

	/**
	 * Sets the web driver for local.
	 *
	 * @param browser the new web driver for local
	 * @throws IOException Signals that an I/O exception has occurred.
	 * @throws InterruptedException the interrupted exception
	 */
	private void setWebDriverForLocal(String browser) throws IOException, InterruptedException
	{
		/**get environment details**/
		this.urlOpps = ReporterConstants.ADMINOPPS_QA_URL;
		this.urlOpcs = ReporterConstants.DONEROPCS_QA_URL;
		/*this.urlOpps = ReporterConstants.ADMINOPPS_STAGE_URL;
		this.urlOpcs = ReporterConstants.DONEROPCS_STAGE_URL;
		 */

		switch(browser)
		{
		case "firefox":
		/*	DesiredCapabilities objCapabilities = new DesiredCapabilities();
			objCapabilities = DesiredCapabilities.firefox();
			FirefoxProfile firefoxProfile = new FirefoxProfile(new File(System.getProperty("user.dir") + "/Drivers/BrowserProfiles/Firefox"));
			firefoxProfile.setPreference("geo.enabled", true);
			firefoxProfile.setPreference("extensions.enabledAddons", "georelocate%40netzgewitter.com:0.2.3");
			firefoxProfile.setPreference("extensions.georelocate@netzgewitter.com.install-event-fired", true);
			firefoxProfile.setPreference("browser.cache.disk.enable", false);
			firefoxProfile.setPreference("browser.cache.memory.enable", false);
			firefoxProfile.setPreference("browser.cache.offline.enable", false);
			firefoxProfile.setPreference("network.http.use-cache", false);
			firefoxProfile.setAcceptUntrustedCertificates(true);
			firefoxProfile.setEnableNativeEvents(true);
			firefoxProfile.setPreference("extensions.ui.locale.hidden", true);
			ProfilesIni profile = new ProfilesIni();
			profile.getProfile("default");
			objCapabilities.setCapability(FirefoxDriver.PROFILE, firefoxProfile);
			this.Driver = new FirefoxDriver(objCapabilities);*/
			this.Driver=new FirefoxDriver();
			break;
		case "ie":

			DesiredCapabilities capab = DesiredCapabilities.internetExplorer();		
			File file = new File("Drivers\\IEDriverServer.exe");
			System.setProperty("webdriver.ie.driver",file.getAbsolutePath());
			capab.setPlatform(Platform.WINDOWS);
			capab.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,true);
			capab.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			capab.setJavascriptEnabled(true);
			capab.setCapability("requireWindowFocus", true);
			capab.setCapability("enablePersistentHover", false);
			capab.setCapability("ignoreZoomSetting", true);
			capab.setCapability("nativeEvents",false);
			this.Driver = new InternetExplorerDriver(capab);
			Process p = Runtime
					.getRuntime()
					.exec("RunDll32.exe InetCpl.cpl,ClearMyTracksByProcess 255");
			p.waitFor();
			Thread.sleep(1000);
			break;

		case "chrome":
			System.setProperty("webdriver.chrome.driver",
					"Drivers\\chrome\\chromedriver_New.exe");

			DesiredCapabilities capabilities = DesiredCapabilities.chrome(); 
			ChromeOptions options = new ChromeOptions();
			options.addArguments("test-type");
			options.addArguments("disable-popup-blocking");
			capabilities.setCapability(ChromeOptions.CAPABILITY, options);
			capabilities.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			this.Driver = new ChromeDriver(capabilities);
			break;

		case "edge":
			/*DesiredCapabilities cap = DesiredCapabilities.edge();
			cap.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);*/
			System.setProperty("webdriver.edge.driver", "C:\\Program Files (x86)\\Microsoft Web Driver\\MicrosoftWebDriver.exe");
			this.Driver = new EdgeDriver();
			Thread.sleep(5000);
			break;

		case "safari":
			System.setProperty("webdriver.safari.noinstall", "true");
			DesiredCapabilities cap1 = DesiredCapabilities.safari();
			if(this.platform.equalsIgnoreCase("mac")){
				cap1.setPlatform(Platform.MAC);
			} else {
				cap1.setPlatform(Platform.WINDOWS);
			}
			cap1.setCapability(CapabilityType.ACCEPT_SSL_CERTS, true);
			cap1.setCapability("nativeEvents", true);
			cap1.setCapability("acceptSslCerts", true);
			cap1.setCapability("safariIgnoreFraudWarning", true);
			cap1.setCapability("WarnAboutFraudulentWebsite", false);
			Thread.sleep(5000);
			do{
				try{
					this.Driver = new SafariDriver(cap1);
					break;
				}catch(Exception ex){
					/*Runtime.getRuntime().exec("taskkill /F /IM Safari.exe");
                     Thread.sleep(3000);
                     Runtime.getRuntime().exec("taskkill /F /IM plugin-container.exe");
                     Runtime.getRuntime().exec("taskkill /F /IM WerFault.exe");*/
					LOG.info("CATCH BLOCK - this.Driver ================ " + this.Driver);
					continue;  
				}
			}while(null==this.Driver);

			break;
		}
	}

	/**
	 * Sets the remote web driver for cloud sauce labs.
	 *
	 * @throws MalformedURLException the malformed url exception
	 */
	private void setRemoteWebDriverForCloudSauceLabs() throws MalformedURLException
	{
		DesiredCapabilities desiredCapabilities = new DesiredCapabilities();
		desiredCapabilities.setCapability(CapabilityType.BROWSER_NAME, this.browser);
		desiredCapabilities.setCapability(CapabilityType.VERSION, this.version);
		desiredCapabilities.setCapability(CapabilityType.PLATFORM, this.platform);
		desiredCapabilities.setCapability("username", this.userName);
		desiredCapabilities.setCapability("accessKey", this.accessKey);
		desiredCapabilities.setCapability("name", this.executedFrom + " - " + this.jobName + " - " + this.buildNumber + " - ");
		URL commandExecutorUri = new URL("http://ondemand.saucelabs.com/wd/hub");
		this.Driver = new RemoteWebDriver(commandExecutorUri, desiredCapabilities);
	}

	/**
	 * Update configuration for cloud sauce labs jenkins.
	 */
	private void updateConfigurationForCloudSauceLabsJenkins()
	{
		this.browser = System.getenv("SELENIUM_BROWSER");
		this.version = System.getenv("SELENIUM_VERSION");
		this.platform = System.getenv("SELENIUM_PLATFORM");
		this.userName = System.getenv("SAUCE_USER_NAME");
		this.accessKey = System.getenv("SAUCE_API_KEY");
		this.buildNumber = System.getenv("BUILD_NUMBER");
		this.jobName = System.getenv("JOB_NAME");

		/*For Debug Purpose*/
		LOG.info("Debug: browser = " + this.browser);
		LOG.info("Debug: version = " + this.version);
		LOG.info("Debug: platform = " + this.platform);
		LOG.info("Debug: userName = " + this.userName);
		LOG.info("Debug: accessKey = " + this.accessKey);
		LOG.info("Debug: executedFrom = " + this.executedFrom);
		LOG.info("Debug: BUILD_NUMBER = " + this.buildNumber);
		LOG.info("Debug: jobName = " + this.jobName);
	}

	/**
	 * Update configuration for cloud browser stack jenkins.
	 */
	/*TBD: Not Implemented For Running Using Jenkins*/
	private void updateConfigurationForCloudBrowserStackJenkins()
	{

	}

	private void updateConfigurationFromJenkins() throws IOException, InterruptedException 
	{
		/**get environment details**/

		if (System.getenv("Environment").equalsIgnoreCase("qa")){
    		this.urlOpps = ReporterConstants.ADMINOPPS_QA_URL;
    		this.urlOpcs = ReporterConstants.DONEROPCS_QA_URL;
   		} else if(System.getenv("Environment").equalsIgnoreCase("production")){
   			this.urlOpps = ReporterConstants.ADMINOPPS_PROD_URL;
    		this.urlOpcs = ReporterConstants.DONEROPCS_PROD_URL;
   		} else if(System.getenv("Environment").equalsIgnoreCase("developement")){
   			this.urlOpps = ReporterConstants.ADMINOPPS_DEV_URL;
    		this.urlOpcs = ReporterConstants.DONEROPCS_DEV_URL;
   		}else if(System.getenv("Environment").equalsIgnoreCase("stage")){
   			this.urlOpps = ReporterConstants.ADMINOPPS_STAGE_URL;
    		this.urlOpcs = ReporterConstants.DONEROPCS_STAGE_URL;
   		}
		//get which node is running
		nodes = System.getenv("Nodes");
		//Get only node name
		String[] tempNode = nodes.split("-");
		String tempNode1 = tempNode[1].trim();
		//Get browsers for that node
		nodeParam = System.getenv(tempNode1);
		/*For Debug Purpose*/
		LOG.info("Debug: node = " + tempNode1);
		LOG.info("Debug: node1 = " + nodeParam);

		String nodeParamList[] = nodeParam.split(",");
		int i;
		for(i=0;i<nodeParamList.length;i++){
			if(nodeParamList[i].equalsIgnoreCase(this.browser+"-"+this.version)){
				LOG.info("Debug: browser = " + nodeParamList[i]);
				setWebDriverForLocal(this.browser);
				proceedExecution = true;
				break;
			}
		}
		LOG.info("Integer Value" + i);
		LOG.info("String Length" + nodeParamList.length );
		if(i==nodeParamList.length){
			LOG.info("Provide respective browser entry in TestNG XML & Jenkins");
			proceedExecution = false;
			//Thread.currentThread().stop();
			//Thread.currentThread().interrupt();
			//return;

		}

	}
}