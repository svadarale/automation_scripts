package com.automation.accelerators;

import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.datatransfer.StringSelection;
import java.awt.event.KeyEvent;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.text.ParseException;
import java.util.TimeZone;

import org.apache.log4j.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoAlertPresentException;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.Point;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.internal.Coordinates;
import org.openqa.selenium.internal.Locatable;
import org.openqa.selenium.support.ui.*;
import org.testng.Assert;

import com.automation.report.CReporter;
import com.automation.utilities.Locator;
import com.google.common.base.Function;

// TODO: Auto-generated Javadoc
/**
 * The Class ActionEngine.
 */
public class ActionEngine extends TestEngineWeb {

	/** The Constant LOG. */
	private static final Logger LOG = Logger.getLogger(ActionEngine.class);

	/** The msg click success. */
	private final String msgClickSuccess = "Successfully Clicked On ";

	/** The msg click failure. */
	private final String msgClickFailure = "Unable To Click On ";

	/** The msg type success. */
	private final String msgTypeSuccess = "Successfully Typed On ";

	/** The msg type failure. */
	private final String msgTypeFailure = "Unable To Type On ";

	/** The msg is element found success. */
	private final String msgIsElementFoundSuccess = "Successfully Found Element ";

	/** The msg is element found failure. */
	private final String msgIsElementFoundFailure = "Unable To Found Element ";

	/**
	 * Setdriver.
	 *
	 * @param driver
	 *            the driver
	 * @param reporter
	 *            the reporter
	 */
	public void setdriver(WebDriver driver, CReporter reporter) {
		this.Driver = driver;
		this.reporter = reporter;

	}

	/**
	 * waitForVisibilityOfElement
	 * 
	 * @param locator
	 * @param locatorName
	 * @param timeOutInSeconds
	 * @return
	 * @throws Throwable
	 */
	public boolean waitForVisibilityOfElement(By locator, String locatorName,
			long timeOutInSeconds) throws Throwable {
		try {
			LOG.info("Waiting for visibility of the element: " + locatorName);
			WebDriverWait wait = new WebDriverWait(this.Driver,
					timeOutInSeconds);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
			this.Driver.findElement(locator);
			LOG.info("Successfully found element: " + locatorName);
			return true;
		} catch (Exception e) {
			LOG.info("Failed to find element: " + locatorName
					+ " with locator: " + locator.toString());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean waitForVisibilityOfElement(By locator, String locatorName)
			throws Throwable {
		return waitForVisibilityOfElement(locator, locatorName,
				EXPLICIT_WAIT_TIME);
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @param timeOutInSeconds
	 * @return
	 * @throws Throwable
	 */
	public boolean waitForElementToBoClickable(By locator, String locatorName,
			long timeOutInSeconds) throws Throwable {
		try {
			LOG.info("Waiting for visibility of the element: " + locatorName);
			WebDriverWait wait = new WebDriverWait(this.Driver,
					timeOutInSeconds);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			this.Driver.findElement(locator);
			LOG.info("Successfully found element: " + locatorName);
			return true;
		} catch (Exception e) {
			LOG.info("Failed to find element: " + locatorName
					+ " with locator: " + locator.toString());
			e.printStackTrace();
			return false;
		}
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean waitForElementToBoClickable(By locator, String locatorName)
			throws Throwable {
		return waitForElementToBoClickable(locator, locatorName,
				EXPLICIT_WAIT_TIME);
	}

	/**
	 * 
	 * @param locator
	 * @param waitTime
	 * @param pollingTime
	 * @return
	 */
	public Boolean fluentlyWaitForElementToBeDisplayed(final By locator,
			int waitTime, int pollingTime) {
		FluentWait<WebDriver> wait = null;
		Boolean status = false;
		try {
			// Defining a fluent wait on the sent element
			wait = new FluentWait<WebDriver>(this.Driver)
					.withTimeout(waitTime, TimeUnit.SECONDS)
					.pollingEvery(pollingTime, TimeUnit.MILLISECONDS)
					.ignoring(NoSuchElementException.class);

			// Keep waiting for the element until it is visible or waitTime is
			// elapsed
			status = wait.until(new Function<WebDriver, Boolean>() {
				public Boolean apply(WebDriver driver) {
					return driver.findElement(locator).isDisplayed();
				}
			});
			return status;
		} catch (Exception e) {
			e.printStackTrace();
			return status;
		}

	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @param timeOutInSeconds
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementVisible(By locator, String locatorName,
			long timeOutInSeconds) throws Throwable {
		boolean isVisible = false;
		try {
			LOG.info("Verifying the visibility of element: " + locatorName);
			WebDriverWait wait = new WebDriverWait(this.Driver,
					timeOutInSeconds);
			isVisible = wait.until(
					ExpectedConditions.visibilityOfElementLocated(locator))
					.isDisplayed();
			LOG.info("Element: " + locatorName + " is visible");
			this.reporter.SuccessReport("isElementVisible",
					this.msgIsElementFoundSuccess + locatorName);
		} catch (Exception e) {
			LOG.info("Element: " + locatorName + " with locator: "
					+ locator.toString() + " is NOT visible");
			LOG.info("Exception Details: " + e.getMessage());
			this.reporter.failureReport(locatorName + "is ",
					this.msgIsElementFoundFailure + locatorName, this.Driver);
		}
		return isVisible;
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementVisible(By locator, String locatorName)
			throws Throwable {
		return isElementVisible(locator, locatorName, EXPLICIT_WAIT_TIME);
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @param timeOutInSeconds
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementClickable(By locator, String locatorName,
			long timeOutInSeconds) throws Throwable {
		boolean isClickable = false;
		try {
			LOG.info("Verifying if the element is clickable: " + locatorName);
			WebDriverWait wait = new WebDriverWait(this.Driver,
					timeOutInSeconds);
			isClickable = wait.until(
					ExpectedConditions.elementToBeClickable(locator))
					.isEnabled();
			LOG.info("Element: " + locatorName + " is clickable");
		} catch (Exception e) {
			LOG.info("Element: " + locatorName + " with locator: "
					+ locator.toString() + " is NOT clickable");
			LOG.info("Exception Details: " + e.getMessage());
		}
		return isClickable;
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementClickable(By locator, String locatorName)
			throws Throwable {
		return isElementClickable(locator, locatorName, EXPLICIT_WAIT_TIME);
	}
	/**
     * isVisible
     * @param  locator of (By)
     * @param  locatorName of (String)
     * @return boolean
     * @throws Throwable the throwable
     */
    public boolean isVisibleOnly(By locator, String locatorName) throws Throwable {
        boolean flag = false;
        try {
        	LOG.info("Verifying the invisibility of element: " + locatorName);
            flag = this.Driver.findElement(locator).isDisplayed();
            LOG.info("Element: " + locatorName + " is visible");
            LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        } catch (Exception e) {
            flag = false;
            LOG.info("Element: " + locatorName + " with locator: "
					+ locator.toString() + " is not visible");
			LOG.info("Exception Details: " + e.getMessage());
        }
        return flag;
    }

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @param timeOutInSeconds
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementNotVisible(By locator, String locatorName,
			long timeOutInSeconds) throws Throwable {
		boolean isNotVisible = false;
		try {
			LOG.info("Verifying the invisibility of element: " + locatorName);
			WebDriverWait wait = new WebDriverWait(this.Driver,
					timeOutInSeconds);
			isNotVisible = wait.until(ExpectedConditions
					.invisibilityOfElementLocated(locator));
			LOG.info("Element: " + locatorName + " is not visible");
		} catch (Exception e) {
			LOG.info("Element: " + locatorName + " with locator: "
					+ locator.toString() + " is visible");
			LOG.info("Exception Details: " + e.getMessage());
		}
		return isNotVisible;
	}

	/**
	 * 
	 * @param locator
	 * @param locatorName
	 * @return
	 * @throws Throwable
	 */
	public boolean isElementNotVisible(By locator, String locatorName)
			throws Throwable {
		return isElementNotVisible(locator, locatorName, EXPLICIT_WAIT_TIME);
	}

	/**
	 * Click.
	 *
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean click(By locator, String locatorName) throws Throwable {
		boolean status = false;
		try {
			LOG.info("Clicking on the element: " + "'" + locatorName + "'");
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			this.Driver.findElement(locator).click();
			LOG.info("Successfully clicked on the element: " + "'"
					+ locatorName + "'");
			this.reporter.SuccessReport("Click on " + "'" + locatorName + "'",
					this.msgClickSuccess + "'" + locatorName + "'");
			status = true;
		} catch (Exception e) {
			status = false;
			LOG.info("Failed to click on the element: " + "'" + locatorName
					+ "'" + " with locator:" + "'" + locator.toString() + "'");
			LOG.info("Exception Details: " + e.getMessage());
			this.reporter
					.failureReport("Click on " + "'" + locatorName + "'",
							this.msgClickFailure + "'" + locatorName + "'",
							this.Driver);
			throw e;
		}
		// fluentWait(30, 5);
		return status;
	}
	
	public boolean click(Locator locator) throws Throwable {
		return click(locator.getBy(),locator.getLocatorName());
	}

	/**
	 * JS click.
	 *
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean JSClick(By locator, String locatorName) throws Throwable {
		LOG.info("Clicking on the element: " + "'" + locatorName + "'");
		boolean flag = false;
		try {
			if (!this.reporter.getBrowserContext().getBrowserName()
					.equalsIgnoreCase("safari")) {
				WebDriverWait wait = new WebDriverWait(this.Driver, 60);
				ScrollToElementVisible(locator);
				wait.until(ExpectedConditions.elementToBeClickable(locator));
				WebElement element = this.Driver.findElement(locator);
				JavascriptExecutor executor = (JavascriptExecutor) this.Driver;
				executor.executeScript("arguments[0].click();", element);
				flag = true;
			} else {
				WebDriverWait wait = new WebDriverWait(this.Driver, 60);
				wait.until(ExpectedConditions.elementToBeClickable(locator));
				this.Driver.findElement(locator).click();
				this.reporter.SuccessReport("Click", this.msgClickSuccess
						+ locatorName);
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();

		} finally {
			if (flag == false) {
				this.reporter.failureReport("Click", "Click perform on"
						+ locatorName,
						Driver);
				LOG.info("Failed to click on the element: " + "'" + locatorName
						+ "'" + " with locator:" + "'" + locator.toString()
						+ "'");
				return flag;
			} else if (flag == true) {
				this.reporter.SuccessReport("Click", "Click is Done on"
						+ locatorName);
				LOG.info("Successfully clicked on the element: " + "'"
						+ locatorName + "'");
				return flag;
			}
		}

		return flag;
	}

	/**
	 * Javascript click.
	 *
	 * @param element
	 *            the element
	 */
	public void javascriptClick(WebElement element) {
		try {
			JavascriptExecutor executor = (JavascriptExecutor) this.Driver;
			executor.executeScript("arguments[0].click();", element);
		} catch (Exception e) {
			throw e;
		}
	}

	/**
	 * Browser specific click.
	 *
	 * @param browserName
	 *            the browser name
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @throws Throwable
	 */
	public void browserSpecificClick(By locator, String locatorName)
			throws Throwable {
		String browserName = this.reporter.getBrowserContext().getBrowserName()
				.toLowerCase();
		try {
			switch (browserName) {
			case "firefox":
			case "chrome": {
				click(locator, locatorName);
				break;
			}
			case "ie":
			case "safari":
			case "edge": {
				JSClick(locator, locatorName);
				break;
			}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Type.
	 *
	 * @param locator
	 *            the locator
	 * @param valueToType
	 *            the value to type
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean type(By locator, String valueToType, String locatorName)
			throws Throwable {
		boolean status = false;
		try {
			LOG.info("Typing in the element: " + "'" + locatorName + "'");
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			this.Driver.findElement(locator).clear();
			this.Driver.findElement(locator).sendKeys(valueToType);
			// fluentWait(10, 5);
			LOG.info("Successfully entered the value: " + valueToType
					+ " in the element: " + "'" + locatorName + "'");
			this.reporter.SuccessReport("Type in " + "'" + locatorName + "'",
					this.msgTypeSuccess + "'" + locatorName + "'");
			status = true;
		} catch (Exception e) {
			status = false;
			e.printStackTrace();
			LOG.info("Failed to enter the value:" + valueToType
					+ " in the element: " + "'" + locatorName + "'");
			this.reporter.failureReport("Type in " + "'" + locatorName + "'",
					this.msgTypeFailure + "'" + locatorName + "'", this.Driver);
			// throw new RuntimeException("Skipped");
			// RuntimeException ee = new RuntimeException();
			throw e;
		}

		return status;
	}

	/**
	 * Browser specific send keys.
	 *
	 * @param browserName
	 *            the browser name
	 * @param locator
	 *            the locator
	 * @param value
	 *            the value
	 * @param locatorName
	 *            the locator name
	 * @throws Throwable
	 */
	public void browserSpecificSendKeys(By locator, String value,
			String locatorName) throws Throwable {
		String browserName = this.reporter.getBrowserContext().getBrowserName()
				.toLowerCase();
		try {
			switch (browserName) {
			case "firefox":
			case "chrome": {
				type(locator, value, locatorName);
				break;
			}
			case "ie":
			case "safari":
			case "edge": {
				javascriptSendKeys(value, this.Driver.findElement(locator),
						locatorName);
				break;
			}
			}
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	/**
	 * Checks if is element present.
	 *
	 * @param by
	 *            the by
	 * @param locatorName
	 *            the locator name
	 * @param expected
	 *            the expected
	 * @return true, if is element present
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean isElementPresent(By by, String locatorName, boolean expected)
			throws Throwable {
		boolean status = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(by);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			this.Driver.findElement(by);
			this.reporter.successwithscreenshot("isElementPresent",
					this.msgIsElementFoundSuccess + locatorName, this.Driver);
			status = true;
		} catch (Exception e) {
			status = false;
			LOG.info(e.getMessage());
			if (expected == status) {
				this.reporter.SuccessReport("isElementPresent", locatorName
						+ "is ElementPresent");
			} else {
				this.reporter.failureReport(locatorName + "is ElementPresent",
						this.msgIsElementFoundFailure + locatorName,
						this.Driver);
			}
		}
		return status;
	}

	/**
	 * Switch to frame.
	 *
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean SwitchToFrame(By locator, String locatorName)
			throws Throwable {
		boolean flag = false;

		try {
			this.Driver.switchTo().defaultContent();
			WebDriverWait wait = new WebDriverWait(this.Driver, 90);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			this.Driver.switchTo().frame(this.Driver.findElement(locator));
			Thread.sleep(3000);
			flag = true;
			return flag;

		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Switch to Frame",
						"Switch to Frame failed" + locatorName,
						this.Driver);

			} else if (flag == true) {
				this.reporter.SuccessReport("Switch to Frame",
						"Switch to Frame : " + locatorName);
			}
		}

	}

	/**
	 * Moves the mouse to the middle of the element. The element is scrolled
	 * into view and its location is calculated using getBoundingClientRect.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link,menus etc..)
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean mouseover(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			WebElement mo = this.Driver.findElement(locator);
			new Actions(this.Driver).moveToElement(mo).build().perform();
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("MouseOver",
						"MouseOver action is not perform on" + locatorName,
						this.Driver);

			} else if (flag == true) {

				this.reporter.SuccessReport("MouseOver",
						"MouserOver Action is Done on" + locatorName);
			}
		}
	}
	public boolean browseAndUpload(String filePath, By locator, String locatorName) throws Throwable {
		  boolean flag = false;
		  try {
		   //enter(locator,locatorName);
		   click(locator, locatorName);
		   Thread.sleep(4000);

		   //Copy the file's absolute path to the clipboard
		   StringSelection ss = new StringSelection(filePath);
		   Toolkit.getDefaultToolkit().getSystemClipboard().setContents(ss, null);

		   //native key strokes for CTRL, V and ENTER keys
		   Robot robot = new Robot();
		   Thread.sleep(2000);
		   robot.keyPress(KeyEvent.VK_CONTROL);
		   Thread.sleep(2000);
		   robot.keyPress(KeyEvent.VK_V);
		   Thread.sleep(2000);
		   robot.keyRelease(KeyEvent.VK_V);
		   Thread.sleep(2000);
		   robot.keyRelease(KeyEvent.VK_CONTROL);
		   Thread.sleep(2000);
		   robot.keyPress(KeyEvent.VK_ENTER);
		   Thread.sleep(2000);
		   robot.keyRelease(KeyEvent.VK_ENTER);
		   Thread.sleep(2000);
		   flag= true;

		  } catch (NoAlertPresentException ex) {
		   // Alert present; set the flag

		   // Alert not present
		   ex.printStackTrace();
		  } finally {
		   if (flag==false) {
		    this.reporter.failureReport("Alert", "There was no alert to handle",
					this.Driver);
		   } else if (flag==true) {
		    this.reporter.SuccessReport("Alert",
		      "The Alert is handled successfully");
		   }
		  }

		  return flag;
		 }
	public boolean clickOption(By locator, String locatorName) throws Throwable
	 {
	  boolean status = false;
	  try
	  {
	   WebDriverWait wait = new WebDriverWait(this.Driver, 60);
	   wait.until(ExpectedConditions.elementToBeClickable(locator));
	   this.Driver.findElement(locator).click();
	   this.reporter.SuccessReport("Click" , this.msgClickSuccess + locatorName);
	   status = true;
	  }
	  catch(Exception e)
	  {
	   status = false;
	   LOG.info(e.getMessage());
	   this.reporter.failureReport("Click", this.msgClickFailure + locatorName, this.Driver);

	  }
	  return status;

	 }
	/**
	 * Tab.
	 *
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean tab(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			WebElement mo = this.Driver.findElement(locator);
			mo.sendKeys(Keys.TAB);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Tab",
						"Tab action is not perform on " + locatorName,
						this.Driver);

			} else if (flag == true) {

				this.reporter.SuccessReport("Tab", "Tab Action is Done on "
						+ locatorName);
			}
		}
	}

	/**
	 * Wait for element present.
	 *
	 * @param by
	 *            the by
	 * @param locator
	 *            the locator
	 * @param secs
	 *            the secs
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean waitForElementPresent(By by, String locator, int secs)
			throws Throwable {
		boolean status = false;

		try {

			WebDriverWait wait = new WebDriverWait(this.Driver, 30);
			ScrollToElementVisible(by);
			wait.until(ExpectedConditions.elementToBeClickable(by));
			for (int i = 0; i < secs / 2; i++) {
				List<WebElement> elements = this.Driver.findElements(by);
				if (elements.size() > 0) {
					status = true;
					return status;

				} else {
					this.Driver.manage().timeouts()
							.implicitlyWait(2, TimeUnit.SECONDS);
				}
			}

		} catch (Exception e) {

			return status;
		}

		return status;

	}

	/**
	 * Assert text on element.
	 *
	 * @param by
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param text
	 *            : expected text to assert on the element
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:link text, label text
	 *            etc..)
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean verifyText(By by, String text, String locatorName)
			throws Throwable {
		boolean flag = false;

		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(by);
			wait.until(ExpectedConditions.elementToBeClickable(by));

			String vtxt = getText(by, locatorName).trim();
			if (vtxt.equalsIgnoreCase(text.trim())) {
				flag = true;
			} else {
				flag = false;
			}
		} catch (Exception e) {
			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("VerifyText", text
						+ " is not present in the location" + locatorName,
						this.Driver);
				flag = false;
			} else if (flag == true) {
				this.reporter.SuccessReport("VerifyText", text
						+ " is present in the location " + locatorName);
				flag = true;
			}
		}
		return flag;

	}

	/**
	 * The innerText of this element.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:label text, SignIn Link
	 *            etc..)
	 * @return the text
	 * @throws Throwable
	 *             the throwable
	 * @return: String return text on element
	 */

	public String getText(By locator, String locatorName) throws Throwable {
		String text = "";
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			if (isElementPresent(locator, locatorName, true)) {
				text = this.Driver.findElement(locator).getText();
				flag = true;
			}
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			if (flag == false) {
				this.reporter.failureReport("GetText",
						"Unable to get Text from" + locatorName, this.Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("GetText", "Able to get Text from"
						+ locatorName);
			}
		}
		return text;
	}
	  /**
     * assertText
     * @param  by of (By)
     * @param  text of (String)
     * @return boolean
     * @throws Throwable the throwable
     */
    public boolean assertText(By by, String text) throws Throwable {
        boolean flag = false;
        LOG.info("+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++");
        try {
            Assert.assertEquals(getText(by, text).trim(), text.trim());
            flag = true;
            return true;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        } finally {
            if (!flag) {
                reporter.failureReport("AssertText :: ", text + " is not present in the element : ",this.Driver);
                return false;
            } else {
                reporter.SuccessReport("AssertText :: ", text + " is  present in the element : ");
            }
        }
    }

	/*
	 * public static int getResponseCode(String url) { try { return
	 * Request.Get(url).execute().returnResponse().getStatusLine()
	 * .getStatusCode(); } catch (Exception e) { throw new RuntimeException(e);
	 * } }
	 */
	/**
	 * This method verify check box is checked or not.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:sign in Checkbox etc..)
	 * @return true, if is checked
	 * @throws Throwable
	 *             the throwable
	 * @return: boolean value(True: if it is checked, False: if not checked)
	 */
	public boolean isChecked(By locator, String locatorName) throws Throwable {
		boolean bvalue = false;
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			if (this.Driver.findElement(locator).isSelected()) {
				flag = true;
				bvalue = true;
			}

		} catch (NoSuchElementException e) {

			bvalue = false;
		} finally {
			if (flag == true) {
				this.reporter.SuccessReport("IsChecked", locatorName
						+ " is Selected");
				// throw new ElementNotFoundException("", "", "");

			} else if (flag == false) {
				this.reporter.failureReport("IsChecked", locatorName
						+ " is not Select",
						this.Driver);
			}
		}
		return bvalue;
	}

	/**
	 * Verify alert present or not.
	 *
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 * @return: Boolean (True: If alert preset, False: If no alert)
	 */
	public boolean AlertAccept() throws Throwable {
		boolean flag = false;
		try {

			// Check the presence of alert
			org.openqa.selenium.Alert alert = this.Driver.switchTo().alert();
			// if present consume the alert
			alert.accept();
			flag = true;
		} catch (NoAlertPresentException ex) {
			// Alert present; set the flag

			// Alert not present
			ex.printStackTrace();
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Alert",
						"There was no alert to handle",
						this.Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("Alert",
						"The Alert is handled successfully");
			}
		}

		return flag;
	}

	/**
	 * Verify alert present or not for Safari.
	 *
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 * @return: Boolean (True: If alert preset, False: If no alert)
	 */
	public boolean JSAcceptAlert() throws Throwable {

		boolean flag = false;
		try {
			JavascriptExecutor executor = (JavascriptExecutor) this.Driver;
			executor.executeScript("confirm = function(message){return true;};");
			executor.executeScript("alert = function(message){return true;};");
			executor.executeScript("prompt = function(message){return true;}");
			flag = true;

		}

		catch (Exception e) {

		} finally {
			if (flag == false) {
				this.reporter.failureReport("Accept Alert",
						"Alert Accept is not performed ",
						this.Driver);
				return flag;
			} else if (flag == true) {
				this.reporter.SuccessReport("Accept Alert",
						"Alert Accept performed ");
				return flag;
			}
		}
		return flag;

	}

	/**
	 * Verify alert present or not.
	 *
	 * @param filePath
	 *            the file path
	 * @param locator
	 *            the locator
	 * @param locatorName
	 *            the locator name
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 * @return: Boolean (True: If alert preset, False: If no alert)
	 */
	public boolean uploadFile(String filePath, By locator, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			click(locator, locatorName);
			Thread.sleep(5000);
			Actions KeyboardEvent = new Actions(this.Driver);
			KeyboardEvent.sendKeys(filePath);
			Thread.sleep(1000);
			KeyboardEvent.sendKeys(Keys.ENTER);

		} catch (NoAlertPresentException ex) {
			// Alert present; set the flag

			// Alert not present
			ex.printStackTrace();
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Alert",
						"There was no alert to handle",
						this.Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("Alert",
						"The Alert is handled successfully");
			}
		}

		return flag;
	}

	/**
	 * Select a value from Dropdown using send keys.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param value
	 *            : Value wish type in dropdown list
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Year Dropdown, items
	 *            Listbox etc..)
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean selectBySendkeys(By locator, String value, String locatorName)
			throws Throwable {

		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			if (this.reporter.getBrowserContext().getBrowserName()
					.equalsIgnoreCase("ie")
					|| this.reporter.getBrowserContext().getBrowserName()
							.equalsIgnoreCase("safari")) {
				this.Driver.findElement(locator).click();
				Select drp = new Select(this.Driver.findElement(locator));
				drp.selectByVisibleText(value);
			} else {
				this.Driver.findElement(locator).sendKeys(value);
			}
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Select", value
						+ "is Not Select from the DropDown " + locatorName,
						this.Driver);
				// throw new ElementNotFoundException("", "", "");

			} else if (flag == true) {
				this.reporter.SuccessReport("Select", value
						+ " is Selected from the DropDown " + locatorName);
			}
		}
	}

	/**
	 * select value from DropDown by using selectByIndex.
	 *
	 * @param locator
	 *            : Action to be performed on element (Get it from Object
	 *            repository)
	 * @param index
	 *            : Index of value wish to select from dropdown list.
	 * @param locatorName
	 *            : Meaningful name to the element (Ex:Year Dropdown, items
	 *            Listbox etc..)
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean selectByIndex(By locator, int index, String locatorName)
			throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			Select s = new Select(this.Driver.findElement(locator));
			s.selectByIndex(index);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Select", "Option at index "
						+ index + " is Not Select from the DropDown"
						+ locatorName,
						this.Driver);

			} else if (flag == true) {
				this.reporter
						.SuccessReport("Select", "Option at index " + index
								+ "is Selected from the DropDown" + locatorName);
			}
		}
	}

	/**
	 * Switch to.
	 *
	 * @param url
	 *            the url
	 * @return true, if successful
	 * @throws Throwable
	 *             the throwable
	 */
	public boolean switchTo(String url) throws Throwable {
		boolean flag = false;
		try {

			Driver.navigate().to(url);
			flag = true;
		} catch (NoAlertPresentException ex) {
			// Alert present; set the flag

			// Alert not present
			ex.printStackTrace();
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Alert",
						"There was no alert to handle",
						this.Driver);
			} else if (flag == true) {
				this.reporter.SuccessReport("Alert",
						"The Alert is handled successfully");
			}
		}

		return flag;

	}

	public boolean verifyValueInList(By locator, String Value,
			String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			Select s = new Select(this.Driver.findElement(locator));
			List<WebElement> list = s.getOptions();
			for (int i = 0; i < list.size(); i++) {
				if ((list.get(i).getText()).equalsIgnoreCase(Value)) {
					flag = true;
					return true;
				}
			}
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == true) {
				this.reporter.SuccessReport("Verify", Value + " is added in  "
						+ locatorName);
			}

		}
		return flag;
	}

	public boolean verifypartialValueInList(By locator, String Value,
			String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			Select s = new Select(this.Driver.findElement(locator));
			List<WebElement> list = s.getOptions();
			for (int i = 0; i < list.size(); i++) {
				if ((list.get(i).getText()).contains(Value)) {
					flag = true;
					return true;
				}
			}
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == true) {
				this.reporter.SuccessReport("Verify", Value + " is added in  "
						+ locatorName);
			}

		}
		return flag;
	}
	public String RandomClass() {
		Random rnd = new Random();
		int n = 100000 + rnd.nextInt(900000);
		String str = Integer.toString(n);
		return str;
	}

	public void enterFilterbox(By Filter_Locator, String Value)
			throws Throwable {
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(Filter_Locator));
			this.Driver.findElement(Filter_Locator).clear();
			Thread.sleep(2000);
			type(Filter_Locator, Value, "Filter");
			Thread.sleep(5000);
			enter(Filter_Locator, "presss the Enter key");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public void FilterCheck(By Filter_Locator, By Grid_Locator, String Value)
			throws Throwable {
		WebDriverWait wait = new WebDriverWait(this.Driver, 60);
		wait.until(ExpectedConditions.elementToBeClickable(Filter_Locator));
		this.Driver.findElement(Filter_Locator).clear();
		type(Filter_Locator, Value, "Filter");
		Thread.sleep(5000);
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("safari")
				|| this.reporter.getBrowserContext().getBrowserName()
						.equalsIgnoreCase("ie")) {
			enter(Filter_Locator, "presss the Enter key");
			Thread.sleep(5000);
		} else {
			tab(Filter_Locator, "presss the Enter key");
			Thread.sleep(5000);
		}
		String Str_optionset_name = getText(Grid_Locator, "Name");

		if (Value.equals(Str_optionset_name)) {
			boolean validationPoint = isElementPresent(Grid_Locator, Value,
					true);
			if (validationPoint) {
				this.reporter.successwithscreenshot("verify " + Value
						+ " from grid", "User is able to verify " + Value
						+ " from grid", this.Driver);
			} else {
				this.reporter.failureReport("verify " + Value + " from grid",
						"User is not able to verify" + Value + " from grid",
						this.Driver);
			}
		}

	}

	public void FilterCheckWithEnter(By Filter_Locator, By Grid_Locator,
			String Value) throws Throwable {

		WebDriverWait wait = new WebDriverWait(this.Driver, 60);
		ScrollToElementVisible(Filter_Locator);
		wait.until(ExpectedConditions.elementToBeClickable(Filter_Locator));
		type(Filter_Locator, Value, "Filter");
		enter(Filter_Locator, "presss the Enter key");
		String Str_optionset_name = getText(Grid_Locator, "Name");

		if (Value.equals(Str_optionset_name)) {
			boolean validationPoint = isElementPresent(Grid_Locator,
					"Optionset name", true);
			if (validationPoint) {
				this.reporter.successwithscreenshot(
						"User is able to verify Optionset name from grid",
						"User is able to verify Optionset name from grid",
						this.Driver);
			} else {
				this.reporter.failureReport(
						"User is not able to verify Optionset name from grid",
						"User is not able to verify Optionset name from grid",
						this.Driver);
			}
		}

	}

	public void GreenLightClick(int pages, By LocatorRows, By LocatorCol,
			By nextLocator, String strName, By GreenlightLocator) {

		WebDriverWait wait = new WebDriverWait(this.Driver, 60);

		outerloop: for (int k = 0; k < pages; k++) {
			wait.until(ExpectedConditions.elementToBeClickable(LocatorRows));
			List<WebElement> rowCount = Driver.findElements(LocatorRows);

			for (int i = 2; i < rowCount.size(); i++) {
				System.out.println(i);

				wait.until(ExpectedConditions.elementToBeClickable(LocatorCol));
				List<WebElement> colCount = Driver.findElements(LocatorCol);

				for (int j = 2; j < colCount.size(); j++) {
					System.out.println(j);
					wait.until(ExpectedConditions
							.elementToBeClickable(nextLocator));
					WebElement ele = Driver.findElement(nextLocator);

					if (ele.getText().equals(strName)) {

						int trafficevalue = i - 2;

						String colvalue = Integer.toString(trafficevalue);
						Driver.findElement(By.id(GreenlightLocator + colvalue))
								.click();
						break outerloop;
					}

				}

			}

		}
	}

	public String strwithtimestamp(String str) {
		DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		Date date = new Date();
		String strDateStamp = dateFormat.format(date);
		strDateStamp = ((strDateStamp.replace(" ", "_")).replace("/", "_"))
				.replace(":", "_");

		return str + strDateStamp;
	}

	public void selectListValueEdge(By elementBy, String selectionValue) {
		try {
			WebElement webElement = this.Driver.findElement(elementBy);
			webElement.click();
			webElement.sendKeys(selectionValue);
			webElement.sendKeys(Keys.ENTER);
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public boolean selectByVisibleText(By locator, String value,
			String locatorName) throws Throwable {

		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			WebElement dropDownListBox = Driver.findElement(locator);

			Select clickThis = new Select(dropDownListBox);

			clickThis.selectByVisibleText(value);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Select", value
						+ "is Not Select from the DropDown " + locatorName,
						this.Driver);
				// throw new ElementNotFoundException("", "", "");

			} else if (flag == true) {
				this.reporter.SuccessReport("Select", value
						+ " is Selected from the DropDown " + locatorName);
			}
		}
	}

	public boolean enter(By locator, String locatorName) throws Throwable {
		boolean flag = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(locator);
			wait.until(ExpectedConditions.elementToBeClickable(locator));
			WebElement mo = this.Driver.findElement(locator);
			mo.sendKeys(Keys.ENTER);
			Thread.sleep(2000);
			flag = true;
			return true;
		} catch (Exception e) {

			return false;
		} finally {
			if (flag == false) {
				this.reporter.failureReport("Enter",
						"Enter action is not perform on " + locatorName,
						this.Driver);

			} else if (flag == true) {

				this.reporter.SuccessReport("Enter", "Enter Action is Done on "
						+ locatorName);
			}
		}
	}

	public boolean ScrollToElementVisible(By elementBy) {
		try {
			WebElement elem = (new WebDriverWait(this.Driver, 1))
					.until(ExpectedConditions
							.presenceOfElementLocated(elementBy));
			return ScrollToElementVisible(elem);
		} catch (Exception ex) {
			return false;
		}
	}

	public boolean ScrollToElementVisible(WebElement element) {
		if (this.reporter.getBrowserContext().getBrowserPlatform()
				.contains("windows")) {
			try {
				ScrollToTop();
				Point p = element.getLocation();
				if (p.getX() == 0 && p.getY() == 0) {
					return false;
				} else {
					((JavascriptExecutor) this.Driver)
							.executeScript("window.scroll(" + p.getX() + ","
									+ (p.getY() - 120) + ");");
					try {
						if (this.reporter.getBrowserContext().getBrowserName()
								.contains("internet")
								|| this.reporter.getBrowserContext()
										.getBrowserName().contains("ie")
								|| this.reporter.getBrowserContext()
										.getBrowserName().contains("edge")) {
							Thread.sleep(1000);
						}
					} catch (Exception e) {
					}
					return true;
				}
			} catch (Exception ex) {
				return false;
			}
		} else {
			try {
				Dimension d = element.getSize();
				if (d.height != 0 && d.width != 0) {
					new Actions(this.Driver).moveToElement(element).perform();
					return true;
				} else {
					JavascriptExecutor js = (JavascriptExecutor) this.Driver;
					int height_covered = 0, pageHeightLeft = 0;
					Long pageCurrentHeight = (Long) js
							.executeScript("return window.innerHeight");
					ScrollToTop();
					Long pageheight1 = (Long) js
							.executeScript("return window.innerHeight");
					Long maxPageHeight1 = (Long) js
							.executeScript("return Math.max(document.documentElement.scrollHeight, document.body.scrollHeight,"
									+ "document.documentElement.clientHeight, window.innerHeight)");
					float sections = (float) maxPageHeight1 / pageheight1;
					int numberOfRows = (int) Math.ceil(sections);
					int pageheight = pageheight1.intValue();
					int maxPageHeight = maxPageHeight1.intValue();
					for (int row = 0; row < numberOfRows; row++) {
						pageHeightLeft = maxPageHeight - height_covered;
						if ((pageHeightLeft < pageheight)) {
							d = element.getSize();
							if (d.height == 0 && d.width == 0) {
								js.executeScript("window.scrollTo(0,"
										+ pageCurrentHeight + ")");
								return false;
							} else {
								new Actions(this.Driver).moveToElement(element)
										.perform();
								return true;
							}
						} else {
							d = element.getSize();
							if (!(d.height != 0 && d.width != 0)) {
								height_covered = height_covered + pageheight;
								js.executeScript("window.scrollTo(0,"
										+ height_covered + ")");
								try {
									Thread.sleep(100);
								} catch (InterruptedException e) {
								}
							} else {
								new Actions(this.Driver).moveToElement(element)
										.perform();
								return true;
							}
						}
					}
					ScrollToTop();
					return false;
				}
			} catch (org.openqa.selenium.ElementNotVisibleException e) {
				return false;
			} catch (Exception ex) {
				return false;
			}
		}
	}

	public void ScrollToTop() {
		if (this.reporter.getBrowserContext().getBrowserPlatform()
				.contains("windows")) {
			try {
				JavascriptExecutor js = (JavascriptExecutor) this.Driver;
				js.executeScript("window.scrollTo(0,0);");
			} catch (Exception e) {
			}
		} else {
			try {
				JavascriptExecutor js = (JavascriptExecutor) this.Driver;
				js.executeScript("$('body').scrollTop(0);");
			} catch (Exception e) {
				System.out
						.println("Failed to Swipe on Top on Non-Desktop device. Check 'ScrollToTop' method under CommonFunctionLib");
			}
		}
	}

	public void ScrollToBottom() {
		if (this.reporter.getBrowserContext().getBrowserPlatform()
				.contains("windows")) {
			try {
				JavascriptExecutor js = (JavascriptExecutor) this.Driver;
				js.executeScript("window.scrollTo(0,document.documentElement.scrollHeight);");
			} catch (Exception e) {
			}
		} else {
			JavascriptExecutor js = (JavascriptExecutor) this.Driver;
			int height_covered = 0, pageHeightLeft = 0;
			Long pageheight1 = (Long) js
					.executeScript("return window.innerHeight");
			Long maxPageHeight1 = (Long) js
					.executeScript("return Math.max(document.documentElement.scrollHeight, document.body.scrollHeight,"
							+ "document.documentElement.clientHeight, window.innerHeight)");
			float sections = (float) maxPageHeight1 / pageheight1;
			int numberOfRows = (int) Math.ceil(sections);
			int pageheight = pageheight1.intValue();
			int maxPageHeight = maxPageHeight1.intValue();
			for (int row = 0; row < numberOfRows; row++) {
				pageHeightLeft = maxPageHeight - height_covered;
				if ((pageHeightLeft < pageheight)) {
				} else {
					height_covered = height_covered + pageheight;
					js.executeScript("window.scrollTo(0," + height_covered
							+ ")");
				}
			}
		}
	}

	public boolean javascriptSendKeys(String value, WebElement webElement,
			String locatorName) throws Throwable {
		boolean state = false;
		try {
			WebDriverWait wait = new WebDriverWait(this.Driver, 60);
			ScrollToElementVisible(webElement);
			wait.until(ExpectedConditions.elementToBeClickable(webElement));
			JavascriptExecutor js = (JavascriptExecutor) (this.Driver);
			js.executeScript("arguments[0].value=\"" + value + "\";",
					webElement);
			this.reporter.SuccessReport("type", this.msgTypeSuccess
					+ locatorName);
			state = true;
		} catch (Exception e) {
			state = false;
			e.printStackTrace();
			LOG.info(e.getMessage());
			this.reporter.failureReport("type", this.msgTypeFailure
					+ locatorName, this.Driver);
		}
		return state;
	}

	public void ScrollToTopPage() {

		try {

			JavascriptExecutor js = (JavascriptExecutor) this.Driver;

			js.executeScript("window.scrollBy(0,-250)", "");

		} catch (Exception e) {

			System.out
					.println("Failed to Swipe on Top on Non-Desktop device. Check 'ScrollToTop' method under CommonFunctionLib");

		}

	}

	public int getListIndex(By locator, String Value) throws Throwable {
		int index = 0;
		try {
			Select s = new Select(this.Driver.findElement(locator));
			List<WebElement> list = s.getOptions();
			for (int i = 0; i < list.size(); i++) {
				if ((((list.get(i).getText()).trim()).contains(Value))) {
					index = i;
					return index;
				}
			}
		} catch (Exception e) {

		}

		return index;
	}

	public String getMonthNumericValue(String monthCharValue) {
		switch (monthCharValue.trim().toUpperCase()) {
		case "JAN":
		case "JANUARY":
			return "01";
		case "FEB":
		case "FEBRUARY":
			return "02";
		case "MAR":
		case "MARCH":
			return "03";
		case "APR":
		case "APRIL":
			return "04";
		case "MAY":
			return "05";
		case "JUN":
		case "JUNE":
			return "06";
		case "JULY":
		case "JUL":
			return "07";
		case "AUGUST":
		case "AUG":
			return "08";
		case "SEP":
		case "SEPTEMBER":
			return "09";
		case "OCT":
		case "OCTOBER":
			return "10";
		case "NOV":
		case "NOVEMBER":
			return "11";
		case "DECEMBER":
		case "DEC":
			return "12";
		default:
			return "";
		}
	}

	public String getMonthName(String monthNumericValue) {
		switch (monthNumericValue.trim()) {
		case "01":
			return "January";
		case "02":
			return "February";
		case "03":
			return "March";
		case "04":
			return "April";
		case "05":
			return "May";
		case "06":
			return "June";
		case "07":
			return "July";
		case "08":
			return "August";
		case "09":
			return "September";
		case "10":
			return "October";
		case "11":
			return "November";
		case "12":
			return "December";
		default:
			return "";
		}
	}

	public String getFirstThreeMonthName(String monthNumericValue) {
		switch (monthNumericValue.trim()) {
		case "01":
			return "Jan";
		case "02":
			return "Feb";
		case "03":
			return "Mar";
		case "04":
			return "Apr";
		case "05":
			return "May";
		case "06":
			return "Jun";
		case "07":
			return "Jul";
		case "08":
			return "Aug";
		case "09":
			return "Sep";
		case "10":
			return "Oct";
		case "11":
			return "Nov";
		case "12":
			return "Dec";
		default:
			return "";
		}
	}

	public String getCurrentDataTimeinFormat(String dateformat) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		try {
			return sdf.format(cal.getTime());
		} catch (Exception e) {
			return "";
		}
	}

	public Date getDateTimeFromProvidedString(String dateValue, String format) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		try {
			Date date = formatter.parse(dateValue);
			return date;
		} catch (Exception e) {
			return null;
		}
	}

	public String getDateTimeFromProvidedString(Date date, String dateformat) {
		SimpleDateFormat sdf = new SimpleDateFormat(dateformat);
		try {
			return sdf.format(date);
		} catch (Exception e) {
			return "";
		}
	}

	public static String getCurrentTimeZone() {
		String timeZone = null;
		// get Calendar instance
		Calendar now = Calendar.getInstance();
		// get current TimeZone using getTimeZone method of Calendar class
		TimeZone tZone = now.getTimeZone();
		// display current TimeZone using getDisplayName() method of TimeZone
		// class
		timeZone = tZone.getDisplayName();
		return timeZone;
	}

	/**
	 * Parameter: To add no. of days in current date - pass parameter value :
	 * date else day.
	 * 
	 * @param calendarFormat
	 * @param format
	 * @param AddNoOfDays
	 * @return
	 */
	public String AddNoOfDaysInServerTime(String calendarFormat, String format,
			int AddNoOfDays) {
		String Timezone = getCurrentTimeZone();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		sdf.setTimeZone(TimeZone.getTimeZone(Timezone));
		Calendar c = Calendar.getInstance();
		if (calendarFormat.equalsIgnoreCase("date")) {
			c.add(Calendar.DATE, AddNoOfDays);
		} else {
			c.add(Calendar.DAY_OF_WEEK, 2);

		}
		String fdate = sdf.format(c.getTime());
		return fdate;
	}

	public String ChangeStringFirstCharToCapital(String str) {
		StringBuilder b = new StringBuilder(str);
		int i = 0;
		do {
			b.replace(i, i + 1, b.substring(i, i + 1).toUpperCase());
			i = b.indexOf(" ", i) + 1;
		} while (i > 0 && i < b.length());
		return b.toString();
	}

	public String getFirstDayOfCurrentWeek(String format) throws ParseException {
		String fdate;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		fdate = sdf.format(cal.getTime());
		Date date1 = sdf.parse(fdate);
		Date date = new Date();
		SimpleDateFormat sdf2 = new SimpleDateFormat("yyyy/MM/dd HH:mm");
		Date date2 = sdf2.parse(sdf2.format(date));
		if (date1.after(date2)) {
			Calendar cal1 = Calendar.getInstance();
			SimpleDateFormat sdf1 = new SimpleDateFormat(format);
			cal1.add(Calendar.DAY_OF_WEEK, -6);
			fdate = sdf1.format(cal1.getTime());
		}
		return fdate;
	}

	public String getLastDayOfCurrentWeek(String format) {
		String ldate;
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY);
		cal.add(Calendar.DAY_OF_WEEK, 6);
		ldate = sdf.format(cal.getTime());
		return ldate;
	}

	public String getLastDayOfMonth(String format) {
		Calendar cal = Calendar.getInstance();
		SimpleDateFormat sdf = new SimpleDateFormat(format);

		cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));
		String ldate = sdf.format(cal.getTime());
		return ldate;
	}

	public String getLastDayOfMonth(int month, int year) {
		Calendar calendar = Calendar.getInstance();
		// passing month-1 because 0-->jan, 1-->feb... 11-->dec
		calendar.set(year, month - 1, 1);
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		Date date = calendar.getTime();
		DateFormat DATE_FORMAT = new SimpleDateFormat("dd");
		SimpleDateFormat sdf = new SimpleDateFormat("dd");
		sdf.format(date);
		DATE_FORMAT.format(date);
		return DATE_FORMAT.format(date);
	}

	public String getDateInExpectedFormat(String format, String dateInString,
			String returnformat) {
		SimpleDateFormat formatter = new SimpleDateFormat(format);
		SimpleDateFormat returnFormatter = new SimpleDateFormat(returnformat);
		Date date = null;
		try {
			date = formatter.parse(dateInString);
			// formatter = new SimpleDateFormat(returnformat);
			return returnFormatter.format(date);
		} catch (ParseException e) {
			e.printStackTrace();
			return null;
		}

	}

	public int getTotalNoofdaysinMonth() {
		int CurrentMonthDays = 0;
		try {
			Calendar c = Calendar.getInstance();
			int monthMaxDays = c.getActualMaximum(Calendar.DAY_OF_MONTH);
			CurrentMonthDays = monthMaxDays;
		} catch (Exception e) {
			e.getMessage();
		}
		return CurrentMonthDays;
	}

	
	public int calculateAge(String dob, String format) throws Exception// Enter
																		// Date
																		// od
																		// Birth
																		// //
	{
		try {
			Date dob_DateObj = getDateTimeFromProvidedString(dob, format);
			long ageInMillis = new Date().getTime() - dob_DateObj.getTime();
			return new Date(ageInMillis).getYear();
		} catch (IllegalArgumentException ex) {
			LOG.info("Failed to get Age from provided format (" + format
					+ ") and date value (" + dob + ")");
			throw ex;
		} catch (NullPointerException ex) {
			LOG.info("Failed to get Age from provided format (" + format
					+ ") and dob value (" + dob + ")");
			throw ex;
		}
	}

	public String getCurrentMonth() {
		String CurrentMonth = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
			Calendar cal = Calendar.getInstance();
			CurrentMonth = sdf.format(cal.getTime());
			System.out.println("Current Month :" + CurrentMonth);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return CurrentMonth;
	}

	public String getNextMonth() {
		String NextMonth = "";
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("MMMM");
			Calendar cal = Calendar.getInstance();
			cal.add(Calendar.MONTH, 1);
			cal.set(Calendar.DAY_OF_MONTH,
					cal.getActualMaximum(Calendar.DAY_OF_MONTH));
			NextMonth = sdf.format(cal.getTime());
			System.out.println("Next Month :" + NextMonth);
		} catch (Exception e) {
			e.getStackTrace();
		}
		return NextMonth;
	}

	public int getCurrentYear() {
		int year = 0;
		Calendar now = Calendar.getInstance();
		try {
			year = now.get(Calendar.YEAR);

		} catch (Exception e) {
			return year;
		}
		return year;
	}

	public void ScrollToBottomPage() {

		try {

			JavascriptExecutor js = (JavascriptExecutor) this.Driver;

			js.executeScript("window.scrollBy(0,250)", "");

		} catch (Exception e) {

			System.out
					.println("Failed to Swipe bottom on Non-Desktop device. Check 'ScrollToBottom' method under CommonFunctionLib");

		}

	}

	public void ScrollToElement(By by) {

		try {

			WebElement element = Driver.findElement(by);
			Coordinates coordinate = ((Locatable) element).getCoordinates();
			coordinate.onPage();
			coordinate.inViewPort();
			Thread.sleep(4000);
		} catch (Exception e) {

			System.out
					.println("Failed to Swipe bottom on Non-Desktop device. Check 'ScrollToBottom' method under CommonFunctionLib");

		}

	}

	public String getMainWindowHandle(WebDriver driver) {
		return driver.getWindowHandle();
	}

	public static boolean closeAllOtherWindows(WebDriver driver,
			String openWindowHandle) {
		Set<String> allWindowHandles = driver.getWindowHandles();
		for (String currentWindowHandle : allWindowHandles) {
			if (!currentWindowHandle.equals(openWindowHandle)) {
				driver.switchTo().window(currentWindowHandle);
				driver.close();
			}
		}

		driver.switchTo().window(openWindowHandle);
		if (driver.getWindowHandles().size() == 1)
			return true;
		else
			return false;
	}

	public static String windowHandles(WebDriver driver) {
		String strhwndval = null;
		Set<String> handleVal_Coll = driver.getWindowHandles();
		Iterator<String> IT_hwndColl = handleVal_Coll.iterator();
		while (IT_hwndColl.hasNext() == true) {
			strhwndval = IT_hwndColl.next();
			driver.switchTo().window(strhwndval);
		}
		return strhwndval;
	}

	public void closeAllWindows(WebDriver driver) {
		String strhwndval = null;
		Set<String> handleVal_Coll = driver.getWindowHandles();
		Iterator<String> IT_hwndColl = handleVal_Coll.iterator();
		while (IT_hwndColl.hasNext()) {
			strhwndval = IT_hwndColl.next();
			driver.switchTo().window(strhwndval);
			driver.close();
		}
	}

	public void dynamicWait(final By by, String sleepTime) throws Throwable {
		try {
			for (int i = 1; i <= 20; i++) {
				if (isElementPresent(by, "Dynamic Wait", true)) {
					ScrollToElementVisible(by);
					break;
				} else {
					Thread.sleep(Long.parseLong(sleepTime));
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public String getDate(int n) {
		SimpleDateFormat sdf = new SimpleDateFormat("dd-MMMM-YYYY");
		Calendar c = Calendar.getInstance();
		c.setTime(new Date());

		c.add(Calendar.DATE, n);
		String output = sdf.format(c.getTime());
		return output;
	}

	public static enum Mode {
		ALPHA, ALPHANUMERIC, NUMERIC
	}

	public String getUniqueID() {
		String strDateStamp = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			strDateStamp = dateFormat.format(date);
			strDateStamp = ((strDateStamp.replace(" ", "")).replace("/", ""))
					.replace(":", "");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return strDateStamp;
	}

	public static String generateRandomString(int length, Mode mode) {
		StringBuffer buffer = new StringBuffer();
		String characters = "";
		Boolean isNumericOnly = false;
		String automationTextPrefix = "";
		switch (mode) {
		case ALPHA:
			characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
			break;

		case ALPHANUMERIC:
			characters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890";
			break;

		case NUMERIC:
			isNumericOnly = true;
			characters = "123456789";
			break;
		}
		int charactersLength = characters.length();
		if (length >= 3 && !isNumericOnly) {
			length = length - 2;
			automationTextPrefix = "AT";
		}
		for (int i = 0; i < length; i++) {
			double index = Math.random() * charactersLength;
			buffer.append(characters.charAt((int) index));
		}
		if (isNumericOnly) {
			return buffer.toString();
		} else {
			return automationTextPrefix + buffer.toString();
		}
	}

	public Integer getRandomNumberBetween(Integer upper, Integer lower) {
		return (int) ((Math.random() * (upper - lower)) + lower);
	}

	public static String getDateInString() {
		String strDateStamp = null;
		try {
			DateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
			Date date = new Date();
			strDateStamp = dateFormat.format(date);
			strDateStamp = ((strDateStamp.replace(" ", "")).replace("/", ""))
					.replace(":", "");
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return strDateStamp;
	}

	/**
	 * Checks if is firefox.
	 *
	 * @return true, if is firefox
	 */
	public boolean isFirefox() {
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("firefox"))
			return true;
		else
			return false;
	}

	/**
	 * Checks if is chrome.
	 *
	 * @return true, if is chrome
	 */
	public boolean isChrome() {
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("chrome"))
			return true;
		else
			return false;
	}

	/**
	 * Checks if is ie.
	 *
	 * @return true, if is ie
	 */
	public boolean isIE() {
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("ie"))
			return true;
		else
			return false;
	}

	/**
	 * Checks if is safari.
	 *
	 * @return true, if is safari
	 */
	public boolean isSafari() {
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("safari"))
			return true;
		else
			return false;
	}

	/**
	 * Checks if is edge.
	 *
	 * @return true, if is edge
	 */
	public boolean isEdge() {
		if (this.reporter.getBrowserContext().getBrowserName()
				.equalsIgnoreCase("edge"))
			return true;
		else
			return false;
	}

	
	public void waitForSeconds(int seconds) throws Throwable{
		Thread.sleep(seconds * 1000);
	}
}
