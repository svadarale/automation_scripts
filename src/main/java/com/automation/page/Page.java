package com.automation.page;



/**
 * The Interface Page.
 */
public interface Page{
	
	/**
	 * Fill.
	 */
	public abstract void fill();
	
	/**
	 * Move.
	 */
	public abstract void move();

}
